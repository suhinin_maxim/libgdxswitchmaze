package com.razvivashka.consts;

/**
 * Created by maxim on 24.09.2017.
 */

public class ViewConst {

    public static final float SCREEN_PADDING = 50.0f;
    public static final float SMALL_BUTTON_SIZE = 200.0f;

}
