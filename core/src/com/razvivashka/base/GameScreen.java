package com.razvivashka.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by maxim on 24.09.2017.
 */

public abstract class GameScreen<T> implements Screen, InputProcessor {

    protected T game;
    protected Stage stage;

    public GameScreen (T game) {
        this.game = game;

        OrthographicCamera camera = new OrthographicCamera();
        Viewport viewport = new FitViewport(1080.0f, 1920.0f, camera);

        stage = new Stage(viewport);

        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(this);

        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    protected abstract void renderScene(float delta);

    protected boolean onBackPressed() {
        return false;
    }

    @Override
    public void dispose () {
        stage.dispose();
    }

    @Override
    public void hide () {
        // empty
    }

    @Override
    public void pause () {
        // empty
    }

    @Override
    public void render (float delta) {
        renderScene(delta);

        stage.act();
        stage.draw();
    }

    @Override
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void resume () {
        // empty
    }

    @Override
    public void show () {
        // empty
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK){
            return onBackPressed();
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
