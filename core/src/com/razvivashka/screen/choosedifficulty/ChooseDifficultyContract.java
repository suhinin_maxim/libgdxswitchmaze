package com.razvivashka.screen.choosedifficulty;

/**
 * Created by admin on 04.09.17.
 */

public interface ChooseDifficultyContract {

    interface View {

        void setPresenter(ChooseDifficultyContract.Presenter presenter);
        void close();
        void openChooseHeroFragment ();

    }

    interface Presenter {

        void close();
        void selectEasy();
        void selectNormal();
        void selectHard();

    }

}
