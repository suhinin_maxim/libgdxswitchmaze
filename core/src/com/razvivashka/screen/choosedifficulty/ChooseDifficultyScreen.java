package com.razvivashka.screen.choosedifficulty;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;
import com.razvivashka.consts.ViewConst;

/**
 * Created by admin on 04.09.17.
 */

public class ChooseDifficultyScreen extends GameScreen<GdxGame> implements ChooseDifficultyContract.View {

    // region fields

    private ChooseDifficultyContract.Presenter mPresenter;
    private InteractionListener mInteractionListener;

    private ImageButton btnClose;
    private ImageButton btnEasy;
    private ImageButton btnNormal;
    private ImageButton btnHard;

    // endregion


    // region constructors

    public ChooseDifficultyScreen(GdxGame game) {
        super(game);
        mInteractionListener = game;
    }

    // endregion

    // region fragment lifecycle


    @Override
    public void show() {
        createCloseBtn();

        createEasyBtn();
        createNormalBtn();
        createHardBtn();
    }

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_choose_difficulty.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }

    @Override
    protected boolean onBackPressed() {
        mPresenter.close();

        return true;
    }

    // endregion


    // region implements ChooseDifficultyContract.View

    @Override
    public void setPresenter (ChooseDifficultyContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void close () {
        mInteractionListener.onChooseDifficultyClose();
    }

    @Override
    public void openChooseHeroFragment () {
        mInteractionListener.onChooseDifficultyOpenChooseHero();
    }

    // endregion


    // region createView

    private void createCloseBtn() {
        ImageButton.ImageButtonStyle closeBtnStyle = new ImageButton.ImageButtonStyle();
        closeBtnStyle.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_close.png", Texture.class)));
        closeBtnStyle.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_close_pressed.png", Texture.class)));

        btnClose = new ImageButton(closeBtnStyle);

        float width = ViewConst.SMALL_BUTTON_SIZE;
        float height = ViewConst.SMALL_BUTTON_SIZE;
        float left = 50;
        float bottom = stage.getHeight() - height - 50;

        btnClose.setSize(width, height);
        btnClose.setPosition(left, bottom);
        btnClose.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.close();
            }
        });

        stage.addActor(btnClose);
    }

    private void createEasyBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/choosedifficulty/btn_easy_difficulty.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/choosedifficulty/btn_easy_difficulty_pressed.png", Texture.class)));

        btnEasy = new ImageButton(style);

        float width = 700;
        float height = 230;
        float left = (stage.getWidth() - width) / 2.0f;
        float bottom = stage.getHeight() - 620;

        btnEasy.setSize(width, height);
        btnEasy.setPosition(left, bottom);
        btnEasy.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectEasy();
            }
        });

        stage.addActor(btnEasy);
    }

    private void createNormalBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/choosedifficulty/btn_normal_difficulty.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/choosedifficulty/btn_normal_difficulty_pressed.png", Texture.class)));

        btnNormal = new ImageButton(style);

        float width = 700;
        float height = 230;
        float left = (stage.getWidth() - width) / 2.0f;
        float bottom = stage.getHeight() - 900;

        btnNormal.setSize(width, height);
        btnNormal.setPosition(left, bottom);
        btnNormal.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectNormal();
            }
        });

        stage.addActor(btnNormal);
    }

    private void createHardBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/choosedifficulty/btn_hard_difficulty.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/choosedifficulty/btn_hard_difficulty_pressed.png", Texture.class)));

        btnHard = new ImageButton(style);

        float width = 700;
        float height = 230;
        float left = (stage.getWidth() - width) / 2.0f;
        float bottom = stage.getHeight() - 1180;

        btnHard.setSize(width, height);
        btnHard.setPosition(left, bottom);
        btnHard.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectHard();
            }
        });

        stage.addActor(btnHard);
    }

    // endregion

    public interface InteractionListener {

        void onChooseDifficultyClose();
        void onChooseDifficultyOpenChooseHero();

    }

}
