package com.razvivashka.screen.choosedifficulty;

import com.razvivashka.logic.GameLogic;
import com.razvivashka.logic.domaindata.GameDifficulty;

/**
 * Created by admin on 04.09.17.
 */

public class ChooseDifficultyPresenter implements ChooseDifficultyContract.Presenter {

    private ChooseDifficultyContract.View contractView;
    private GameLogic gameLogic;

    public ChooseDifficultyPresenter (ChooseDifficultyContract.View contractView, GameLogic gameLogic) {
        this.contractView = contractView;
        contractView.setPresenter(this);

        this.gameLogic = gameLogic;
    }

    @Override
    public void close () {
        contractView.close();
    }

    @Override
    public void selectEasy () {
        gameLogic.setDifficulty(GameDifficulty.EASY);
        contractView.openChooseHeroFragment();
    }

    @Override
    public void selectNormal () {
        gameLogic.setDifficulty(GameDifficulty.NORMAL);
        contractView.openChooseHeroFragment();
    }

    @Override
    public void selectHard () {
        gameLogic.setDifficulty(GameDifficulty.HARD);
        contractView.openChooseHeroFragment();
    }

}
