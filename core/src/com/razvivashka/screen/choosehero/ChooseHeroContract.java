package com.razvivashka.screen.choosehero;

/**
 * Created by maxim on 24.09.2017.
 */

public class ChooseHeroContract {

    public interface View {

        void setPresenter(ChooseHeroContract.Presenter presenter);
        void back();
        void openChooseLevel();
    }

    public interface Presenter {

        void back();
        void selectHeroOne ();
        void selectHeroTwo ();
        void selectHeroThree ();
        void selectHeroFour ();
        void selectHeroFive ();
        void selectHeroSix ();

    }

}
