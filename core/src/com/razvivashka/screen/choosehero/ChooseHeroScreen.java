package com.razvivashka.screen.choosehero;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;

/**
 * Created by maxim on 24.09.2017.
 */

public class ChooseHeroScreen extends GameScreen<GdxGame> implements ChooseHeroContract.View {

    // region fields

    private ChooseHeroContract.Presenter mPresenter;
    private InteractionListener mInteractionListener;

    private ImageButton btnHero1;
    private ImageButton btnHero2;
    private ImageButton btnHero3;
    private ImageButton btnHero4;
    private ImageButton btnHero5;
    private ImageButton btnHero6;

    // endregion


    // region constructors

    public ChooseHeroScreen(GdxGame game) {
        super(game);
        mInteractionListener = game;

        createButtons();
    }

    // endregion


    // region lifecycle

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_choose_hero.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }

    @Override
    protected boolean onBackPressed() {
        mPresenter.back();

        return true;
    }

    // endregion


    // region implements ChooseHeroContract.View

    @Override
    public void setPresenter(ChooseHeroContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void openChooseLevel() {
        mInteractionListener.onChooseHeroOpenChooseLevel();
    }

    @Override
    public void back() {
        mInteractionListener.onChooseHeroBackPressed();
    }

    // endregion


    // region create view

    private void createButtons() {
        float hero_btn_size = 480;

        float screen_padding = 40;
        float buttons_margin = 40;

        // first hero
        float left = screen_padding;
        float bottom = screen_padding + 2*hero_btn_size + 2*buttons_margin;
        btnHero1 = createHeroButton("screen/choosehero/btn_hero1.png", "screen/choosehero/btn_hero1_pressed.png");
        btnHero1.setSize(hero_btn_size, hero_btn_size);
        btnHero1.setPosition(left, bottom);
        btnHero1.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectHeroOne();
            }
        });

        // second hero
        left = screen_padding + hero_btn_size + buttons_margin;
        bottom = screen_padding + 2*hero_btn_size + 2*buttons_margin;
        btnHero2 = createHeroButton("screen/choosehero/btn_hero2.png", "screen/choosehero/btn_hero2_pressed.png");
        btnHero2.setSize(hero_btn_size, hero_btn_size);
        btnHero2.setPosition(left, bottom);
        btnHero2.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectHeroTwo();
            }
        });

        // third hero
        left = screen_padding;
        bottom = screen_padding + hero_btn_size + buttons_margin;
        btnHero3 = createHeroButton("screen/choosehero/btn_hero3.png", "screen/choosehero/btn_hero3_pressed.png");
        btnHero3.setSize(hero_btn_size, hero_btn_size);
        btnHero3.setPosition(left, bottom);
        btnHero3.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectHeroThree();
            }
        });

        // fourth hero
        left = screen_padding + hero_btn_size + buttons_margin;
        bottom = screen_padding + hero_btn_size + buttons_margin;
        btnHero4 = createHeroButton("screen/choosehero/btn_hero4.png", "screen/choosehero/btn_hero4_pressed.png");
        btnHero4.setSize(hero_btn_size, hero_btn_size);
        btnHero4.setPosition(left, bottom);
        btnHero4.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectHeroFour();
            }
        });

        // fifth hero
        left = screen_padding;
        bottom = screen_padding;
        btnHero5 = createHeroButton("screen/choosehero/btn_hero5.png", "screen/choosehero/btn_hero5_pressed.png");
        btnHero5.setSize(hero_btn_size, hero_btn_size);
        btnHero5.setPosition(left, bottom);
        btnHero5.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectHeroFive();
            }
        });

        // six hero
        left = screen_padding + hero_btn_size + buttons_margin;
        bottom = screen_padding;
        btnHero6 = createHeroButton("screen/choosehero/btn_hero6.png", "screen/choosehero/btn_hero6_pressed.png");
        btnHero6.setSize(hero_btn_size, hero_btn_size);
        btnHero6.setPosition(left, bottom);
        btnHero6.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.selectHeroSix();
            }
        });

    }

    private ImageButton createHeroButton(String bg, String bg_pressed) {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get(bg, Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get(bg_pressed, Texture.class)));

        ImageButton button = new ImageButton(style);
        stage.addActor(button);

        return button;
    }

    // endregion


    public interface InteractionListener {

        void onChooseHeroBackPressed ();
        void onChooseHeroOpenChooseLevel ();

    }

}
