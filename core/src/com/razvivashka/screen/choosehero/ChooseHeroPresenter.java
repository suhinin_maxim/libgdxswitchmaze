package com.razvivashka.screen.choosehero;

import com.razvivashka.logic.GameLogic;
import com.razvivashka.logic.domaindata.HeroCellType;

/**
 * Created by maxim on 24.09.2017.
 */

public class ChooseHeroPresenter implements ChooseHeroContract.Presenter {

    private ChooseHeroContract.View mContractView;
    private GameLogic mGameLogic;

    public ChooseHeroPresenter (ChooseHeroContract.View contractView, GameLogic gameLogic) {
        mContractView = contractView;
        contractView.setPresenter(this);

        mGameLogic = gameLogic;
    }

    @Override
    public void back () {
        mContractView.back();
    }

    @Override
    public void selectHeroOne () {
        selectHero(HeroCellType.HERO_ONE);
    }

    @Override
    public void selectHeroTwo () {
        selectHero(HeroCellType.HERO_TWO);
    }

    @Override
    public void selectHeroThree () {
        selectHero(HeroCellType.HERO_THREE);
    }

    @Override
    public void selectHeroFour () {
        selectHero(HeroCellType.HERO_FOUR);
    }

    @Override
    public void selectHeroFive () {
        selectHero(HeroCellType.HERO_FIVE);
    }

    @Override
    public void selectHeroSix () {
        selectHero(HeroCellType.HERO_SIX);
    }

    private void selectHero (HeroCellType heroCellType) {
        mGameLogic.clearHeroCellTypes();
        mGameLogic.addHeroCellType(heroCellType);
        mContractView.openChooseLevel();
    }

}