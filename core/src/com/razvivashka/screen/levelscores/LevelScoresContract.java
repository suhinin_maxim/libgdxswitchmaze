package com.razvivashka.screen.levelscores;

/**
 * Created by maxim on 29.09.2017.
 */

public interface LevelScoresContract {

    interface View {

        void setPresenter(LevelScoresContract.Presenter presenter);
        void openNextLevel();
        void openChooseLevelDifficulty();

    }

    interface Presenter {

        void openNextLevel();

    }

}
