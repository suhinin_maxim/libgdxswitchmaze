package com.razvivashka.screen.levelscores;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;

/**
 * Created by maxim on 29.09.2017.
 */

public class LevelScoresScreen extends GameScreen<GdxGame> implements LevelScoresContract.View {

    // region Fields

    private final int ARROW_ORIGIN_LEFT = 116;
    private final int ARROW_ORIGIN_BOTTOM = 112;
    private final int ARROW_LEFT = 529;
    private final int ARROW_BOTTOM = 996;

    private LevelScoresScreen.InteractionListener mInteractionListener;
    private LevelScoresContract.Presenter mPresenter;

    private Image mArrow;

    // endregion


    // region constructors

    public LevelScoresScreen(GdxGame game) {
        super(game);
        mInteractionListener = game;
    }

    // endregion

    
    // region fragment lifecycle


    @Override
    public void show() {
        super.show();

        createArrow();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        mPresenter.openNextLevel();

        return true;
    }

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_level_scores.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }

    //endregion

    // region implements YouLoseContract.View

    @Override
    public void setPresenter(LevelScoresContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void openNextLevel() {
        mInteractionListener.onLevelScoresOpenNextLevel();
    }

    @Override
    public void openChooseLevelDifficulty() {
        mInteractionListener.onLevelScoresOpenChooseDifficulty();
    }

    // endregion


    // region create view

    private void createArrow() {
        Texture arrow = GdxGame.assets.get("screen/levelscores/arrow.png", Texture.class);
        mArrow = new Image(new TextureRegion(arrow));
        mArrow.setOrigin(ARROW_ORIGIN_LEFT, ARROW_ORIGIN_BOTTOM);
        mArrow.setPosition(ARROW_LEFT - ARROW_ORIGIN_LEFT, ARROW_BOTTOM - ARROW_ORIGIN_BOTTOM);

        stage.addActor(mArrow);
    }

    // endregion


    // region interfaces

    public interface InteractionListener {

        void onLevelScoresOpenNextLevel();
        void onLevelScoresOpenChooseDifficulty();

    }

    // endregion

}
