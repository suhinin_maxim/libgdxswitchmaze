package com.razvivashka.screen.levelscores;

import com.razvivashka.logic.GameLogic;

/**
 * Created by maxim on 29.09.2017.
 */

public class LevelScoresPresenter implements LevelScoresContract.Presenter {

    //region Fields

    private LevelScoresContract.View mLevelScoresView;
    private GameLogic mGameLogic;

    //endregion


    //region Constructors

    public LevelScoresPresenter(LevelScoresContract.View nextLevelView, GameLogic gameLogic) {
        mGameLogic = gameLogic;

        mLevelScoresView = nextLevelView;
        mLevelScoresView.setPresenter(this);
    }

    //endregion


    // region implements LevelCompleteContract.Presenter

    @Override
    public void openNextLevel() {
        boolean hasNextLevel = mGameLogic.loadNextLevel();
        if (hasNextLevel == true) {
            mLevelScoresView.openNextLevel();
        } else {
            mLevelScoresView.openChooseLevelDifficulty();
        }
    }


    //endregion
}
