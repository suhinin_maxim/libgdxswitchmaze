package com.razvivashka.screen.pause;


import com.razvivashka.logic.GameLogic;

/**
 * Created by maxim on 15.09.2017.
 */

public class PausePresenter implements PauseContract.Presenter {

    //region Fields

    private PauseContract.View mPauseView;
    private GameLogic mGameLogic;

    //endregion


    //region Constructors

    public PausePresenter(PauseContract.View pauseView, GameLogic gameLogic) {
        mGameLogic = gameLogic;

        mPauseView = pauseView;
        mPauseView.setPresenter(this);
    }

    //endregion


    // region implements PauseContract.Presenter

    @Override
    public void resume() {
        mPauseView.openGameBoardFragment();
    }

    @Override
    public void replayLevel() {
        mGameLogic.replayLevel();
        mPauseView.openGameBoardFragment();
    }

    @Override
    public void close() {
        mPauseView.openChooseLevelFragment();
    }

    // endregion

}
