package com.razvivashka.screen.pause;

/**
 * Created by maxim on 15.09.2017.
 */

public interface PauseContract {

    interface View {

        void setPresenter(PauseContract.Presenter presenter);
        void openGameBoardFragment();
        void openChooseLevelFragment();

    }

    interface Presenter {

        void resume();
        void replayLevel();
        void close();

    }

}
