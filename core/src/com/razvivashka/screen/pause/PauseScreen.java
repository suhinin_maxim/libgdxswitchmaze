package com.razvivashka.screen.pause;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;
import com.razvivashka.consts.ViewConst;

/**
 * Created by maxim on 15.09.2017.
 */

public class PauseScreen extends GameScreen<GdxGame> implements PauseContract.View {

    // region Fields

    private PauseScreen.InteractionListener mInteractionListener;
    private PauseContract.Presenter mPresenter;

    // endregion

    public PauseScreen(GdxGame gdxGame) {
        super(gdxGame);
        mInteractionListener = game;
    }

    @Override
    public void show() {
        super.show();

        createGoBtn();
        createReplayBtn();
        createCloseBtn();
    }

    // region fragment lifecycle

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_pause.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }


    //endregion

    // region implements PauseContract.View

    @Override
    public void setPresenter(PauseContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void openGameBoardFragment() {
        mInteractionListener.onPauseOpenGameBoard();
    }

    @Override
    public void openChooseLevelFragment() {
        mInteractionListener.onPauseOpenChooseLevel();
    }

    // endregion


    // region create view

    private void createGoBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/pause/btn_go.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/pause/btn_go_pressed.png", Texture.class)));

        ImageButton button = new ImageButton(style);
        button.setSize(650, 520);

        float left = stage.getWidth()/2f - button.getWidth()/2f;
        float bottom = stage.getHeight()/2f - button.getHeight()/2f;

        button.setPosition(left, bottom);
        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.resume();
            }
        });

        stage.addActor(button);
    }

    private void createReplayBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_home.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_home_pressed.png", Texture.class)));

        ImageButton button = new ImageButton(style);
        button.setSize(ViewConst.SMALL_BUTTON_SIZE, ViewConst.SMALL_BUTTON_SIZE);
        button.setPosition(ViewConst.SCREEN_PADDING, ViewConst.SCREEN_PADDING);
        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.replayLevel();
            }
        });

        stage.addActor(button);
    }

    private void createCloseBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_close.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_close_pressed.png", Texture.class)));

        ImageButton button = new ImageButton(style);
        button.setSize(ViewConst.SMALL_BUTTON_SIZE, ViewConst.SMALL_BUTTON_SIZE);
        button.setPosition(stage.getWidth() - ViewConst.SMALL_BUTTON_SIZE - ViewConst.SCREEN_PADDING, ViewConst.SCREEN_PADDING);
        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.close();
            }
        });

        stage.addActor(button);
    }

    // endregion

    public interface InteractionListener {

        void onPauseOpenGameBoard();
        void onPauseOpenChooseLevel();

    }

}
