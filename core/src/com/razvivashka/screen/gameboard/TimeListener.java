package com.razvivashka.screen.gameboard;

/**
 * Created by maxim on 24.09.2017.
 */

public interface TimeListener {

    void onTimeChanged(long millis);

}
