package com.razvivashka.screen.gameboard;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razvivashka.GdxGame;

/**
 * Created by admin on 11.09.17.
 */

public class NumberView extends Actor {

    private int mDigitPadding = 10;
    private boolean mAutoWidth = false;
    private float mDigitWidth;
    private float mDigitHeight;
    private int mNumber;

    private Image[] mDigitViews;
    private int[] mDigits;

    private String[] digitsResIds;

    public NumberView() {
        initDigitsRes();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        for (Image digitView : mDigitViews) {
            digitView.draw(batch, parentAlpha);
        }
    }

    public void setDigitPadding(int digitPadding) {
        mDigitPadding = digitPadding;
    }

    public void setAutoWidth(boolean autoWidth) {
        mAutoWidth = autoWidth;
    }

    public void setDigitWidth(float digitWidth) {
        mDigitWidth = digitWidth;
    }

    public void setDigitHeight(float digitHeight){
        mDigitHeight = digitHeight;
    }

    public void setNumber (int number) {
        if (number < 0) {
            throw new IllegalArgumentException("Error! Incorrect number.");
        }

        mNumber = number;
        mDigits = createDigits(number);
        mDigitViews = createDigitViews(mDigits);
    }

    public int getNumber () {
        return mNumber;
    }

    private int[] createDigits(int number) {
        int length = 1;
        if (number > 0) {
            length = (int)(Math.log10(number) + 1);
        }

        int[] digits = new int[length];
        int pos = 0;
        while (number > 0) {
            digits[pos] = number % 10;
            number = number / 10;

            pos++;
        }

        return digits;
    }

    private Image[] createDigitViews(int[] digits) {
        if (mAutoWidth == true) {
            mDigitWidth = getWidth()/digits.length - 2*mDigitPadding;
            mDigitHeight = getHeight() - 2*mDigitPadding;
        }

        Image[] digitViews = new Image[digits.length];
        for (int pos=0; pos<digitViews.length; pos++) {
            Image digitView = new Image();
            digitView.setSize(mDigitWidth, mDigitHeight);
            digitView.setPosition(getX() + (((digits.length - pos)*2) - 1)*mDigitPadding + (digits.length - pos - 1)*mDigitWidth, getY() + mDigitPadding);
            digitViews[pos] = digitView;

            setDigit(digitView, digits[pos]);
        }

        return digitViews;
    }

    private void setDigit (Image digitView, int digit) {
        String resId = digitsResIds[digit];
        if (resId != null) {
            Texture texture = GdxGame.assets.get(resId, Texture.class);
            digitView.setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
        }
    }

    private void initDigitsRes () {
        digitsResIds = new String[] {
                "common/digit/digit_0.png",
                "common/digit/digit_1.png",
                "common/digit/digit_2.png",
                "common/digit/digit_3.png",
                "common/digit/digit_4.png",
                "common/digit/digit_5.png",
                "common/digit/digit_6.png",
                "common/digit/digit_7.png",
                "common/digit/digit_8.png",
                "common/digit/digit_9.png",
        };
    }

}
