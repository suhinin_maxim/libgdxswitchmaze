package com.razvivashka.screen.gameboard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;
import com.razvivashka.screen.gameboard.items.BoardItem;
import com.razvivashka.screen.gameboard.items.GameBoardItemView;
import com.razvivashka.screen.gameboard.items.HeroItem;
import com.razvivashka.screen.gameboard.items.StaticCellItem;


/**
 * Created by maxim on 23.09.2017.
 */

public class GameBoardScreen extends GameScreen<GdxGame> implements GameBoardContract.View {

    // region Fields

    private final float GAME_FIELD_LEFT = 63.0f;
    private final float GAME_FIELD_BOTTOM = 237.0f;
    private final float GAME_FIELD_WIDTH = 959.0f;
    private final float GAME_FIELD_HEIGHT = 1506.0f;

    private final float GAME_BOARD_SIZE = GAME_FIELD_WIDTH;

    private final float BONUS_FIELD_LEFT = 205.0f;
    private final float BONUS_FIELD_BOTTOM = 1776.0f;
    private final float BONUS_FIELD_WIDTH = 102.0f;
    private final float BONUS_FIELD_HEIGHT = 102.0f;

    private final float GOLD_FIELD_LEFT = 499.0f;
    private final float GOLD_FIELD_BOTTOM = 1776.0f;
    private final float GOLD_FIELD_WIDTH = 102.0f;
    private final float GOLD_FIELD_HEIGHT = 102.0f;

    private final float MINUTE_FIELD_LEFT = 783.0f;
    private final float MINUTE_FIELD_BOTTOM = 1776.0f;
    private final float MINUTE_FIELD_WIDTH = 102.0f;
    private final float MINUTE_FIELD_HEIGHT = 102.0f;

    private final float SECOND_FIELD_LEFT = 920.0f;
    private final float SECOND_FIELD_BOTTOM = 1776.0f;
    private final float SECOND_FIELD_WIDTH = 102.0f;
    private final float SECOND_FIELD_HEIGHT = 102.0f;

    private final float BENZINE_PROGRESS_LEFT = 221.0f;
    private final float BENZINE_PROGRESS_BOTTOM = 48.0f;
    private final float BENZINE_PROGRESS_WIDTH = 820.0f;
    private final float BENZINE_PROGRESS_HEIGHT = 40.0f;

    private InteractionListener mInteractionListener;
    GameBoardContract.Presenter mPresenter;

    private NumberView mBonusView;
    private NumberView mGoldView;
    private NumberView mMinutesView;
    private NumberView mSecondsView;
    private ProgressBar mBenzineProgress;

    // endregion

    public GameBoardScreen (GdxGame gdxGame) {
        super(gdxGame);
        mInteractionListener = game;
    }

    // region lifecycle


    @Override
    public void show() {
        createTopPanel();
        createBoard();
        createBottomPanel();
    }

    @Override
    public void pause() {
        super.pause();

        mPresenter.pause();
    }

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_game_board.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }

    @Override
    protected boolean onBackPressed() {
        mPresenter.back();

        return true;
    }

    // endregion

    @Override
    public void setPresenter(GameBoardContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void moveHeroView(HeroItem item) {
        float cellSize = GAME_BOARD_SIZE / mPresenter.getFieldSize();
        float viewSize = cellSize / 3f;

        float left = cellSize * item.getPosJ() + viewSize * item.getCellJ();
        float bottom = cellSize * (mPresenter.getFieldSize() - item.getPosI() - 1) + viewSize * (2 - item.getCellI());
        item.getView().setPosition(left + GAME_FIELD_LEFT, bottom + GAME_FIELD_BOTTOM + GAME_FIELD_HEIGHT - GAME_BOARD_SIZE);
    }

    @Override
    public GameBoardItemView getBoardCellView(final BoardItem item) {
        String bgFilePath;
        switch (item.getImageType()) {
            case CORNER:
                bgFilePath = "screen/gameboard/board/corner.png";
                break;
            case LINE:
                bgFilePath = "screen/gameboard/board/line.png";
                break;
            case CROSSROAD:
                bgFilePath = "screen/gameboard/board/crossroad.png";
                break;
            case TEE:
                bgFilePath = "screen/gameboard/board/tee.png";
                break;
            default:
            case GROUND:
                bgFilePath = "screen/gameboard/board/ground.png";
                break;
            case START:
                bgFilePath = "screen/gameboard/board/start.png";
                break;
            case FINISH:
                bgFilePath = "screen/gameboard/board/finish.png";
                break;
        }

        float cellSize = GAME_BOARD_SIZE / mPresenter.getFieldSize();

        final GameBoardItemView view = new GameBoardItemView(new TextureRegion(GdxGame.assets.get(bgFilePath, Texture.class)));
        view.setSize(cellSize, cellSize);

        float left = cellSize * item.getPosJ();
        float bottom = cellSize * (mPresenter.getFieldSize() - item.getPosI() - 1);
        view.setPosition(left + GAME_FIELD_LEFT, bottom + GAME_FIELD_BOTTOM + GAME_FIELD_HEIGHT - GAME_BOARD_SIZE);
        view.setOrigin(cellSize/2f, cellSize/2f);
        view.setRotation(item.getDegree());
        view.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                item.getOnClickListener().onClick(view);
            }
        });

        stage.addActor(view);

        return view;
    }

    @Override
    public GameBoardItemView getHeroCellView(HeroItem item) {
        String bgFilePath = null;
        switch (item.getImageType()) {
            case HERO_ONE:
                bgFilePath = "screen/gameboard/heroes/hero_one.png";
                break;
            case HERO_TWO:
                bgFilePath = "screen/gameboard/heroes/hero_two.png";
                break;
            case HERO_THREE:
                bgFilePath = "screen/gameboard/heroes/hero_three.png";
                break;
            case HERO_FOUR:
                bgFilePath = "screen/gameboard/heroes/hero_four.png";
                break;
            case HERO_FIVE:
                bgFilePath = "screen/gameboard/heroes/hero_five.png";
                break;
            case HERO_SIX:
                bgFilePath = "screen/gameboard/heroes/hero_six.png";
                break;
        }

        float cellSize = GAME_BOARD_SIZE / mPresenter.getFieldSize();
        float viewSize = cellSize / 3f;

        GameBoardItemView view = new GameBoardItemView(new TextureRegion(GdxGame.assets.get(bgFilePath, Texture.class)));
        view.setSize(viewSize, viewSize);
        view.setTouchable(Touchable.disabled);

        float left = cellSize * item.getPosJ() + viewSize * item.getCellJ();
        float bottom = cellSize * (mPresenter.getFieldSize() - item.getPosI() - 1) + viewSize * (2 - item.getCellI());
        view.setPosition(left + GAME_FIELD_LEFT, bottom + GAME_FIELD_BOTTOM + GAME_FIELD_HEIGHT - GAME_BOARD_SIZE);

        stage.addActor(view);

        return view;
    }

    @Override
    public GameBoardItemView getStaticCellView(StaticCellItem item) {
        String bgFilePath = null;
        switch (item.getImageType()) {
            case BONUS:
                bgFilePath = "screen/gameboard/static/bonus1.png";
                break;
            case GOLD:
                bgFilePath = "screen/gameboard/static/coin.png";
                break;
            case ADD_TIME_5:
                bgFilePath = "screen/gameboard/static/add_time_bonuse.png";
                break;
            case ADD_TIME_10:
                bgFilePath = "screen/gameboard/static/add_time_bonuse.png";
                break;
        }

        float cellSize = GAME_BOARD_SIZE / mPresenter.getFieldSize();
        float viewSize = cellSize / 3f;

        GameBoardItemView view = new GameBoardItemView(new TextureRegion(GdxGame.assets.get(bgFilePath, Texture.class)));
        view.setSize(viewSize, viewSize);
        view.setTouchable(Touchable.disabled);

        float left = cellSize * item.getPosJ() + viewSize;
        float bottom = cellSize * (mPresenter.getFieldSize() - item.getPosI() - 1) + viewSize;
        view.setPosition(left + GAME_FIELD_LEFT, bottom + GAME_FIELD_BOTTOM + GAME_FIELD_HEIGHT - GAME_BOARD_SIZE);

        stage.addActor(view);

        return view;
    }

    @Override
    public void setBonusCount(int count) {
        mBonusView.setNumber(count);
    }

    @Override
    public void setGoldCount(int count) {
        mGoldView.setNumber(count);
    }

    @Override
    public void setBenzineProgress(final int percent) {
        mBenzineProgress.setValue(percent);
    }

    @Override
    public void setTimeLeft(final long millis) {
        int minutes = (int) ((millis / (1000 * 60)) % 60);
        if (mMinutesView.getNumber() != minutes) {
            mMinutesView.setNumber(minutes);
        }

        int seconds = (int) (millis / 1000) % 60 ;
        if (mSecondsView.getNumber() != seconds) {
            mSecondsView.setNumber(seconds);
        }
    }

    @Override
    public void openNextLevelFragment() {
        mInteractionListener.onGameBoardShowNextLevel();
    }

    @Override
    public void openYouLoseFragment() {
        mInteractionListener.onGameBoardOpenYouLose();
    }

    @Override
    public void openChooseLevelFragment() {
        mInteractionListener.onGameBoardOpenChooseLevel();
    }

    @Override
    public void openPauseFragment() {
        mInteractionListener.onGameBoardPause();
    }

    // region create view

    private void createTopPanel() {
        // create treasures view
        mBonusView = new NumberView();
        mBonusView.setAutoWidth(true);
        mBonusView.setDigitPadding(5);
        mBonusView.setSize(BONUS_FIELD_WIDTH, BONUS_FIELD_HEIGHT);
        mBonusView.setPosition(BONUS_FIELD_LEFT, BONUS_FIELD_BOTTOM);
        stage.addActor(mBonusView);

        mBonusView.setNumber(0);

        // create gold view
        mGoldView = new NumberView();
        mGoldView.setAutoWidth(true);
        mGoldView.setDigitPadding(5);
        mGoldView.setSize(GOLD_FIELD_WIDTH, GOLD_FIELD_HEIGHT);
        mGoldView.setPosition(GOLD_FIELD_LEFT, GOLD_FIELD_BOTTOM);
        stage.addActor(mGoldView);

        mGoldView.setNumber(0);

        // create minutes view
        mMinutesView = new NumberView();
        mMinutesView.setAutoWidth(true);
        mMinutesView.setDigitPadding(5);
        mMinutesView.setSize(MINUTE_FIELD_WIDTH, MINUTE_FIELD_HEIGHT);
        mMinutesView.setPosition(MINUTE_FIELD_LEFT, MINUTE_FIELD_BOTTOM);
        stage.addActor(mMinutesView);

        mMinutesView.setNumber(0);

        // create seconds view
        mSecondsView = new NumberView();
        mSecondsView.setAutoWidth(true);
        mSecondsView.setDigitPadding(5);
        mSecondsView.setSize(SECOND_FIELD_WIDTH, SECOND_FIELD_HEIGHT);
        mSecondsView.setPosition(SECOND_FIELD_LEFT, SECOND_FIELD_BOTTOM);
        stage.addActor(mSecondsView);

        mSecondsView.setNumber(0);
    }

    private void createBoard() {
        if (mPresenter != null) {
            mPresenter.createBoard();
            mPresenter.createHeroes();
            mPresenter.createStaticCells();

            mPresenter.start();
        }
    }

    private void createBottomPanel() {
        // create progressbar style
        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture("screen/gameboard/bg_benzine_progress.png")));

        ProgressBar.ProgressBarStyle progressBarStyle = new ProgressBar.ProgressBarStyle();
        progressBarStyle.background = drawable;

        Pixmap pixmap = new Pixmap(0, 20, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.GREEN);
        pixmap.fill();
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));
        pixmap.dispose();
        progressBarStyle.knob = drawable;

        pixmap = new Pixmap(100, 20, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.GREEN);
        pixmap.fill();
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));
        pixmap.dispose();
        progressBarStyle.knobBefore = drawable;

        mBenzineProgress = new ProgressBar(0f, 100f, 1f, false, progressBarStyle);
        mBenzineProgress.setAnimateDuration(0.25f);
        mBenzineProgress.setSize(BENZINE_PROGRESS_WIDTH, BENZINE_PROGRESS_HEIGHT);
        mBenzineProgress.setPosition(BENZINE_PROGRESS_LEFT, BENZINE_PROGRESS_BOTTOM);

        stage.addActor(mBenzineProgress);
    }

    // endregion


    public interface InteractionListener {

        void onGameBoardShowNextLevel();
        void onGameBoardOpenYouLose();
        void onGameBoardOpenChooseLevel();
        void onGameBoardPause();

    }

}
