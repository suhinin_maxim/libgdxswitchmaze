package com.razvivashka.screen.gameboard;

import com.razvivashka.screen.gameboard.items.BoardItem;
import com.razvivashka.screen.gameboard.items.BoardItemView;
import com.razvivashka.screen.gameboard.items.HeroItem;
import com.razvivashka.screen.gameboard.items.StaticCellItem;


public interface GameBoardContract {

    interface View {

        void setPresenter(GameBoardContract.Presenter presenter);

        BoardItemView getHeroCellView(HeroItem item);
        BoardItemView getBoardCellView(BoardItem item);
        BoardItemView getStaticCellView(StaticCellItem item);

        void openNextLevelFragment();
        void openYouLoseFragment();
        void openChooseLevelFragment();
        void openPauseFragment();

        void setBonusCount(int count);
        void setGoldCount(int count);
        void setBenzineProgress(int percent);
        void setTimeLeft(long millis);
        void moveHeroView(HeroItem item);
    }

    interface Presenter {

        void back();

        void createBoard();
        void createHeroes();
        void createStaticCells();
        void start();
        void pause();
        void stop();

        int getBenzineProgress();
        long getTimeLeft();
        int getFieldSize();

    }

}
