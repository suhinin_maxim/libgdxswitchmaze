package com.razvivashka.screen.gameboard;

import com.badlogic.gdx.Gdx;
import com.razvivashka.services.AdvertisingOnClose;
import com.razvivashka.services.AdvertisingService;
import com.razvivashka.services.Logger;
import com.razvivashka.logic.GameLogic;
import com.razvivashka.logic.domaindata.GameBoardCell;
import com.razvivashka.logic.domaindata.GameBoardCellType;
import com.razvivashka.logic.domaindata.GameState;
import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.repositories.levels.GameLevelRepository;
import com.razvivashka.screen.gameboard.items.BoardImageType;
import com.razvivashka.screen.gameboard.items.BoardItem;
import com.razvivashka.screen.gameboard.items.BoardItemView;
import com.razvivashka.screen.gameboard.items.HeroImageType;
import com.razvivashka.screen.gameboard.items.HeroItem;
import com.razvivashka.screen.gameboard.items.StaticCellItem;
import com.razvivashka.screen.gameboard.items.StaticImageType;

import java.util.Timer;
import java.util.TimerTask;

import sun.rmi.runtime.Log;

public class GameBoardPresenter implements GameBoardContract.Presenter {

    //region Const

    private static final long SPEED_TIMER_TIME = 500;

    //endregion

    //region Fields

    private Logger mLogger;
    private GameLogic mGameLogic;
    private GameBoardContract.View mGameBoardView;
    private GameLevelRepository mGameLevelRepository;

    private Timer mTimer;
    private RecalcGameSceneTimerTask mMyTimerTask;

    //endregion


    //region Constructors

    public GameBoardPresenter(Logger logger, GameBoardContract.View gameBoardView, GameLogic gameLogic, GameLevelRepository gameLevelRepository) {
        mGameLogic = gameLogic;
        mGameLogic.incTimeLeft(SPEED_TIMER_TIME);

        mGameBoardView = gameBoardView;
        mGameBoardView.setPresenter(this);

        mGameLevelRepository = gameLevelRepository;

        mTimer = new Timer();
        mMyTimerTask = new RecalcGameSceneTimerTask();

        mLogger = logger;
        mLogger.startLevel(gameLogic.getCurrentLevel());
    }

    //endregion

    //region GameBoardContract.Presenter

    @Override
    public void back() {
        stop();
        mGameLogic.setGameState(GameState.LOOSE);
        mGameBoardView.openChooseLevelFragment();
    }

    @Override
    public void createBoard() {
        GameBoardCell[][] field = mGameLogic.getField();

        for (int i=0; i<mGameLogic.getFieldSize(); i++) {
            for (int j=0; j<mGameLogic.getFieldSize(); j++) {
                GameBoardCell cell = field[i][j];

                BoardItem item = new BoardItem();
                item.setPosI(i);
                item.setPosJ(j);
                item.setImageType(getBoardImageType(cell.getType()));
                item.setDegree(cell.getDegree());
                item.setView(mGameBoardView.getBoardCellView(item));
                item.setOnClickListener(new OnClickListenerCell(mGameLogic, i, j));
            }
        }
    }

    @Override
    public void createHeroes() {
        for (HeroCell cell : mGameLogic.getHeroes()) {
            HeroItem item = new HeroItem();
            item.setPosI(cell.getPosI());
            item.setPosJ(cell.getPosJ());
            item.setCellI(cell.getCellI());
            item.setCellJ(cell.getCellJ());
            item.setImageType(getHeroImageType(cell.getType()));
            item.setView(mGameBoardView.getHeroCellView(item));

            cell.setMoveListener(new OnHeroCellMoveListener(item));
        }
    }

    @Override
    public void createStaticCells() {
        for (StaticCell cell : mGameLogic.getStaticCells()) {
            StaticCellItem item = new StaticCellItem();
            item.setPosI(cell.getPosI());
            item.setPosJ(cell.getPosJ());
            item.setImageType(getStaticImageType(cell.getType()));
            item.setView(mGameBoardView.getStaticCellView(item));

            cell.setOnActionListener(new OnStaticActionListener(item));
        }
    }

    @Override
    public void start() {
        mTimer.schedule(mMyTimerTask, SPEED_TIMER_TIME, SPEED_TIMER_TIME);
    }

    @Override
    public void pause() {
        stop();
        mGameLogic.setGameState(GameState.PAUSE);
        mGameBoardView.openPauseFragment();
    }

    @Override
    public void stop() {
        mTimer.cancel();
        mTimer.purge();
    }

    @Override
    public int getBenzineProgress () {
        return (mGameLogic.getCruising() - mGameLogic.getSteps()) * 100 / mGameLogic.getMaxCruising();
    }

    @Override
    public long getTimeLeft() {
        return mGameLogic.getTimeLeft();
    }

    @Override
    public int getFieldSize() {
        return mGameLogic.getFieldSize();
    }

    //endregion


    // region Inner Classes

    class RecalcGameSceneTimerTask extends TimerTask {

        @Override
        public void run() {
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    int savedLevel = mGameLogic.getCurrentLevel();

                    switch (mGameLogic.getGameState()) {
                        case WIN:
                            mLogger.winLevel(savedLevel, mGameLogic.calcScores());
                            mGameLevelRepository.saveCurrentLevel(mGameLogic.getDifficulty(), savedLevel + 1);

                            stop();
                            mGameBoardView.openNextLevelFragment();
                            break;
                        case LOOSE:
                            mLogger.loseLevel(savedLevel);

                            stop();
                            mGameBoardView.openYouLoseFragment();
                            break;
                        default:
                            mGameLogic.gameMove();
                            mGameLogic.incTimeLeft(SPEED_TIMER_TIME);

                            mGameBoardView.setBonusCount(mGameLogic.getCollectedBonusCount(StaticCellType.BONUS.name()));
                            mGameBoardView.setGoldCount(mGameLogic.getCollectedBonusCount(StaticCellType.GOLD.name()));
                            mGameBoardView.setTimeLeft(mGameLogic.getTimeLeft());
                            mGameBoardView.setBenzineProgress(getBenzineProgress());
                            break;
                    }
                }
            });
        }

    }

    //endregion


    // region Service Methods

    private class OnClickListenerCell implements BoardItem.OnClickListener {

        private int posI, posJ;
        private GameLogic gameLogic;

        public OnClickListenerCell(GameLogic gameLogic, int i, int j) {
            this.gameLogic = gameLogic;
            posI = i;
            posJ = j;
        }

        @Override
        public void onClick(BoardItemView view) {
            GameBoardCell cell = gameLogic.move(posI, posJ);

            view.setRotation(cell.getDegree());
        }
    }

    private class OnHeroCellMoveListener implements HeroCell.OnMoveListener {

        private HeroItem item;

        private OnHeroCellMoveListener(HeroItem item) {
            this.item = item;
        }

        @Override
        public void OnMove(HeroCell cell) {
            item.setPosI(cell.getPosI());
            item.setPosJ(cell.getPosJ());
            item.setCellI(cell.getCellI());
            item.setCellJ(cell.getCellJ());

            mGameBoardView.moveHeroView(item);
        }
    }

    private class OnStaticActionListener implements StaticCell.OnActionListener {

        private StaticCellItem item;

        public OnStaticActionListener(StaticCellItem item) {
            this.item = item;
        }

        @Override
        public void onAction(StaticCell cell) {
            item.getView().setVisible(false);
        }
    }

    private BoardImageType getBoardImageType(GameBoardCellType gameBoardCellType) {
        BoardImageType imageType;

        switch (gameBoardCellType) {
            case CORNER:
                imageType = BoardImageType.CORNER;
                break;
            case LINE:
                imageType = BoardImageType.LINE;
                break;
            case CROSSROAD:
                imageType = BoardImageType.CROSSROAD;
                break;
            case TEE:
                imageType = BoardImageType.TEE;
                break;
            case GROUND:
                imageType = BoardImageType.GROUND;
                break;
            case START:
                imageType = BoardImageType.START;
                break;
            case FINISH:
                imageType = BoardImageType.FINISH;
                break;
            default:
                imageType = BoardImageType.GROUND;
        }

        return imageType;
    }

    private StaticImageType getStaticImageType(StaticCellType staticCellType) {
        StaticImageType imageType = null;

        switch (staticCellType) {
            case BONUS:
                imageType = StaticImageType.BONUS;
                break;
            case GOLD:
                imageType = StaticImageType.GOLD;
                break;
            case ADD_TIME_5:
                imageType = StaticImageType.ADD_TIME_5;
                break;
            case ADD_TIME_10:
                imageType = StaticImageType.ADD_TIME_10;
                break;
        }

        return imageType;
    }

    private HeroImageType getHeroImageType (HeroCellType heroCellType) {
        HeroImageType imageType = null;
        switch (heroCellType) {
            case HERO_ONE:
                imageType = HeroImageType.HERO_ONE;
                break;
            case HERO_TWO:
                imageType = HeroImageType.HERO_TWO;
                break;
            case HERO_THREE:
                imageType = HeroImageType.HERO_THREE;
                break;
            case HERO_FOUR:
                imageType = HeroImageType.HERO_FOUR;
                break;
            case HERO_FIVE:
                imageType = HeroImageType.HERO_FIVE;
                break;
            case HERO_SIX:
                imageType = HeroImageType.HERO_SIX;
                break;
        }

        return imageType;
    }

    // endregion

}
