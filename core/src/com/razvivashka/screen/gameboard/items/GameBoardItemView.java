package com.razvivashka.screen.gameboard.items;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by maxim on 25.09.2017.
 */

public class GameBoardItemView extends Image implements BoardItemView {

    public GameBoardItemView(TextureRegion textureRegion) {
        super(textureRegion);

        setScaling(Scaling.fit);
    }

    @Override
    public void setRotation(float degrees) {
        super.setRotation(360 - degrees);
    }
}
