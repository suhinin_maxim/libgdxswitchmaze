package com.razvivashka.screen.gameboard.items;


public class BoardItem {

    private int posI,posJ;
    private BoardImageType imageType;
    private int degree;
    private BoardItemView view;
    private OnClickListener onClickListener;

    public int getPosI() {
        return posI;
    }

    public void setPosI(int posI) {
        this.posI = posI;
    }

    public int getPosJ() {
        return posJ;
    }

    public void setPosJ(int posJ) {
        this.posJ = posJ;
    }

    public BoardImageType getImageType() {
        return imageType;
    }

    public void setImageType(BoardImageType imageType) {
        this.imageType = imageType;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public BoardItemView getView() {
        return view;
    }

    public void setView(BoardItemView view) {
        this.view = view;
    }

    public OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {

        void onClick(BoardItemView view);

    }

}
