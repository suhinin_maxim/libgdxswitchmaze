package com.razvivashka.screen.gameboard.items;

public enum BoardImageType {
    CORNER,
    LINE,
    CROSSROAD,
    TEE,
    GROUND,
    START,
    FINISH
}
