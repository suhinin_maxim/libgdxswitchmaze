package com.razvivashka.screen.gameboard.items;

/**
 * Created by maxim on 25.09.2017.
 */

public interface BoardItemView {

    void setSize(float width, float height);
    void setPosition(float x, float y);
    void setRotation(float degree);
    void setVisible(boolean isVisible);

}
