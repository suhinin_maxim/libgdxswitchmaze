package com.razvivashka.screen.gameboard.items;


public enum StaticImageType {
    BONUS,
    GOLD,
    ADD_TIME_5,
    ADD_TIME_10
}
