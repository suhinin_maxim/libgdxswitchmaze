package com.razvivashka.screen.gameboard.items;


public class HeroItem {

    private int posI,posJ;
    private int cellI, cellJ;
    private HeroImageType imageType;
    private BoardItemView view;

    public int getPosI() {
        return posI;
    }

    public void setPosI(int posI) {
        this.posI = posI;
    }

    public int getPosJ() {
        return posJ;
    }

    public void setPosJ(int posJ) {
        this.posJ = posJ;
    }

    public int getCellI() {
        return cellI;
    }

    public void setCellI(int cellI) {
        this.cellI = cellI;
    }

    public int getCellJ() {
        return cellJ;
    }

    public void setCellJ (int cellJ) {
        this.cellJ = cellJ;
    }

    public HeroImageType getImageType() {
        return imageType;
    }

    public void setImageType(HeroImageType imageType) {
        this.imageType = imageType;
    }

    public BoardItemView getView() {
        return view;
    }

    public void setView(BoardItemView view) {
        this.view = view;
    }
}
