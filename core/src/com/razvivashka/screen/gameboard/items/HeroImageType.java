package com.razvivashka.screen.gameboard.items;

/**
 * Created by admin on 13.09.17.
 */

public enum HeroImageType {
    HERO_ONE,
    HERO_TWO,
    HERO_THREE,
    HERO_FOUR,
    HERO_FIVE,
    HERO_SIX
}
