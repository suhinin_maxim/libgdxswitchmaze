package com.razvivashka.screen.gameboard.items;


public class StaticCellItem {

    private int posI,posJ;
    private StaticImageType imageType;
    private BoardItemView view;

    public int getPosI() {
        return posI;
    }

    public void setPosI(int posI) {
        this.posI = posI;
    }

    public int getPosJ() {
        return posJ;
    }

    public void setPosJ(int posJ) {
        this.posJ = posJ;
    }

    public StaticImageType getImageType() {
        return imageType;
    }

    public void setImageType(StaticImageType imageType) {
        this.imageType = imageType;
    }

    public BoardItemView getView() {
        return view;
    }

    public void setView(BoardItemView view) {
        this.view = view;
    }

}
