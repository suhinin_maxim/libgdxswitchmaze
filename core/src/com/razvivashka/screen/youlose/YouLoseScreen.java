package com.razvivashka.screen.youlose;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;
import com.razvivashka.consts.ViewConst;

/**
 * Created by maxim on 08.09.2017.
 */

public class YouLoseScreen extends GameScreen<GdxGame> implements YouLoseContract.View {

    // region Fields

    private InteractionListener mInteractionListener;
    private YouLoseContract.Presenter mPresenter;

    // endregion


    // region constructors

    public YouLoseScreen(GdxGame game) {
        super(game);
        mInteractionListener = game;

        createReplayBtn();
        createHomeBtn();
    }

    // endregion


    // region fragment lifecycler

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_you_lose.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }

    //endregion

    // region implements YouLoseContract.View

    @Override
    public void setPresenter(YouLoseContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void openChooseDifficultyScreen() {
        mInteractionListener.onYouLoseGoHome();
    }

    @Override
    public void openGameBoardScreen() {
        mInteractionListener.onYouLoseReplayLevel();
    }

    // endregion


    // region create view

    private void createReplayBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/youlose/btn_replay.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("screen/youlose/btn_replay_pressed.png", Texture.class)));

        ImageButton button = new ImageButton(style);
        button.setSize(650, 520);

        float left = stage.getWidth()/2f - button.getWidth()/2f;
        float bottom = stage.getHeight()/2f - button.getHeight()/2f;

        button.setPosition(left, bottom);
        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.replayLevel();
            }
        });

        stage.addActor(button);
    }

    private void createHomeBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_home.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_home_pressed.png", Texture.class)));

        ImageButton button = new ImageButton(style);
        button.setSize(ViewConst.SMALL_BUTTON_SIZE, ViewConst.SMALL_BUTTON_SIZE);
        button.setPosition(ViewConst.SCREEN_PADDING, ViewConst.SCREEN_PADDING);
        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.home();
            }
        });

        stage.addActor(button);
    }

    // endregion


    // region interfaces

    public interface InteractionListener {

        void onYouLoseGoHome();
        void onYouLoseReplayLevel();

    }

    // endregion
}