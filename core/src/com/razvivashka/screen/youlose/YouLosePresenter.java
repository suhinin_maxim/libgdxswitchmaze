package com.razvivashka.screen.youlose;


import com.badlogic.gdx.Gdx;
import com.razvivashka.logic.GameLogic;
import com.razvivashka.services.AdvertisingOnClose;
import com.razvivashka.services.AdvertisingService;

/**
 * Created by maxim on 08.09.2017.
 */

public class YouLosePresenter implements YouLoseContract.Presenter {

    //region Fields

    private YouLoseContract.View mContractView;
    private GameLogic mGameLogic;

    private AdvertisingService mAdvertisingService;

    //endregion


    //region Constructors

    public YouLosePresenter(YouLoseContract.View contractView, GameLogic gameLogic, AdvertisingService advertisingService) {
        mGameLogic = gameLogic;

        mContractView = contractView;
        mContractView.setPresenter(this);

        mAdvertisingService = advertisingService;
    }

    //endregion


    // region implements YouLoseContract.Presenter

    @Override
    public void home() {
        mContractView.openChooseDifficultyScreen();
    }

    @Override
    public void replayLevel() {
        showAdvertising();
    }

    // endregion


    // region Service Methods

    private void showAdvertising() {
        mAdvertisingService.show(new AdvertisingOnClose() {
            @Override
            public void onClose() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        mGameLogic.replayLevel();
                        mContractView.openGameBoardScreen();
                    }
                });
            }
        });
    }

    // endregion

}