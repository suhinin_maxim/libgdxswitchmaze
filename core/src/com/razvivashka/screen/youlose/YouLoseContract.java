package com.razvivashka.screen.youlose;

/**
 * Created by maxim on 08.09.2017.
 */

public class YouLoseContract {

    interface View {

        void setPresenter(YouLoseContract.Presenter presenter);
        void openChooseDifficultyScreen();
        void openGameBoardScreen();

    }

    interface Presenter {

        void home();
        void replayLevel();

    }

}
