package com.razvivashka.screen.levelcomplete;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;
import com.razvivashka.screen.gameboard.NumberView;


public class LevelCompleteScreen extends GameScreen<GdxGame> implements LevelCompleteContract.View {

    // region Fields

    private final int BONUS_FIELD_CENTER_LEFT = 740;
    private final int BONUS_FIELD_CENTER_BOTTOM = 675;
    private final int BONUS_FIELD_WIDTH = 102;
    private final int BONUS_FIELD_HEIGHT = 102;

    private final int BENZINE_FIELD_CENTER_LEFT = 740;
    private final int BENZINE_FIELD_CENTER_BOTTOM = 427;
    private final int BENZINE_FIELD_WIDTH = 102;
    private final int BENZINE_FIELD_HEIGHT = 102;

    private final int GOLD_FIELD_CENTER_LEFT = 740;
    private final int GOLD_FIELD_CENTER_BOTTOM = 187;
    private final int GOLD_FIELD_WIDTH = 102;
    private final int GOLD_FIELD_HEIGHT = 102;

    private InteractionListener mInteractionListener;
    private LevelCompleteContract.Presenter mPresenter;

    private NumberView mBonusView;
    private NumberView mBenzineView;
    private NumberView mGoldView;

    // endregion


    // region constructors

    public LevelCompleteScreen(GdxGame game) {
        super(game);
        mInteractionListener = game;
    }

    // endregion


    // region fragment lifecycle

    @Override
    public void show() {
        createBonusView();
        createBenzineView();
        createGoldView();
    }

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_level_complete.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        mPresenter.openLevelScores();

        return true;
    }

    //endregion


    // region implements LevelCompleteContract.View

    @Override
    public void setPresenter(LevelCompleteContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void openLevelScores() {
        mInteractionListener.onLevelCompleteOpenLevelScores();
    }

    @Override
    public void openChooseLevelDifficulty() {
        mInteractionListener.onLevelCompleteOpenChooseLevelDifficulty();
    }

    // endregion


    // region create View

    private void createBonusView() {
        mBonusView = new NumberView();
        mBonusView.setDigitWidth(30);
        mBonusView.setDigitHeight(60);
        mBonusView.setDigitPadding(10);
        mBonusView.setSize(BONUS_FIELD_WIDTH, BONUS_FIELD_HEIGHT);
        mBonusView.setPosition(BONUS_FIELD_CENTER_LEFT - BONUS_FIELD_WIDTH/2f, BONUS_FIELD_CENTER_BOTTOM - BONUS_FIELD_HEIGHT/2f);
        stage.addActor(mBonusView);

        mBonusView.setNumber(mPresenter.getBonusCount());
    }

    private void createBenzineView() {
        mBenzineView = new NumberView();
        mBenzineView.setDigitWidth(30);
        mBenzineView.setDigitHeight(60);
        mBenzineView.setDigitPadding(10);
        mBenzineView.setSize(BENZINE_FIELD_WIDTH, BENZINE_FIELD_HEIGHT);
        mBenzineView.setPosition(BENZINE_FIELD_CENTER_LEFT - BENZINE_FIELD_WIDTH/2f, BENZINE_FIELD_CENTER_BOTTOM - BENZINE_FIELD_HEIGHT/2f);
        stage.addActor(mBenzineView);

        mBenzineView.setNumber(mPresenter.getBenzineCount());
    }

    private void createGoldView() {
        mGoldView = new NumberView();
        mGoldView.setDigitWidth(30);
        mGoldView.setDigitHeight(60);
        mGoldView.setDigitPadding(10);
        mGoldView.setSize(GOLD_FIELD_WIDTH, GOLD_FIELD_HEIGHT);
        mGoldView.setPosition(GOLD_FIELD_CENTER_LEFT - GOLD_FIELD_WIDTH/2f, GOLD_FIELD_CENTER_BOTTOM - GOLD_FIELD_HEIGHT/2f);
        stage.addActor(mGoldView);

        mGoldView.setNumber(mPresenter.getGoldCount());
    }

    // endregion


    public interface InteractionListener {

        void onLevelCompleteOpenLevelScores();
        void onLevelCompleteOpenChooseLevelDifficulty();                    // TODO temp remove after fix LevelScores

    }

}
