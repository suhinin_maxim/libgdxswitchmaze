package com.razvivashka.screen.levelcomplete;


public interface LevelCompleteContract {

    interface View {

        void setPresenter(LevelCompleteContract.Presenter presenter);
        void openLevelScores();
        void openChooseLevelDifficulty();            // TODO temp remove after fix LevelScoresScree

    }

    interface Presenter {

        void openLevelScores();

        int getBonusCount();
        int getBenzineCount();
        int getGoldCount();

    }

}
