package com.razvivashka.screen.levelcomplete;


import com.badlogic.gdx.Gdx;
import com.razvivashka.logic.GameLogic;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.services.AdvertisingOnClose;
import com.razvivashka.services.AdvertisingService;

public class LevelCompletePresenter implements LevelCompleteContract.Presenter {

    //region Fields

    private LevelCompleteContract.View mView;
    private GameLogic mGameLogic;

    private AdvertisingService mAdvertisingService;

    //endregion


    //region Constructors

    public LevelCompletePresenter(LevelCompleteContract.View levelCompleteView, GameLogic gameLogic, AdvertisingService advertisingService) {
        mGameLogic = gameLogic;

        mView = levelCompleteView;
        mView.setPresenter(this);

        mAdvertisingService = advertisingService;
    }

    //endregion


    // region implements LevelCompleteContract.Presenter

    @Override
    public void openLevelScores() {
        showAdvertising();
    }

    @Override
    public int getBonusCount() {
        return mGameLogic.getCollectedBonusCount(StaticCellType.BONUS.name());
    }

    @Override
    public int getBenzineCount() {
        return mGameLogic.getCruising() - mGameLogic.getSteps();
    }

    @Override
    public int getGoldCount() {
        return mGameLogic.getCollectedBonusCount(StaticCellType.GOLD.name());
    }

    //endregion


    // region Service Methods

    private void showAdvertising() {
        mAdvertisingService.show(new AdvertisingOnClose() {
            @Override
            public void onClose() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        // TODO TEMP BEGIN
                        boolean hasNextLevel = mGameLogic.loadNextLevel();
                        if (hasNextLevel == true) {
                            mView.openLevelScores();
                        } else {
                            mView.openChooseLevelDifficulty();
                        }
                        // TODO TEMP END

                        //mView.openLevelScores();                                  // TODO remove everything else after fix LevelScores Screen
                    }
                });
            }
        });
    }

    // endregion

}
