package com.razvivashka.screen.chooselevel;


import com.razvivashka.logic.GameLogic;
import com.razvivashka.logic.levels.GameLevel;
import com.razvivashka.repositories.levels.GameLevelRepository;

/**
 * Created by maxim on 06.09.2017.
 */

public class ChooseLevelPresenter implements ChooseLevelContract.Presenter {

    // region Fields

    private ChooseLevelContract.View mView;
    private GameLogic mGameLogic;
    private GameLevelRepository mGameLevelRepository;

    private GameLevelCell[] mLevels;

    // endregion Fields


    // region Constructors

    public ChooseLevelPresenter (ChooseLevelContract.View contractView, GameLogic gameLogic, GameLevelRepository gameLevelRepository) {
        mView = contractView;
        mGameLogic = gameLogic;
        mGameLevelRepository = gameLevelRepository;

        contractView.setPresenter(this);
        initLevels();
    }

    // endregion Constructors


    // region implements implements ChooseLevelContract.Presenter

    @Override
    public void back () {
        mView.back();
    }

    @Override
    public void home () {
        mView.home();
    }

    @Override
    public void selectLevel (int level) {
        mGameLogic.loadLevel(level);
        mView.openGameBoardFragment();
    }

    @Override
    public String getDifficultyImageFileName () {
        String fileName = null;

        switch (mGameLogic.getDifficulty()) {
            case EASY:
                fileName = "screen/chooselevel/banner_choose_level_easy.png";
                break;
            case NORMAL:
                fileName = "screen/chooselevel/banner_choose_level_normal.png";
                break;
            case HARD:
                fileName = "screen/chooselevel/banner_choose_level_hard.png";
                break;
        }

        return fileName;
    }

    @Override
    public GameLevelCell[] getLevels() {
        return mLevels;
    }

    // endregion


    // region service methods

    private void initLevels() {
        mLevels = new GameLevelCell[mGameLogic.getLevelCount()];
        int currentLevel = mGameLevelRepository.getSavedCurrentLevel(mGameLogic.getDifficulty());

//        int ind = 0;
//        for (int number=1; number<=mGameLogic.getMaxLevelNumber(); number++) {
//            GameLevel gameLevel = mGameLogic.findLevel(number);
//            if (gameLevel != null) {
//                GameLevelCell gameLevelCell = new GameLevelCell();
//                gameLevelCell.setOpen(number<=currentLevel);
//                gameLevelCell.setLevelNumber(number);
//                gameLevelCell.setPosition(ind+1);
//
//                mLevels[ind] = gameLevelCell;
//                ind++;
//            }
//        }

        int ind = 0;
        int number = 1;
        while(ind<mGameLogic.getLevelCount() && number<=mGameLogic.getMaxLevelNumber()) {
            GameLevel gameLevel = mGameLogic.findLevel(number);
            if (gameLevel == null) {
                number++;
                continue;
            }

            GameLevelCell gameLevelCell = new GameLevelCell();
            gameLevelCell.setOpen(gameLevel.getLevel()<=currentLevel);
            gameLevelCell.setLevelNumber(gameLevel.getLevel());
            gameLevelCell.setPosition(ind+1);

            mLevels[ind] = gameLevelCell;
            ind++;
            number = gameLevel.getLevel() + 1;
        }

    }

    // endregion

}
