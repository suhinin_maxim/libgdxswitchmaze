package com.razvivashka.screen.chooselevel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;
import com.razvivashka.consts.ViewConst;
import com.razvivashka.screen.gameboard.NumberView;

/**
 * Created by maxim on 06.09.2017.
 */

public class ChooseLevelScreen extends GameScreen<GdxGame> implements ChooseLevelContract.View {

    // region fields

    private ChooseLevelContract.Presenter mPresenter;
    private ChooseLevelScreen.InteractionListener mInteractionListener;

    private final float BOTTOM_BAR_MARGIN = 100.0f;
    private final int TABLE_COLUMNS_COUNT = 3;

    private Table table;
    private ScrollPane scrollPane;

    private ImageButton btnHome;
    private Image imageDifficulty;

    // endregion


    // region constructors

    public ChooseLevelScreen(GdxGame game) {
        super(game);
        mInteractionListener = game;
    }

    // endregion


    // region fragment lifecycler


    @Override
    public void show() {
        createListView();
        createBottomBar();
    }

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        Texture backgroundTexture = GdxGame.assets.get("common/backgrounds/bg_green_road.png", Texture.class);
        stage.getBatch().draw(backgroundTexture, 0, 0);
        stage.getBatch().end();
    }

    @Override
    protected boolean onBackPressed() {
        mPresenter.back();

        return true;
    }

    // endregion


    // region implements ChooseLevelContract.View

    @Override
    public void setPresenter(ChooseLevelContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void back () {
        mInteractionListener.onChooseLevelBackPressed();
    }

    @Override
    public void home () {
        mInteractionListener.onChooseLevelOpenChooseDifficulty();
    }

    @Override
    public void openGameBoardFragment () {
        mInteractionListener.onChooseLevelOpenGameBoard();
    }

    // endregion

    // region create view

    private void createListView() {
        table = new Table();
        table.align(Align.top);
        int rowCount = mPresenter.getLevels().length / TABLE_COLUMNS_COUNT + ((mPresenter.getLevels().length % TABLE_COLUMNS_COUNT) > 0 ? 1 : 0);

        float cellSpace = 30.0f;
        float scrollPaneWidth = stage.getWidth() - 2*ViewConst.SCREEN_PADDING;
        float cellSize = (scrollPaneWidth - (TABLE_COLUMNS_COUNT + 1)*cellSpace) / (float) TABLE_COLUMNS_COUNT;

        for (int i=0; i<rowCount; i++) {
            for (int j=0; j<TABLE_COLUMNS_COUNT; j++) {
                int position = i * TABLE_COLUMNS_COUNT + j;
                if (position < mPresenter.getLevels().length) {
                    final GameLevelCell gameLevelCell = mPresenter.getLevels()[position];

                    Group levelCellBtn = createLevelCellBtn(gameLevelCell, cellSize);
                    levelCellBtn.addListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            mPresenter.selectLevel(gameLevelCell.getLevelNumber());
                        }
                    });

                    table.add(levelCellBtn).size(cellSize).space(cellSpace);
                }
            }
            table.row();
        }

        scrollPane = new ScrollPane(table);
        scrollPane.setScrollingDisabled(false, false);
        scrollPane.setSize(scrollPaneWidth, stage.getHeight() - BOTTOM_BAR_MARGIN - ViewConst.SMALL_BUTTON_SIZE - ViewConst.SCREEN_PADDING);
        scrollPane.setPosition(ViewConst.SCREEN_PADDING, BOTTOM_BAR_MARGIN + ViewConst.SMALL_BUTTON_SIZE);
        scrollPane.setSmoothScrolling(true);

        stage.addActor(scrollPane);
    }

    private Group createLevelCellBtn(GameLevelCell levelCell, float cellSize) {
        Group cell = new Group();

        // create ImageButton
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();

        String upFileName;
        String downFileName;
        if (levelCell.isOpen()) {
            upFileName = "screen/chooselevel/btn_main_road.png";
            downFileName = "screen/chooselevel/btn_main_road_pressed.png";
        } else {
            upFileName = "screen/chooselevel/btn_stop.png";
            downFileName = "screen/chooselevel/btn_stop_pressed.png";
        }

        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get(upFileName, Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get(downFileName, Texture.class)));

        ImageButton imageButton = new ImageButton(style);
        imageButton.setSize(cellSize, cellSize);
        cell.addActor(imageButton);

        // create treasures view
        NumberView numberView = new NumberView();
        numberView.setDigitPadding(4);
        numberView.setDigitWidth(30);
        numberView.setDigitHeight(50);
        numberView.setPosition(15, 15);
        cell.addActor(numberView);

        numberView.setNumber(levelCell.getPosition());

        return cell;
    }

    private void createBottomBar() {
        createHomeBtn();
        createDifficultyImage();
    }

    private void createHomeBtn() {
        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_home.png", Texture.class)));
        style.down = new TextureRegionDrawable(new TextureRegion(GdxGame.assets.get("common/buttons/btn_home_pressed.png", Texture.class)));

        btnHome = new ImageButton(style);
        btnHome.setSize( ViewConst.SMALL_BUTTON_SIZE,  ViewConst.SMALL_BUTTON_SIZE);
        btnHome.setPosition(ViewConst.SCREEN_PADDING, ViewConst.SCREEN_PADDING);
        btnHome.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mPresenter.home();
            }
        });

        stage.addActor(btnHome);
    }

    private void createDifficultyImage() {
        float leftMargin = 100.0f;

        float width = stage.getWidth() - 3*ViewConst.SCREEN_PADDING - leftMargin - ViewConst.SMALL_BUTTON_SIZE;
        float height = ViewConst.SMALL_BUTTON_SIZE;
        float left = 2*ViewConst.SCREEN_PADDING + ViewConst.SMALL_BUTTON_SIZE + leftMargin;
        float bottom = ViewConst.SCREEN_PADDING;

        imageDifficulty = new Image(new TextureRegion(GdxGame.assets.get(mPresenter.getDifficultyImageFileName(), Texture.class)));
        imageDifficulty.setScaling(Scaling.fit);
        imageDifficulty.setSize(width, height);
        imageDifficulty.setAlign(Align.right);
        imageDifficulty.setPosition(left, bottom);

        stage.addActor(imageDifficulty);
    }

    // endregion


    public interface InteractionListener {
        
        void onChooseLevelBackPressed();
        void onChooseLevelOpenChooseDifficulty();
        void onChooseLevelOpenGameBoard();
        
    }

}
