package com.razvivashka.screen.chooselevel;


/**
 * Created by maxim on 06.09.2017.
 */

public class ChooseLevelContract {

    interface View {

        void setPresenter(ChooseLevelContract.Presenter presenter);
        void back();
        void home();
        void openGameBoardFragment();

    }

    interface Presenter {

        void back();
        void home();
        void selectLevel(int level);

        String getDifficultyImageFileName();
        GameLevelCell[] getLevels();

    }

}
