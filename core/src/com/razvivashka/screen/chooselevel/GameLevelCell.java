package com.razvivashka.screen.chooselevel;

/**
 * Created by maxim on 24.09.2017.
 */

public class GameLevelCell {

    private boolean isOpen;
    private int levelNumber;
    private int position;

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(int levelNumber) {
        this.levelNumber = levelNumber;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
