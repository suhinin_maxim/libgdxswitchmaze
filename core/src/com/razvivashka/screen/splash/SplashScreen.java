package com.razvivashka.screen.splash;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razvivashka.GdxGame;
import com.razvivashka.base.GameScreen;

/**
 * Created by maxim on 24.09.2017.
 */

public class SplashScreen extends GameScreen<GdxGame> {

    // region fields

    private InteractionListener interactionListener;

    private Texture background;
    private ProgressBar progressBar;

    private final int LOAD_PROGRESS_LEFT = 60;
    private final int LOAD_PROGRESS_BOTTOM = 126;
    private final int LOAD_PROGRESS_WIDTH = 960;
    private final int LOAD_PROGRESS_HEIGHT = 130;

    private float minimumShowTime = 1f;

    // endregion


    // region constructors

    public SplashScreen(GdxGame game) {
        super(game);
        interactionListener = game;

        background = new Texture(Gdx.files.internal("common/backgrounds/bg_splash.png"));

        createProgressBar();
    }

    // endregion


    // region lifecycle

    @Override
    protected void renderScene(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.getBatch().begin();
        stage.getBatch().draw(background, 0, 0);
        stage.getBatch().end();

        if(GdxGame.assets.update() && minimumShowTime <= 0) {
            interactionListener.onSplashScreenOpenChooseDifficulty();
        }

        progressBar.setValue(GdxGame.assets.getProgress());
        minimumShowTime -= delta;
    }

    // endregion


    // region create view

    private void createProgressBar() {
        Texture bgLoadProgress = new Texture(Gdx.files.internal("screen/splash/bg_load_progress.png"));
        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(bgLoadProgress));

        ProgressBar.ProgressBarStyle progressBarStyle = new ProgressBar.ProgressBarStyle();
        progressBarStyle.background = drawable;

        Pixmap pixmap = new Pixmap(0, 20, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.GREEN);
        pixmap.fill();
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));
        pixmap.dispose();

        progressBarStyle.knob = drawable;

        pixmap = new Pixmap(100, 20, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.GREEN);
        pixmap.fill();
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));
        pixmap.dispose();

        progressBarStyle.knobBefore = drawable;

        progressBar = new ProgressBar(0.0f, 1.0f, 0.01f, false, progressBarStyle);
        progressBar.setAnimateDuration(0.25f);
        progressBar.setBounds(LOAD_PROGRESS_LEFT, LOAD_PROGRESS_BOTTOM, LOAD_PROGRESS_WIDTH, LOAD_PROGRESS_HEIGHT);

        stage.addActor(progressBar);
    }

    // endregion


    public interface InteractionListener {

        void onSplashScreenOpenChooseDifficulty ();

    }

}
