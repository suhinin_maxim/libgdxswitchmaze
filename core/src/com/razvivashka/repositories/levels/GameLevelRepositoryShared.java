package com.razvivashka.repositories.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.razvivashka.consts.SharedConst;
import com.razvivashka.logic.domaindata.GameDifficulty;

/**
 * Created by maxim on 03.11.2017.
 */

public class GameLevelRepositoryShared implements GameLevelRepository {

    public static final String SHARED_EASY_CURRENT_LEVEL = "shared_easy_current_level";
    public static final String SHARED_NORMAL_CURRENT_LEVEL = "shared_normal_current_level";
    public static final String SHARED_HARD_CURRENT_LEVEL = "shared_hard_current_level";

    @Override
    public int getSavedCurrentLevel(GameDifficulty difficulty) {
        Preferences preferences = Gdx.app.getPreferences(SharedConst.PREF_NAME);

        switch (difficulty) {
            case EASY:
                return preferences.getInteger(SHARED_EASY_CURRENT_LEVEL, 1);
            case NORMAL:
                return preferences.getInteger(SHARED_NORMAL_CURRENT_LEVEL, 1);
            case HARD:
                return preferences.getInteger(SHARED_HARD_CURRENT_LEVEL, 1);
        }

        return 0;
    }

    @Override
    public void saveCurrentLevel (GameDifficulty difficulty, int level) {
        Preferences preferences = Gdx.app.getPreferences(SharedConst.PREF_NAME);

        switch (difficulty) {
            case EASY:
                preferences.putInteger(SHARED_EASY_CURRENT_LEVEL, level);
                break;
            case NORMAL:
                preferences.putInteger(SHARED_NORMAL_CURRENT_LEVEL, level);
                break;
            case HARD:
                preferences.putInteger(SHARED_HARD_CURRENT_LEVEL, level);
                break;
        }

        preferences.flush();
    }

}
