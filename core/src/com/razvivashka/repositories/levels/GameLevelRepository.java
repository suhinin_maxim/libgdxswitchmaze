package com.razvivashka.repositories.levels;

import com.razvivashka.logic.domaindata.GameDifficulty;

/**
 * Created by maxim on 03.11.2017.
 */

public interface GameLevelRepository {

    int getSavedCurrentLevel(GameDifficulty difficulty);
    void saveCurrentLevel(GameDifficulty difficulty, int level);

}
