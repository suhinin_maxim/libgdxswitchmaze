package com.razvivashka;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.I18NBundle;
import com.razvivashka.logic.GameLogic;
import com.razvivashka.logic.GameLogicStandard;
import com.razvivashka.repositories.levels.GameLevelRepositoryShared;
import com.razvivashka.screen.chooselevel.ChooseLevelPresenter;
import com.razvivashka.screen.chooselevel.ChooseLevelScreen;
import com.razvivashka.screen.gameboard.GameBoardPresenter;
import com.razvivashka.screen.gameboard.GameBoardScreen;
import com.razvivashka.screen.choosedifficulty.ChooseDifficultyPresenter;
import com.razvivashka.screen.choosedifficulty.ChooseDifficultyScreen;
import com.razvivashka.screen.choosehero.ChooseHeroPresenter;
import com.razvivashka.screen.choosehero.ChooseHeroScreen;
import com.razvivashka.screen.levelcomplete.LevelCompletePresenter;
import com.razvivashka.screen.levelcomplete.LevelCompleteScreen;
import com.razvivashka.screen.levelscores.LevelScoresPresenter;
import com.razvivashka.screen.levelscores.LevelScoresScreen;
import com.razvivashka.screen.pause.PausePresenter;
import com.razvivashka.screen.pause.PauseScreen;
import com.razvivashka.screen.splash.SplashScreen;
import com.razvivashka.screen.youlose.YouLosePresenter;
import com.razvivashka.screen.youlose.YouLoseScreen;
import com.razvivashka.services.AdvertisingService;

import java.util.Locale;

public class GdxGame extends Game implements
		SplashScreen.InteractionListener,
		GameBoardScreen.InteractionListener,
		LevelCompleteScreen.InteractionListener,
		ChooseDifficultyScreen.InteractionListener,
		ChooseHeroScreen.InteractionListener,
		ChooseLevelScreen.InteractionListener,
		YouLoseScreen.InteractionListener,
		PauseScreen.InteractionListener,
		LevelScoresScreen.InteractionListener {

	public static AssetManager assets = new AssetManager();
	public static I18NBundle messages;

	private static com.razvivashka.services.Logger sLogger;
	private static AdvertisingService sAdvertisingService;

	private GameLogic mGameLogic;

	private SpriteBatch spriteBatch;

	public GdxGame(com.razvivashka.services.Logger logger, AdvertisingService advertisingService) {
		sLogger = logger;
		sAdvertisingService = advertisingService;
	}

	@Override
	public void create() {
		Texture.setAssetManager(assets);
		loadTextures();

		FileHandle baseFileHandle = Gdx.files.internal("i18n/messages");
		Locale locale = new Locale("ru", "RU");
		messages = I18NBundle.createBundle(baseFileHandle, locale);

		mGameLogic = new GameLogicStandard();

		spriteBatch = new SpriteBatch();

		setScreen(new SplashScreen(this));
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {
		spriteBatch.dispose();

		assets.dispose();
	}

	//region InteractionListeners


	// region implements GameBoard.InteractionListener

	@Override
	public void onSplashScreenOpenChooseDifficulty() {
		openChooseDifficulty();
	}

	// endregion


	// region implements GameBoard.InteractionListener

	@Override
	public void onGameBoardShowNextLevel() {
		openLevelComplete();
	}

	@Override
	public void onGameBoardOpenYouLose() {
		openYouLose();
	}

	@Override
	public void onGameBoardOpenChooseLevel() {
		openChooseLevel();
	}

	@Override
	public void onGameBoardPause() {
		openPause();
	}

	// endregion


	// region implements NextLevel.InteractionListener

	@Override
	public void onLevelCompleteOpenLevelScores() {
		openGameBoard();		// TODO temp
//		openLevelScores();
	}

	@Override
	public void onLevelCompleteOpenChooseLevelDifficulty() {
		openChooseDifficulty();
	}

	// endregion


	// region implements ChooseDifficulty.InteractionListener

	@Override
	public void onChooseDifficultyClose () {
		Gdx.app.exit();
	}

	@Override
	public void onChooseDifficultyOpenChooseHero () {
		openChooseHero();
	}

	// endregion


	// region implements ChooseHero.InteractionListener

	@Override
	public void onChooseHeroBackPressed () {
		openChooseDifficulty();
	}

	@Override
	public void onChooseHeroOpenChooseLevel () {
		openChooseLevel();
	}

	// endregion

	// region implements ChooseLevel.InteractionListener

	@Override
	public void onChooseLevelBackPressed () {
		openChooseHero();
	}

	@Override
	public void onChooseLevelOpenChooseDifficulty () {
		openChooseDifficulty();
	}

	@Override
	public void onChooseLevelOpenGameBoard () {
		openGameBoard();
	}

	// endregion


	// region implements YouLose.InteractionListener

	@Override
	public void onYouLoseGoHome() {
		openChooseDifficulty();
	}

	@Override
	public void onYouLoseReplayLevel() {
		openGameBoard();
	}

	// endregion


	// region implements Pause.InteractionListener

	@Override
	public void onPauseOpenGameBoard() {
		openGameBoard();
	}

	@Override
	public void onPauseOpenChooseLevel() {
		openChooseLevel();
	}

	// endregion


	// region implements LevelScores.InteractionListener

	@Override
	public void onLevelScoresOpenNextLevel() {
		openGameBoard();
	}

	@Override
	public void onLevelScoresOpenChooseDifficulty() {
		openChooseDifficulty();
	}

	// endregion

	//endregion InteractionListeners


	// region open screens

	private void openChooseDifficulty() {
		ChooseDifficultyScreen screen = new ChooseDifficultyScreen(this);
		new ChooseDifficultyPresenter(screen, mGameLogic);
		setScreen(screen);
	}

	private void openChooseHero() {
		ChooseHeroScreen screen = new ChooseHeroScreen(this);
		new ChooseHeroPresenter(screen, mGameLogic);
		setScreen(screen);
	}

	private void openGameBoard() {
		GameBoardScreen screen = new GameBoardScreen(this);
		new GameBoardPresenter(sLogger, screen, mGameLogic, new GameLevelRepositoryShared());
		setScreen(screen);
	}

	private void openChooseLevel() {
		ChooseLevelScreen screen = new ChooseLevelScreen(this);
		new ChooseLevelPresenter(screen, mGameLogic, new GameLevelRepositoryShared());
		setScreen(screen);
	}

	private void openPause() {
		PauseScreen screen = new PauseScreen(this);
		new PausePresenter(screen, mGameLogic);
		setScreen(screen);
	}

	private void openLevelComplete() {
		LevelCompleteScreen screen = new LevelCompleteScreen(this);
		new LevelCompletePresenter(screen, mGameLogic, sAdvertisingService);
		setScreen(screen);
	}

	private void openYouLose() {
		YouLoseScreen screen = new YouLoseScreen(this);
		new YouLosePresenter(screen, mGameLogic, sAdvertisingService);
		setScreen(screen);
	}

	private void openLevelScores() {
		LevelScoresScreen screen = new LevelScoresScreen(this);
		new LevelScoresPresenter(screen, mGameLogic);
		setScreen(screen);
	}

	// endregion


	// region service methods

	private void loadTextures () {

		// region common/backgrounds

		assets.load("common/backgrounds/bg_choose_difficulty.png", Texture.class);
		assets.load("common/backgrounds/bg_choose_hero.png", Texture.class);
		assets.load("common/backgrounds/bg_green_road.png", Texture.class);
		assets.load("common/backgrounds/bg_you_lose.png", Texture.class);
		assets.load("common/backgrounds/bg_pause.png", Texture.class);
		assets.load("common/backgrounds/bg_level_complete.png", Texture.class);
		assets.load("common/backgrounds/bg_game_board.png", Texture.class);
		assets.load("common/backgrounds/bg_level_scores.png", Texture.class);

		// endregion


		// region common/buttons

		assets.load("common/buttons/btn_close.png", Texture.class);
		assets.load("common/buttons/btn_close_pressed.png", Texture.class);

		assets.load("common/buttons/btn_home.png", Texture.class);
		assets.load("common/buttons/btn_home_pressed.png", Texture.class);

		// endregion


		// region common/digit

		assets.load("common/digit/digit_0.png", Texture.class);
		assets.load("common/digit/digit_1.png", Texture.class);
		assets.load("common/digit/digit_2.png", Texture.class);
		assets.load("common/digit/digit_3.png", Texture.class);
		assets.load("common/digit/digit_4.png", Texture.class);
		assets.load("common/digit/digit_5.png", Texture.class);
		assets.load("common/digit/digit_6.png", Texture.class);
		assets.load("common/digit/digit_7.png", Texture.class);
		assets.load("common/digit/digit_8.png", Texture.class);
		assets.load("common/digit/digit_9.png", Texture.class);


		// endregion


		// region screen/choosedifficulty

		assets.load("screen/choosedifficulty/btn_easy_difficulty.png", Texture.class);
		assets.load("screen/choosedifficulty/btn_easy_difficulty_pressed.png", Texture.class);

		assets.load("screen/choosedifficulty/btn_normal_difficulty.png", Texture.class);
		assets.load("screen/choosedifficulty/btn_normal_difficulty_pressed.png", Texture.class);

		assets.load("screen/choosedifficulty/btn_hard_difficulty.png", Texture.class);
		assets.load("screen/choosedifficulty/btn_hard_difficulty_pressed.png", Texture.class);

		// endregion


		// region screen/choosehero

		assets.load("screen/choosehero/btn_hero1.png", Texture.class);
		assets.load("screen/choosehero/btn_hero1_pressed.png", Texture.class);

		assets.load("screen/choosehero/btn_hero2.png", Texture.class);
		assets.load("screen/choosehero/btn_hero2_pressed.png", Texture.class);

		assets.load("screen/choosehero/btn_hero3.png", Texture.class);
		assets.load("screen/choosehero/btn_hero3_pressed.png", Texture.class);

		assets.load("screen/choosehero/btn_hero4.png", Texture.class);
		assets.load("screen/choosehero/btn_hero4_pressed.png", Texture.class);

		assets.load("screen/choosehero/btn_hero5.png", Texture.class);
		assets.load("screen/choosehero/btn_hero5_pressed.png", Texture.class);

		assets.load("screen/choosehero/btn_hero6.png", Texture.class);
		assets.load("screen/choosehero/btn_hero6_pressed.png", Texture.class);

		// endregion


		// region screen/chooselevel

		assets.load("screen/chooselevel/btn_main_road.png", Texture.class);
		assets.load("screen/chooselevel/btn_main_road_pressed.png", Texture.class);

		assets.load("screen/chooselevel/btn_stop.png", Texture.class);
		assets.load("screen/chooselevel/btn_stop_pressed.png", Texture.class);

		assets.load("screen/chooselevel/banner_choose_level_easy.png", Texture.class);
		assets.load("screen/chooselevel/banner_choose_level_normal.png", Texture.class);
		assets.load("screen/chooselevel/banner_choose_level_hard.png", Texture.class);

		// endregion


		// region screen/gameBoard

		// region screen/gameboard/board

		assets.load("screen/gameboard/board/corner.png", Texture.class);
		assets.load("screen/gameboard/board/line.png", Texture.class);
		assets.load("screen/gameboard/board/crossroad.png", Texture.class);
		assets.load("screen/gameboard/board/tee.png", Texture.class);
		assets.load("screen/gameboard/board/ground.png", Texture.class);
		assets.load("screen/gameboard/board/start.png", Texture.class);
		assets.load("screen/gameboard/board/finish.png", Texture.class);
		assets.load("screen/gameboard/board/ground.png", Texture.class);

		// endregion


		// region screen/gameboard/heroes

		assets.load("screen/gameboard/heroes/hero_one.png", Texture.class);
		assets.load("screen/gameboard/heroes/hero_two.png", Texture.class);
		assets.load("screen/gameboard/heroes/hero_three.png", Texture.class);
		assets.load("screen/gameboard/heroes/hero_four.png", Texture.class);
		assets.load("screen/gameboard/heroes/hero_five.png", Texture.class);
		assets.load("screen/gameboard/heroes/hero_six.png", Texture.class);

		// endregion


		// region screen/gameboard/static

		assets.load("screen/gameboard/static/bonus1.png", Texture.class);
		assets.load("screen/gameboard/static/coin.png", Texture.class);
		assets.load("screen/gameboard/static/add_time_bonuse.png", Texture.class);

		// endregion

		assets.load("screen/gameboard/bg_benzine_progress.png", Texture.class);

		// endregion


		// region screen/youlose

		assets.load("screen/youlose/btn_replay.png", Texture.class);
		assets.load("screen/youlose/btn_replay_pressed.png", Texture.class);

		// endregion


		// region screen/pause

		assets.load("screen/pause/btn_go.png", Texture.class);
		assets.load("screen/pause/btn_go_pressed.png", Texture.class);

		// endregion


		// region screen/levelscores

		assets.load("screen/levelscores/arrow.png", Texture.class);

		// endregion

	}

	// endregion

}
