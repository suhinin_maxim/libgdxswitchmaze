package com.razvivashka.logic.levels.level170;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level170Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level170Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 21, 30, 20, 0, 23, 10},
			{31, 30, 10, 30, 33, 10, 21, 23},
			{11, 0, 40, 11, 20, 23, 23, 22},
			{10, 10, 32, 31, 23, 20, 22, 20},
			{11, 32, 20, 33, 20, 33, 32, 21},
			{32, 21, 10, 30, 11, 30, 30, 33},
			{21, 22, 11, 22, 31, 20, 30, 60},
			{30, 21, 10, 30, 40, 40, 11, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.BONUS));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.BONUS));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,3, StaticCellType.GOLD));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,3, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
