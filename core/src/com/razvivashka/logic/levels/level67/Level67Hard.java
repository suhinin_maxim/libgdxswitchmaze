package com.razvivashka.logic.levels.level67;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level67Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level67Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 22, 23, 31, 22, 20, 33, 0},
			{40, 23, 21, 31, 23, 23, 20, 33},
			{40, 33, 20, 0, 0, 21, 20, 23},
			{33, 33, 10, 23, 0, 30, 23, 31},
			{22, 31, 32, 32, 30, 20, 33, 0},
			{40, 10, 10, 32, 22, 11, 0, 11},
			{20, 21, 10, 20, 33, 11, 22, 60},
			{31, 11, 22, 11, 23, 31, 20, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,6, StaticCellType.BONUS));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,1, StaticCellType.BONUS));
		list.add(new StaticCell(5,5, StaticCellType.GOLD));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.GOLD));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.BONUS));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,4, StaticCellType.BONUS));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,3, StaticCellType.GOLD));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
