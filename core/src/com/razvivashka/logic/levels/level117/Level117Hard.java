package com.razvivashka.logic.levels.level117;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level117Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level117Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 20, 31, 22, 22, 22, 32},
			{40, 22, 23, 32, 30, 10, 0, 33},
			{40, 0, 31, 0, 11, 0, 11, 23},
			{40, 21, 40, 40, 11, 11, 30, 23},
			{10, 11, 10, 20, 0, 30, 11, 10},
			{21, 30, 23, 32, 22, 33, 11, 22},
			{33, 30, 31, 33, 40, 30, 40, 60},
			{32, 30, 21, 10, 31, 11, 11, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,0, StaticCellType.GOLD));
		list.add(new StaticCell(3,6, StaticCellType.BONUS));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.BONUS));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
