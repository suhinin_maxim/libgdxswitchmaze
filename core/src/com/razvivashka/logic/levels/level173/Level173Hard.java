package com.razvivashka.logic.levels.level173;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level173Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level173Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 20, 20, 21, 33, 20, 10, 11},
			{40, 10, 0, 30, 21, 32, 40, 30},
			{33, 22, 33, 10, 11, 23, 32, 30},
			{11, 31, 40, 23, 33, 21, 40, 20},
			{40, 31, 33, 40, 20, 21, 31, 0},
			{33, 32, 11, 11, 31, 32, 10, 11},
			{40, 33, 31, 10, 22, 11, 21, 60},
			{0, 40, 33, 0, 0, 21, 32, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.BONUS));
		list.add(new StaticCell(3,0, StaticCellType.GOLD));
		list.add(new StaticCell(1,3, StaticCellType.GOLD));
		list.add(new StaticCell(0,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,5, StaticCellType.GOLD));
		list.add(new StaticCell(2,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,2, StaticCellType.BONUS));
		list.add(new StaticCell(6,1, StaticCellType.BONUS));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,4, StaticCellType.GOLD));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
