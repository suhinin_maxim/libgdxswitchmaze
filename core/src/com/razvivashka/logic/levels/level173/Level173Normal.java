package com.razvivashka.logic.levels.level173;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level173Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level173Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 33, 32, 30, 11, 11},
			{10, 22, 10, 22, 30, 33},
			{30, 23, 22, 20, 11, 21},
			{31, 30, 33, 40, 40, 30},
			{23, 31, 21, 33, 30, 60},
			{23, 33, 10, 32, 33, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,5, StaticCellType.BONUS));
		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,2, StaticCellType.BONUS));
		list.add(new StaticCell(1,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,1, StaticCellType.GOLD));
		list.add(new StaticCell(4,0, StaticCellType.BONUS));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.BONUS));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 12;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
