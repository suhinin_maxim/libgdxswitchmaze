package com.razvivashka.logic.levels.level146;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level146Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level146Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 10, 0, 22, 30, 32, 21, 0},
			{40, 20, 22, 22, 10, 20, 33, 10},
			{32, 33, 22, 40, 40, 40, 0, 0},
			{40, 31, 33, 32, 21, 20, 0, 32},
			{32, 32, 10, 22, 33, 0, 40, 10},
			{20, 23, 0, 23, 21, 0, 23, 23},
			{32, 30, 32, 22, 23, 30, 11, 60},
			{0, 20, 40, 20, 20, 31, 40, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,3, StaticCellType.GOLD));
		list.add(new StaticCell(3,7, StaticCellType.BONUS));
		list.add(new StaticCell(7,6, StaticCellType.BONUS));
		list.add(new StaticCell(1,2, StaticCellType.BONUS));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,3, StaticCellType.GOLD));
		list.add(new StaticCell(5,7, StaticCellType.BONUS));
		list.add(new StaticCell(2,2, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
