package com.razvivashka.logic.levels.level184;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level184Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level184Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 21, 31, 10, 23, 10, 11},
			{31, 22, 30, 31, 33, 31, 11, 40},
			{21, 33, 23, 0, 23, 33, 30, 30},
			{30, 21, 33, 33, 40, 40, 22, 40},
			{11, 20, 32, 40, 31, 21, 11, 30},
			{22, 10, 0, 31, 11, 30, 33, 40},
			{23, 40, 22, 21, 22, 30, 40, 60},
			{21, 0, 33, 10, 20, 11, 20, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.GOLD));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,2, StaticCellType.GOLD));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.GOLD));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
