package com.razvivashka.logic.levels.level15;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level15Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level15Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 22, 33, 33, 11, 40, 11, 33},
			{40, 11, 22, 20, 0, 0, 10, 21},
			{40, 23, 22, 10, 11, 21, 22, 23},
			{11, 33, 0, 22, 0, 21, 30, 40},
			{21, 33, 11, 40, 32, 30, 21, 31},
			{11, 10, 31, 33, 30, 40, 23, 10},
			{22, 30, 10, 33, 30, 32, 20, 60},
			{40, 11, 31, 21, 33, 33, 40, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,6, StaticCellType.GOLD));
		list.add(new StaticCell(7,7, StaticCellType.GOLD));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,3, StaticCellType.GOLD));
		list.add(new StaticCell(1,3, StaticCellType.GOLD));
		list.add(new StaticCell(5,6, StaticCellType.BONUS));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,0, StaticCellType.GOLD));
		list.add(new StaticCell(4,2, StaticCellType.GOLD));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.BONUS));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,6, StaticCellType.BONUS));
		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
