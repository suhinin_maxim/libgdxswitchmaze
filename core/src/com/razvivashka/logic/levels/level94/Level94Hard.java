package com.razvivashka.logic.levels.level94;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level94Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level94Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 0, 23, 32, 33, 0, 11},
			{40, 22, 20, 10, 32, 30, 10, 33},
			{33, 30, 22, 20, 0, 40, 10, 31},
			{21, 22, 20, 31, 23, 31, 21, 40},
			{30, 11, 23, 0, 30, 10, 11, 10},
			{21, 21, 32, 22, 22, 23, 33, 33},
			{30, 32, 21, 30, 31, 23, 32, 60},
			{10, 32, 0, 23, 22, 33, 33, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.GOLD));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,2, StaticCellType.GOLD));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,3, StaticCellType.GOLD));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
