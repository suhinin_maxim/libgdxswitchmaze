package com.razvivashka.logic.levels.level33;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level33Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level33Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 11, 0, 31, 31, 0, 32},
			{32, 20, 40, 33, 10, 22, 23, 33},
			{30, 23, 30, 22, 10, 40, 22, 31},
			{10, 40, 40, 0, 23, 22, 31, 10},
			{33, 10, 30, 33, 23, 20, 20, 40},
			{21, 10, 40, 30, 23, 31, 30, 0},
			{30, 20, 10, 31, 33, 10, 23, 60},
			{32, 40, 32, 33, 31, 33, 20, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));
		list.add(new StaticCell(0,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.GOLD));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.BONUS));
		list.add(new StaticCell(1,4, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
