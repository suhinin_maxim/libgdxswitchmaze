package com.razvivashka.logic.levels.level129;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level129Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level129Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 32, 22, 0, 11, 20, 21},
			{20, 22, 0, 22, 22, 31, 22, 23},
			{33, 40, 10, 23, 30, 20, 30, 31},
			{40, 31, 11, 33, 21, 11, 31, 32},
			{30, 11, 33, 30, 33, 23, 23, 31},
			{33, 20, 32, 23, 32, 33, 30, 31},
			{11, 23, 31, 33, 0, 21, 21, 60},
			{40, 33, 40, 20, 40, 31, 30, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,6, StaticCellType.BONUS));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,1, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
