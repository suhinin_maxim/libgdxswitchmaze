package com.razvivashka.logic.levels.level129;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level129Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level129Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 21, 23, 10, 33},
			{40, 40, 23, 0, 10, 31},
			{11, 40, 10, 21, 23, 20},
			{40, 40, 32, 31, 23, 32},
			{21, 33, 10, 22, 11, 60},
			{20, 30, 33, 21, 33, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,2, StaticCellType.BONUS));
		list.add(new StaticCell(5,0, StaticCellType.GOLD));
		list.add(new StaticCell(2,5, StaticCellType.GOLD));
		list.add(new StaticCell(4,1, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));
		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,2, StaticCellType.GOLD));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,1, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 12;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
