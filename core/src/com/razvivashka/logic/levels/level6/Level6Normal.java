package com.razvivashka.logic.levels.level6;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level6Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level6Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 10, 40, 22, 10, 32},
			{32, 30, 23, 40, 33, 32},
			{33, 40, 20, 33, 21, 22},
			{21, 30, 32, 11, 33, 33},
			{10, 40, 30, 33, 10, 60},
			{40, 11, 30, 20, 40, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.BONUS));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,2, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 12;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
