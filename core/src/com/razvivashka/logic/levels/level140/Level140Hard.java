package com.razvivashka.logic.levels.level140;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level140Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level140Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 31, 30, 0, 21, 40, 32},
			{21, 23, 33, 11, 10, 10, 33, 0},
			{23, 10, 22, 32, 22, 32, 23, 21},
			{10, 21, 32, 32, 30, 33, 22, 22},
			{31, 11, 32, 33, 30, 40, 40, 32},
			{33, 0, 31, 20, 10, 30, 32, 11},
			{10, 20, 22, 40, 40, 10, 30, 60},
			{0, 11, 40, 0, 22, 30, 30, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.BONUS));
		list.add(new StaticCell(7,2, StaticCellType.BONUS));
		list.add(new StaticCell(4,0, StaticCellType.GOLD));
		list.add(new StaticCell(4,3, StaticCellType.BONUS));
		list.add(new StaticCell(2,4, StaticCellType.BONUS));
		list.add(new StaticCell(6,1, StaticCellType.BONUS));
		list.add(new StaticCell(3,7, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
