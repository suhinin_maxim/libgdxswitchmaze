package com.razvivashka.logic.levels.level84;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level84Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level84Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 10, 0, 22, 40, 33, 31},
			{10, 20, 20, 31, 33, 31, 40, 23},
			{31, 40, 20, 22, 20, 23, 30, 23},
			{10, 40, 22, 11, 21, 11, 20, 0},
			{33, 30, 32, 22, 23, 21, 30, 11},
			{40, 30, 10, 10, 21, 31, 32, 10},
			{11, 30, 20, 0, 20, 22, 0, 60},
			{33, 32, 33, 31, 32, 30, 10, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.GOLD));
		list.add(new StaticCell(0,7, StaticCellType.GOLD));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,2, StaticCellType.GOLD));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.GOLD));
		list.add(new StaticCell(1,1, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
