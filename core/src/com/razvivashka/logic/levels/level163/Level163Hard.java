package com.razvivashka.logic.levels.level163;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level163Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level163Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 20, 11, 0, 40, 11, 22, 22},
			{11, 40, 22, 40, 0, 30, 33, 32},
			{11, 21, 31, 32, 11, 21, 30, 40},
			{30, 10, 40, 40, 40, 23, 30, 0},
			{30, 0, 31, 32, 22, 30, 21, 33},
			{31, 22, 23, 31, 33, 40, 33, 21},
			{30, 31, 0, 23, 22, 33, 32, 60},
			{20, 31, 22, 33, 20, 21, 11, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.GOLD));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.GOLD));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,3, StaticCellType.BONUS));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
