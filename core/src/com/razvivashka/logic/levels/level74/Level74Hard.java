package com.razvivashka.logic.levels.level74;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level74Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level74Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 11, 30, 31, 11, 30, 21},
			{40, 40, 11, 20, 23, 31, 33, 23},
			{40, 40, 23, 30, 0, 0, 22, 40},
			{30, 21, 10, 31, 22, 31, 21, 30},
			{32, 22, 22, 10, 0, 21, 21, 31},
			{30, 11, 23, 11, 0, 33, 30, 20},
			{11, 31, 11, 21, 10, 11, 40, 60},
			{23, 40, 33, 40, 40, 10, 40, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,2, StaticCellType.BONUS));
		list.add(new StaticCell(6,3, StaticCellType.GOLD));
		list.add(new StaticCell(1,0, StaticCellType.BONUS));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,6, StaticCellType.BONUS));
		list.add(new StaticCell(6,1, StaticCellType.GOLD));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
