package com.razvivashka.logic.levels.level28;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level28Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level28Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 30, 31, 10, 11, 30, 21},
			{31, 32, 23, 30, 30, 11, 23, 31},
			{30, 23, 32, 31, 31, 23, 21, 30},
			{30, 40, 23, 33, 30, 10, 40, 10},
			{0, 11, 40, 22, 0, 10, 10, 33},
			{22, 31, 32, 21, 33, 40, 0, 33},
			{20, 33, 40, 23, 22, 40, 11, 60},
			{33, 32, 30, 32, 33, 31, 33, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,0, StaticCellType.BONUS));
		list.add(new StaticCell(3,6, StaticCellType.BONUS));
		list.add(new StaticCell(3,7, StaticCellType.BONUS));
		list.add(new StaticCell(0,1, StaticCellType.BONUS));
		list.add(new StaticCell(6,5, StaticCellType.GOLD));
		list.add(new StaticCell(7,7, StaticCellType.BONUS));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,4, StaticCellType.BONUS));
		list.add(new StaticCell(4,7, StaticCellType.GOLD));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.BONUS));
		list.add(new StaticCell(6,1, StaticCellType.GOLD));
		list.add(new StaticCell(4,3, StaticCellType.BONUS));
		list.add(new StaticCell(1,2, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
