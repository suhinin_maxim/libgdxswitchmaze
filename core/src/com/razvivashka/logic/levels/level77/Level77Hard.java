package com.razvivashka.logic.levels.level77;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level77Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level77Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 33, 33, 31, 31, 20, 11, 31},
			{33, 0, 30, 40, 0, 40, 23, 20},
			{40, 21, 30, 33, 32, 21, 32, 22},
			{40, 32, 21, 23, 33, 33, 20, 21},
			{33, 30, 10, 21, 20, 20, 40, 21},
			{33, 23, 20, 33, 33, 10, 31, 33},
			{20, 22, 11, 11, 23, 0, 10, 60},
			{21, 33, 32, 10, 40, 33, 11, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.GOLD));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,1, StaticCellType.BONUS));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,6, StaticCellType.GOLD));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.BONUS));
		list.add(new StaticCell(7,7, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
