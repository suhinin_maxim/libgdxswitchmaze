package com.razvivashka.logic.levels.level142;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level142Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level142Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 21, 10, 10, 10, 30},
			{33, 40, 40, 23, 20, 21},
			{0, 33, 11, 30, 22, 10},
			{21, 0, 40, 11, 0, 30},
			{21, 33, 10, 40, 21, 60},
			{11, 33, 33, 21, 30, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,3, StaticCellType.BONUS));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.BONUS));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 12;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
