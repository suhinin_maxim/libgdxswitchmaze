package com.razvivashka.logic.levels.level188;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level188Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level188Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 0, 23, 40, 21},
			{31, 23, 40, 22, 30, 40},
			{20, 22, 10, 10, 11, 0},
			{22, 40, 31, 23, 32, 33},
			{32, 31, 10, 30, 22, 60},
			{0, 30, 11, 11, 40, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.GOLD));
		list.add(new StaticCell(1,5, StaticCellType.GOLD));
		list.add(new StaticCell(4,3, StaticCellType.GOLD));
		list.add(new StaticCell(1,4, StaticCellType.BONUS));
		list.add(new StaticCell(4,0, StaticCellType.BONUS));
		list.add(new StaticCell(5,3, StaticCellType.GOLD));
		list.add(new StaticCell(0,5, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 12;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
