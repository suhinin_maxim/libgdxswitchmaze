package com.razvivashka.logic.levels.level188;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level188Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level188Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 33, 0, 23, 11, 22, 22},
			{21, 22, 11, 0, 23, 30, 23, 40},
			{40, 33, 22, 30, 31, 31, 31, 20},
			{11, 20, 33, 40, 11, 23, 20, 30},
			{22, 21, 32, 33, 31, 11, 31, 0},
			{23, 22, 10, 10, 31, 23, 10, 10},
			{11, 0, 11, 10, 0, 0, 20, 60},
			{20, 31, 33, 30, 10, 40, 40, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.GOLD));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.BONUS));
		list.add(new StaticCell(1,5, StaticCellType.GOLD));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.BONUS));
		list.add(new StaticCell(0,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.GOLD));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
