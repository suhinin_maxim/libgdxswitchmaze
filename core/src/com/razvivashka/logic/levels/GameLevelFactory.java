package com.razvivashka.logic.levels;

import com.razvivashka.logic.domaindata.GameDifficulty;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.levels.level1.Level1;
import com.razvivashka.logic.levels.level1.Level1Easy;
import com.razvivashka.logic.levels.level1.Level1Hard;
import com.razvivashka.logic.levels.level1.Level1Normal;
import com.razvivashka.logic.levels.level100.Level100;
import com.razvivashka.logic.levels.level100.Level100Easy;
import com.razvivashka.logic.levels.level100.Level100Hard;
import com.razvivashka.logic.levels.level100.Level100Normal;
import com.razvivashka.logic.levels.level101.Level101;
import com.razvivashka.logic.levels.level101.Level101Easy;
import com.razvivashka.logic.levels.level101.Level101Hard;
import com.razvivashka.logic.levels.level101.Level101Normal;
import com.razvivashka.logic.levels.level102.Level102;
import com.razvivashka.logic.levels.level102.Level102Easy;
import com.razvivashka.logic.levels.level102.Level102Hard;
import com.razvivashka.logic.levels.level102.Level102Normal;
import com.razvivashka.logic.levels.level103.Level103;
import com.razvivashka.logic.levels.level103.Level103Easy;
import com.razvivashka.logic.levels.level103.Level103Hard;
import com.razvivashka.logic.levels.level103.Level103Normal;
import com.razvivashka.logic.levels.level104.Level104;
import com.razvivashka.logic.levels.level104.Level104Easy;
import com.razvivashka.logic.levels.level104.Level104Hard;
import com.razvivashka.logic.levels.level104.Level104Normal;
import com.razvivashka.logic.levels.level105.Level105;
import com.razvivashka.logic.levels.level105.Level105Easy;
import com.razvivashka.logic.levels.level105.Level105Hard;
import com.razvivashka.logic.levels.level105.Level105Normal;
import com.razvivashka.logic.levels.level106.Level106;
import com.razvivashka.logic.levels.level106.Level106Easy;
import com.razvivashka.logic.levels.level106.Level106Hard;
import com.razvivashka.logic.levels.level106.Level106Normal;
import com.razvivashka.logic.levels.level107.Level107;
import com.razvivashka.logic.levels.level107.Level107Easy;
import com.razvivashka.logic.levels.level107.Level107Hard;
import com.razvivashka.logic.levels.level107.Level107Normal;
import com.razvivashka.logic.levels.level108.Level108;
import com.razvivashka.logic.levels.level108.Level108Easy;
import com.razvivashka.logic.levels.level108.Level108Hard;
import com.razvivashka.logic.levels.level108.Level108Normal;
import com.razvivashka.logic.levels.level109.Level109;
import com.razvivashka.logic.levels.level109.Level109Easy;
import com.razvivashka.logic.levels.level109.Level109Hard;
import com.razvivashka.logic.levels.level109.Level109Normal;
import com.razvivashka.logic.levels.level110.Level110;
import com.razvivashka.logic.levels.level110.Level110Easy;
import com.razvivashka.logic.levels.level110.Level110Hard;
import com.razvivashka.logic.levels.level110.Level110Normal;
import com.razvivashka.logic.levels.level111.Level111;
import com.razvivashka.logic.levels.level111.Level111Easy;
import com.razvivashka.logic.levels.level111.Level111Hard;
import com.razvivashka.logic.levels.level111.Level111Normal;
import com.razvivashka.logic.levels.level112.Level112;
import com.razvivashka.logic.levels.level112.Level112Easy;
import com.razvivashka.logic.levels.level112.Level112Hard;
import com.razvivashka.logic.levels.level112.Level112Normal;
import com.razvivashka.logic.levels.level113.Level113;
import com.razvivashka.logic.levels.level113.Level113Easy;
import com.razvivashka.logic.levels.level113.Level113Hard;
import com.razvivashka.logic.levels.level113.Level113Normal;
import com.razvivashka.logic.levels.level114.Level114;
import com.razvivashka.logic.levels.level114.Level114Easy;
import com.razvivashka.logic.levels.level114.Level114Hard;
import com.razvivashka.logic.levels.level114.Level114Normal;
import com.razvivashka.logic.levels.level115.Level115;
import com.razvivashka.logic.levels.level115.Level115Easy;
import com.razvivashka.logic.levels.level115.Level115Hard;
import com.razvivashka.logic.levels.level115.Level115Normal;
import com.razvivashka.logic.levels.level116.Level116;
import com.razvivashka.logic.levels.level116.Level116Easy;
import com.razvivashka.logic.levels.level116.Level116Hard;
import com.razvivashka.logic.levels.level116.Level116Normal;
import com.razvivashka.logic.levels.level117.Level117;
import com.razvivashka.logic.levels.level117.Level117Easy;
import com.razvivashka.logic.levels.level117.Level117Hard;
import com.razvivashka.logic.levels.level117.Level117Normal;
import com.razvivashka.logic.levels.level118.Level118;
import com.razvivashka.logic.levels.level118.Level118Easy;
import com.razvivashka.logic.levels.level118.Level118Hard;
import com.razvivashka.logic.levels.level118.Level118Normal;
import com.razvivashka.logic.levels.level119.Level119;
import com.razvivashka.logic.levels.level119.Level119Easy;
import com.razvivashka.logic.levels.level119.Level119Hard;
import com.razvivashka.logic.levels.level119.Level119Normal;
import com.razvivashka.logic.levels.level120.Level120;
import com.razvivashka.logic.levels.level120.Level120Easy;
import com.razvivashka.logic.levels.level120.Level120Hard;
import com.razvivashka.logic.levels.level120.Level120Normal;
import com.razvivashka.logic.levels.level121.Level121;
import com.razvivashka.logic.levels.level121.Level121Easy;
import com.razvivashka.logic.levels.level121.Level121Hard;
import com.razvivashka.logic.levels.level121.Level121Normal;
import com.razvivashka.logic.levels.level122.Level122;
import com.razvivashka.logic.levels.level122.Level122Easy;
import com.razvivashka.logic.levels.level122.Level122Hard;
import com.razvivashka.logic.levels.level122.Level122Normal;
import com.razvivashka.logic.levels.level123.Level123;
import com.razvivashka.logic.levels.level123.Level123Easy;
import com.razvivashka.logic.levels.level123.Level123Hard;
import com.razvivashka.logic.levels.level123.Level123Normal;
import com.razvivashka.logic.levels.level124.Level124;
import com.razvivashka.logic.levels.level124.Level124Easy;
import com.razvivashka.logic.levels.level124.Level124Hard;
import com.razvivashka.logic.levels.level124.Level124Normal;
import com.razvivashka.logic.levels.level125.Level125;
import com.razvivashka.logic.levels.level125.Level125Easy;
import com.razvivashka.logic.levels.level125.Level125Hard;
import com.razvivashka.logic.levels.level125.Level125Normal;
import com.razvivashka.logic.levels.level126.Level126;
import com.razvivashka.logic.levels.level126.Level126Easy;
import com.razvivashka.logic.levels.level126.Level126Hard;
import com.razvivashka.logic.levels.level126.Level126Normal;
import com.razvivashka.logic.levels.level127.Level127;
import com.razvivashka.logic.levels.level127.Level127Easy;
import com.razvivashka.logic.levels.level127.Level127Hard;
import com.razvivashka.logic.levels.level127.Level127Normal;
import com.razvivashka.logic.levels.level128.Level128;
import com.razvivashka.logic.levels.level128.Level128Easy;
import com.razvivashka.logic.levels.level128.Level128Hard;
import com.razvivashka.logic.levels.level128.Level128Normal;
import com.razvivashka.logic.levels.level129.Level129;
import com.razvivashka.logic.levels.level129.Level129Easy;
import com.razvivashka.logic.levels.level129.Level129Hard;
import com.razvivashka.logic.levels.level129.Level129Normal;
import com.razvivashka.logic.levels.level130.Level130;
import com.razvivashka.logic.levels.level130.Level130Easy;
import com.razvivashka.logic.levels.level130.Level130Hard;
import com.razvivashka.logic.levels.level130.Level130Normal;
import com.razvivashka.logic.levels.level131.Level131;
import com.razvivashka.logic.levels.level131.Level131Easy;
import com.razvivashka.logic.levels.level131.Level131Hard;
import com.razvivashka.logic.levels.level131.Level131Normal;
import com.razvivashka.logic.levels.level132.Level132;
import com.razvivashka.logic.levels.level132.Level132Easy;
import com.razvivashka.logic.levels.level132.Level132Hard;
import com.razvivashka.logic.levels.level132.Level132Normal;
import com.razvivashka.logic.levels.level133.Level133;
import com.razvivashka.logic.levels.level133.Level133Easy;
import com.razvivashka.logic.levels.level133.Level133Hard;
import com.razvivashka.logic.levels.level133.Level133Normal;
import com.razvivashka.logic.levels.level134.Level134;
import com.razvivashka.logic.levels.level134.Level134Easy;
import com.razvivashka.logic.levels.level134.Level134Hard;
import com.razvivashka.logic.levels.level134.Level134Normal;
import com.razvivashka.logic.levels.level135.Level135;
import com.razvivashka.logic.levels.level135.Level135Easy;
import com.razvivashka.logic.levels.level135.Level135Hard;
import com.razvivashka.logic.levels.level135.Level135Normal;
import com.razvivashka.logic.levels.level136.Level136;
import com.razvivashka.logic.levels.level136.Level136Easy;
import com.razvivashka.logic.levels.level136.Level136Hard;
import com.razvivashka.logic.levels.level136.Level136Normal;
import com.razvivashka.logic.levels.level137.Level137;
import com.razvivashka.logic.levels.level137.Level137Easy;
import com.razvivashka.logic.levels.level137.Level137Hard;
import com.razvivashka.logic.levels.level137.Level137Normal;
import com.razvivashka.logic.levels.level138.Level138;
import com.razvivashka.logic.levels.level138.Level138Easy;
import com.razvivashka.logic.levels.level138.Level138Hard;
import com.razvivashka.logic.levels.level138.Level138Normal;
import com.razvivashka.logic.levels.level139.Level139;
import com.razvivashka.logic.levels.level139.Level139Easy;
import com.razvivashka.logic.levels.level139.Level139Hard;
import com.razvivashka.logic.levels.level139.Level139Normal;
import com.razvivashka.logic.levels.level14.Level14;
import com.razvivashka.logic.levels.level14.Level14Easy;
import com.razvivashka.logic.levels.level14.Level14Hard;
import com.razvivashka.logic.levels.level14.Level14Normal;
import com.razvivashka.logic.levels.level140.Level140;
import com.razvivashka.logic.levels.level140.Level140Easy;
import com.razvivashka.logic.levels.level140.Level140Hard;
import com.razvivashka.logic.levels.level140.Level140Normal;
import com.razvivashka.logic.levels.level141.Level141;
import com.razvivashka.logic.levels.level141.Level141Easy;
import com.razvivashka.logic.levels.level141.Level141Hard;
import com.razvivashka.logic.levels.level141.Level141Normal;
import com.razvivashka.logic.levels.level142.Level142;
import com.razvivashka.logic.levels.level142.Level142Easy;
import com.razvivashka.logic.levels.level142.Level142Hard;
import com.razvivashka.logic.levels.level142.Level142Normal;
import com.razvivashka.logic.levels.level143.Level143;
import com.razvivashka.logic.levels.level143.Level143Easy;
import com.razvivashka.logic.levels.level143.Level143Hard;
import com.razvivashka.logic.levels.level143.Level143Normal;
import com.razvivashka.logic.levels.level144.Level144;
import com.razvivashka.logic.levels.level144.Level144Easy;
import com.razvivashka.logic.levels.level144.Level144Hard;
import com.razvivashka.logic.levels.level144.Level144Normal;
import com.razvivashka.logic.levels.level145.Level145;
import com.razvivashka.logic.levels.level145.Level145Easy;
import com.razvivashka.logic.levels.level145.Level145Hard;
import com.razvivashka.logic.levels.level145.Level145Normal;
import com.razvivashka.logic.levels.level146.Level146;
import com.razvivashka.logic.levels.level146.Level146Easy;
import com.razvivashka.logic.levels.level146.Level146Hard;
import com.razvivashka.logic.levels.level146.Level146Normal;
import com.razvivashka.logic.levels.level147.Level147;
import com.razvivashka.logic.levels.level147.Level147Easy;
import com.razvivashka.logic.levels.level147.Level147Hard;
import com.razvivashka.logic.levels.level147.Level147Normal;
import com.razvivashka.logic.levels.level148.Level148;
import com.razvivashka.logic.levels.level148.Level148Easy;
import com.razvivashka.logic.levels.level148.Level148Hard;
import com.razvivashka.logic.levels.level148.Level148Normal;
import com.razvivashka.logic.levels.level149.Level149;
import com.razvivashka.logic.levels.level149.Level149Easy;
import com.razvivashka.logic.levels.level149.Level149Hard;
import com.razvivashka.logic.levels.level149.Level149Normal;
import com.razvivashka.logic.levels.level15.Level15;
import com.razvivashka.logic.levels.level15.Level15Easy;
import com.razvivashka.logic.levels.level15.Level15Hard;
import com.razvivashka.logic.levels.level15.Level15Normal;
import com.razvivashka.logic.levels.level150.Level150;
import com.razvivashka.logic.levels.level150.Level150Easy;
import com.razvivashka.logic.levels.level150.Level150Hard;
import com.razvivashka.logic.levels.level150.Level150Normal;
import com.razvivashka.logic.levels.level151.Level151;
import com.razvivashka.logic.levels.level151.Level151Easy;
import com.razvivashka.logic.levels.level151.Level151Hard;
import com.razvivashka.logic.levels.level151.Level151Normal;
import com.razvivashka.logic.levels.level152.Level152;
import com.razvivashka.logic.levels.level152.Level152Easy;
import com.razvivashka.logic.levels.level152.Level152Hard;
import com.razvivashka.logic.levels.level152.Level152Normal;
import com.razvivashka.logic.levels.level153.Level153;
import com.razvivashka.logic.levels.level153.Level153Easy;
import com.razvivashka.logic.levels.level153.Level153Hard;
import com.razvivashka.logic.levels.level153.Level153Normal;
import com.razvivashka.logic.levels.level154.Level154;
import com.razvivashka.logic.levels.level154.Level154Easy;
import com.razvivashka.logic.levels.level154.Level154Hard;
import com.razvivashka.logic.levels.level154.Level154Normal;
import com.razvivashka.logic.levels.level155.Level155;
import com.razvivashka.logic.levels.level155.Level155Easy;
import com.razvivashka.logic.levels.level155.Level155Hard;
import com.razvivashka.logic.levels.level155.Level155Normal;
import com.razvivashka.logic.levels.level156.Level156;
import com.razvivashka.logic.levels.level156.Level156Easy;
import com.razvivashka.logic.levels.level156.Level156Hard;
import com.razvivashka.logic.levels.level156.Level156Normal;
import com.razvivashka.logic.levels.level157.Level157;
import com.razvivashka.logic.levels.level157.Level157Easy;
import com.razvivashka.logic.levels.level157.Level157Hard;
import com.razvivashka.logic.levels.level157.Level157Normal;
import com.razvivashka.logic.levels.level158.Level158;
import com.razvivashka.logic.levels.level158.Level158Easy;
import com.razvivashka.logic.levels.level158.Level158Hard;
import com.razvivashka.logic.levels.level158.Level158Normal;
import com.razvivashka.logic.levels.level159.Level159;
import com.razvivashka.logic.levels.level159.Level159Easy;
import com.razvivashka.logic.levels.level159.Level159Hard;
import com.razvivashka.logic.levels.level159.Level159Normal;
import com.razvivashka.logic.levels.level16.Level16;
import com.razvivashka.logic.levels.level16.Level16Easy;
import com.razvivashka.logic.levels.level16.Level16Hard;
import com.razvivashka.logic.levels.level16.Level16Normal;
import com.razvivashka.logic.levels.level160.Level160;
import com.razvivashka.logic.levels.level160.Level160Easy;
import com.razvivashka.logic.levels.level160.Level160Hard;
import com.razvivashka.logic.levels.level160.Level160Normal;
import com.razvivashka.logic.levels.level161.Level161;
import com.razvivashka.logic.levels.level161.Level161Easy;
import com.razvivashka.logic.levels.level161.Level161Hard;
import com.razvivashka.logic.levels.level161.Level161Normal;
import com.razvivashka.logic.levels.level162.Level162;
import com.razvivashka.logic.levels.level162.Level162Easy;
import com.razvivashka.logic.levels.level162.Level162Hard;
import com.razvivashka.logic.levels.level162.Level162Normal;
import com.razvivashka.logic.levels.level163.Level163;
import com.razvivashka.logic.levels.level163.Level163Easy;
import com.razvivashka.logic.levels.level163.Level163Hard;
import com.razvivashka.logic.levels.level163.Level163Normal;
import com.razvivashka.logic.levels.level164.Level164;
import com.razvivashka.logic.levels.level164.Level164Easy;
import com.razvivashka.logic.levels.level164.Level164Hard;
import com.razvivashka.logic.levels.level164.Level164Normal;
import com.razvivashka.logic.levels.level165.Level165;
import com.razvivashka.logic.levels.level165.Level165Easy;
import com.razvivashka.logic.levels.level165.Level165Hard;
import com.razvivashka.logic.levels.level165.Level165Normal;
import com.razvivashka.logic.levels.level166.Level166;
import com.razvivashka.logic.levels.level166.Level166Easy;
import com.razvivashka.logic.levels.level166.Level166Hard;
import com.razvivashka.logic.levels.level166.Level166Normal;
import com.razvivashka.logic.levels.level167.Level167;
import com.razvivashka.logic.levels.level167.Level167Easy;
import com.razvivashka.logic.levels.level167.Level167Hard;
import com.razvivashka.logic.levels.level167.Level167Normal;
import com.razvivashka.logic.levels.level168.Level168;
import com.razvivashka.logic.levels.level168.Level168Easy;
import com.razvivashka.logic.levels.level168.Level168Hard;
import com.razvivashka.logic.levels.level168.Level168Normal;
import com.razvivashka.logic.levels.level169.Level169;
import com.razvivashka.logic.levels.level169.Level169Easy;
import com.razvivashka.logic.levels.level169.Level169Hard;
import com.razvivashka.logic.levels.level169.Level169Normal;
import com.razvivashka.logic.levels.level170.Level170;
import com.razvivashka.logic.levels.level170.Level170Easy;
import com.razvivashka.logic.levels.level170.Level170Hard;
import com.razvivashka.logic.levels.level170.Level170Normal;
import com.razvivashka.logic.levels.level171.Level171;
import com.razvivashka.logic.levels.level171.Level171Easy;
import com.razvivashka.logic.levels.level171.Level171Hard;
import com.razvivashka.logic.levels.level171.Level171Normal;
import com.razvivashka.logic.levels.level172.Level172;
import com.razvivashka.logic.levels.level172.Level172Easy;
import com.razvivashka.logic.levels.level172.Level172Hard;
import com.razvivashka.logic.levels.level172.Level172Normal;
import com.razvivashka.logic.levels.level173.Level173;
import com.razvivashka.logic.levels.level173.Level173Easy;
import com.razvivashka.logic.levels.level173.Level173Hard;
import com.razvivashka.logic.levels.level173.Level173Normal;
import com.razvivashka.logic.levels.level174.Level174;
import com.razvivashka.logic.levels.level174.Level174Easy;
import com.razvivashka.logic.levels.level174.Level174Hard;
import com.razvivashka.logic.levels.level174.Level174Normal;
import com.razvivashka.logic.levels.level175.Level175;
import com.razvivashka.logic.levels.level175.Level175Easy;
import com.razvivashka.logic.levels.level175.Level175Hard;
import com.razvivashka.logic.levels.level175.Level175Normal;
import com.razvivashka.logic.levels.level176.Level176;
import com.razvivashka.logic.levels.level176.Level176Easy;
import com.razvivashka.logic.levels.level176.Level176Hard;
import com.razvivashka.logic.levels.level176.Level176Normal;
import com.razvivashka.logic.levels.level177.Level177;
import com.razvivashka.logic.levels.level177.Level177Easy;
import com.razvivashka.logic.levels.level177.Level177Hard;
import com.razvivashka.logic.levels.level177.Level177Normal;
import com.razvivashka.logic.levels.level178.Level178;
import com.razvivashka.logic.levels.level178.Level178Easy;
import com.razvivashka.logic.levels.level178.Level178Hard;
import com.razvivashka.logic.levels.level178.Level178Normal;
import com.razvivashka.logic.levels.level179.Level179;
import com.razvivashka.logic.levels.level179.Level179Easy;
import com.razvivashka.logic.levels.level179.Level179Hard;
import com.razvivashka.logic.levels.level179.Level179Normal;
import com.razvivashka.logic.levels.level18.Level18;
import com.razvivashka.logic.levels.level18.Level18Easy;
import com.razvivashka.logic.levels.level18.Level18Hard;
import com.razvivashka.logic.levels.level18.Level18Normal;
import com.razvivashka.logic.levels.level180.Level180;
import com.razvivashka.logic.levels.level180.Level180Easy;
import com.razvivashka.logic.levels.level180.Level180Hard;
import com.razvivashka.logic.levels.level180.Level180Normal;
import com.razvivashka.logic.levels.level181.Level181;
import com.razvivashka.logic.levels.level181.Level181Easy;
import com.razvivashka.logic.levels.level181.Level181Hard;
import com.razvivashka.logic.levels.level181.Level181Normal;
import com.razvivashka.logic.levels.level182.Level182;
import com.razvivashka.logic.levels.level182.Level182Easy;
import com.razvivashka.logic.levels.level182.Level182Hard;
import com.razvivashka.logic.levels.level182.Level182Normal;
import com.razvivashka.logic.levels.level183.Level183;
import com.razvivashka.logic.levels.level183.Level183Easy;
import com.razvivashka.logic.levels.level183.Level183Hard;
import com.razvivashka.logic.levels.level183.Level183Normal;
import com.razvivashka.logic.levels.level184.Level184;
import com.razvivashka.logic.levels.level184.Level184Easy;
import com.razvivashka.logic.levels.level184.Level184Hard;
import com.razvivashka.logic.levels.level184.Level184Normal;
import com.razvivashka.logic.levels.level185.Level185;
import com.razvivashka.logic.levels.level185.Level185Easy;
import com.razvivashka.logic.levels.level185.Level185Hard;
import com.razvivashka.logic.levels.level185.Level185Normal;
import com.razvivashka.logic.levels.level186.Level186;
import com.razvivashka.logic.levels.level186.Level186Easy;
import com.razvivashka.logic.levels.level186.Level186Hard;
import com.razvivashka.logic.levels.level186.Level186Normal;
import com.razvivashka.logic.levels.level187.Level187;
import com.razvivashka.logic.levels.level187.Level187Easy;
import com.razvivashka.logic.levels.level187.Level187Hard;
import com.razvivashka.logic.levels.level187.Level187Normal;
import com.razvivashka.logic.levels.level188.Level188;
import com.razvivashka.logic.levels.level188.Level188Easy;
import com.razvivashka.logic.levels.level188.Level188Hard;
import com.razvivashka.logic.levels.level188.Level188Normal;
import com.razvivashka.logic.levels.level189.Level189;
import com.razvivashka.logic.levels.level189.Level189Easy;
import com.razvivashka.logic.levels.level189.Level189Hard;
import com.razvivashka.logic.levels.level189.Level189Normal;
import com.razvivashka.logic.levels.level19.Level19;
import com.razvivashka.logic.levels.level19.Level19Easy;
import com.razvivashka.logic.levels.level19.Level19Hard;
import com.razvivashka.logic.levels.level19.Level19Normal;
import com.razvivashka.logic.levels.level190.Level190;
import com.razvivashka.logic.levels.level190.Level190Easy;
import com.razvivashka.logic.levels.level190.Level190Hard;
import com.razvivashka.logic.levels.level190.Level190Normal;
import com.razvivashka.logic.levels.level191.Level191;
import com.razvivashka.logic.levels.level191.Level191Easy;
import com.razvivashka.logic.levels.level191.Level191Hard;
import com.razvivashka.logic.levels.level191.Level191Normal;
import com.razvivashka.logic.levels.level192.Level192;
import com.razvivashka.logic.levels.level192.Level192Easy;
import com.razvivashka.logic.levels.level192.Level192Hard;
import com.razvivashka.logic.levels.level192.Level192Normal;
import com.razvivashka.logic.levels.level193.Level193;
import com.razvivashka.logic.levels.level193.Level193Easy;
import com.razvivashka.logic.levels.level193.Level193Hard;
import com.razvivashka.logic.levels.level193.Level193Normal;
import com.razvivashka.logic.levels.level194.Level194;
import com.razvivashka.logic.levels.level194.Level194Easy;
import com.razvivashka.logic.levels.level194.Level194Hard;
import com.razvivashka.logic.levels.level194.Level194Normal;
import com.razvivashka.logic.levels.level195.Level195;
import com.razvivashka.logic.levels.level195.Level195Easy;
import com.razvivashka.logic.levels.level195.Level195Hard;
import com.razvivashka.logic.levels.level195.Level195Normal;
import com.razvivashka.logic.levels.level196.Level196;
import com.razvivashka.logic.levels.level196.Level196Easy;
import com.razvivashka.logic.levels.level196.Level196Hard;
import com.razvivashka.logic.levels.level196.Level196Normal;
import com.razvivashka.logic.levels.level197.Level197;
import com.razvivashka.logic.levels.level197.Level197Easy;
import com.razvivashka.logic.levels.level197.Level197Hard;
import com.razvivashka.logic.levels.level197.Level197Normal;
import com.razvivashka.logic.levels.level198.Level198;
import com.razvivashka.logic.levels.level198.Level198Easy;
import com.razvivashka.logic.levels.level198.Level198Hard;
import com.razvivashka.logic.levels.level198.Level198Normal;
import com.razvivashka.logic.levels.level199.Level199;
import com.razvivashka.logic.levels.level199.Level199Easy;
import com.razvivashka.logic.levels.level199.Level199Hard;
import com.razvivashka.logic.levels.level199.Level199Normal;
import com.razvivashka.logic.levels.level2.Level2;
import com.razvivashka.logic.levels.level2.Level2Easy;
import com.razvivashka.logic.levels.level2.Level2Hard;
import com.razvivashka.logic.levels.level2.Level2Normal;
import com.razvivashka.logic.levels.level200.Level200;
import com.razvivashka.logic.levels.level200.Level200Easy;
import com.razvivashka.logic.levels.level200.Level200Hard;
import com.razvivashka.logic.levels.level200.Level200Normal;
import com.razvivashka.logic.levels.level25.Level25;
import com.razvivashka.logic.levels.level25.Level25Easy;
import com.razvivashka.logic.levels.level25.Level25Hard;
import com.razvivashka.logic.levels.level25.Level25Normal;
import com.razvivashka.logic.levels.level26.Level26;
import com.razvivashka.logic.levels.level26.Level26Easy;
import com.razvivashka.logic.levels.level26.Level26Hard;
import com.razvivashka.logic.levels.level26.Level26Normal;
import com.razvivashka.logic.levels.level27.Level27;
import com.razvivashka.logic.levels.level27.Level27Easy;
import com.razvivashka.logic.levels.level27.Level27Hard;
import com.razvivashka.logic.levels.level27.Level27Normal;
import com.razvivashka.logic.levels.level28.Level28;
import com.razvivashka.logic.levels.level28.Level28Easy;
import com.razvivashka.logic.levels.level28.Level28Hard;
import com.razvivashka.logic.levels.level28.Level28Normal;
import com.razvivashka.logic.levels.level3.Level3;
import com.razvivashka.logic.levels.level3.Level3Easy;
import com.razvivashka.logic.levels.level3.Level3Hard;
import com.razvivashka.logic.levels.level3.Level3Normal;
import com.razvivashka.logic.levels.level31.Level31;
import com.razvivashka.logic.levels.level31.Level31Easy;
import com.razvivashka.logic.levels.level31.Level31Hard;
import com.razvivashka.logic.levels.level31.Level31Normal;
import com.razvivashka.logic.levels.level32.Level32;
import com.razvivashka.logic.levels.level32.Level32Easy;
import com.razvivashka.logic.levels.level32.Level32Hard;
import com.razvivashka.logic.levels.level32.Level32Normal;
import com.razvivashka.logic.levels.level33.Level33;
import com.razvivashka.logic.levels.level33.Level33Easy;
import com.razvivashka.logic.levels.level33.Level33Hard;
import com.razvivashka.logic.levels.level33.Level33Normal;
import com.razvivashka.logic.levels.level4.Level4;
import com.razvivashka.logic.levels.level4.Level4Easy;
import com.razvivashka.logic.levels.level4.Level4Hard;
import com.razvivashka.logic.levels.level4.Level4Normal;
import com.razvivashka.logic.levels.level40.Level40;
import com.razvivashka.logic.levels.level40.Level40Easy;
import com.razvivashka.logic.levels.level40.Level40Hard;
import com.razvivashka.logic.levels.level40.Level40Normal;
import com.razvivashka.logic.levels.level43.Level43;
import com.razvivashka.logic.levels.level43.Level43Easy;
import com.razvivashka.logic.levels.level43.Level43Hard;
import com.razvivashka.logic.levels.level43.Level43Normal;
import com.razvivashka.logic.levels.level45.Level45;
import com.razvivashka.logic.levels.level45.Level45Easy;
import com.razvivashka.logic.levels.level45.Level45Hard;
import com.razvivashka.logic.levels.level45.Level45Normal;
import com.razvivashka.logic.levels.level46.Level46;
import com.razvivashka.logic.levels.level46.Level46Easy;
import com.razvivashka.logic.levels.level46.Level46Hard;
import com.razvivashka.logic.levels.level46.Level46Normal;
import com.razvivashka.logic.levels.level48.Level48;
import com.razvivashka.logic.levels.level48.Level48Easy;
import com.razvivashka.logic.levels.level48.Level48Hard;
import com.razvivashka.logic.levels.level48.Level48Normal;
import com.razvivashka.logic.levels.level5.Level5;
import com.razvivashka.logic.levels.level5.Level5Easy;
import com.razvivashka.logic.levels.level5.Level5Hard;
import com.razvivashka.logic.levels.level5.Level5Normal;
import com.razvivashka.logic.levels.level51.Level51;
import com.razvivashka.logic.levels.level51.Level51Easy;
import com.razvivashka.logic.levels.level51.Level51Hard;
import com.razvivashka.logic.levels.level51.Level51Normal;
import com.razvivashka.logic.levels.level55.Level55;
import com.razvivashka.logic.levels.level55.Level55Easy;
import com.razvivashka.logic.levels.level55.Level55Hard;
import com.razvivashka.logic.levels.level55.Level55Normal;
import com.razvivashka.logic.levels.level6.Level6;
import com.razvivashka.logic.levels.level6.Level6Easy;
import com.razvivashka.logic.levels.level6.Level6Hard;
import com.razvivashka.logic.levels.level6.Level6Normal;
import com.razvivashka.logic.levels.level61.Level61;
import com.razvivashka.logic.levels.level61.Level61Easy;
import com.razvivashka.logic.levels.level61.Level61Hard;
import com.razvivashka.logic.levels.level61.Level61Normal;
import com.razvivashka.logic.levels.level62.Level62;
import com.razvivashka.logic.levels.level62.Level62Easy;
import com.razvivashka.logic.levels.level62.Level62Hard;
import com.razvivashka.logic.levels.level62.Level62Normal;
import com.razvivashka.logic.levels.level63.Level63;
import com.razvivashka.logic.levels.level63.Level63Easy;
import com.razvivashka.logic.levels.level63.Level63Hard;
import com.razvivashka.logic.levels.level63.Level63Normal;
import com.razvivashka.logic.levels.level64.Level64;
import com.razvivashka.logic.levels.level64.Level64Easy;
import com.razvivashka.logic.levels.level64.Level64Hard;
import com.razvivashka.logic.levels.level64.Level64Normal;
import com.razvivashka.logic.levels.level65.Level65;
import com.razvivashka.logic.levels.level65.Level65Easy;
import com.razvivashka.logic.levels.level65.Level65Hard;
import com.razvivashka.logic.levels.level65.Level65Normal;
import com.razvivashka.logic.levels.level66.Level66;
import com.razvivashka.logic.levels.level66.Level66Easy;
import com.razvivashka.logic.levels.level66.Level66Hard;
import com.razvivashka.logic.levels.level66.Level66Normal;
import com.razvivashka.logic.levels.level67.Level67;
import com.razvivashka.logic.levels.level67.Level67Easy;
import com.razvivashka.logic.levels.level67.Level67Hard;
import com.razvivashka.logic.levels.level67.Level67Normal;
import com.razvivashka.logic.levels.level68.Level68;
import com.razvivashka.logic.levels.level68.Level68Easy;
import com.razvivashka.logic.levels.level68.Level68Hard;
import com.razvivashka.logic.levels.level68.Level68Normal;
import com.razvivashka.logic.levels.level69.Level69;
import com.razvivashka.logic.levels.level69.Level69Easy;
import com.razvivashka.logic.levels.level69.Level69Hard;
import com.razvivashka.logic.levels.level69.Level69Normal;
import com.razvivashka.logic.levels.level7.Level7;
import com.razvivashka.logic.levels.level7.Level7Easy;
import com.razvivashka.logic.levels.level7.Level7Hard;
import com.razvivashka.logic.levels.level7.Level7Normal;
import com.razvivashka.logic.levels.level70.Level70;
import com.razvivashka.logic.levels.level70.Level70Easy;
import com.razvivashka.logic.levels.level70.Level70Hard;
import com.razvivashka.logic.levels.level70.Level70Normal;
import com.razvivashka.logic.levels.level71.Level71;
import com.razvivashka.logic.levels.level71.Level71Easy;
import com.razvivashka.logic.levels.level71.Level71Hard;
import com.razvivashka.logic.levels.level71.Level71Normal;
import com.razvivashka.logic.levels.level72.Level72;
import com.razvivashka.logic.levels.level72.Level72Easy;
import com.razvivashka.logic.levels.level72.Level72Hard;
import com.razvivashka.logic.levels.level72.Level72Normal;
import com.razvivashka.logic.levels.level73.Level73;
import com.razvivashka.logic.levels.level73.Level73Easy;
import com.razvivashka.logic.levels.level73.Level73Hard;
import com.razvivashka.logic.levels.level73.Level73Normal;
import com.razvivashka.logic.levels.level74.Level74;
import com.razvivashka.logic.levels.level74.Level74Easy;
import com.razvivashka.logic.levels.level74.Level74Hard;
import com.razvivashka.logic.levels.level74.Level74Normal;
import com.razvivashka.logic.levels.level75.Level75;
import com.razvivashka.logic.levels.level75.Level75Easy;
import com.razvivashka.logic.levels.level75.Level75Hard;
import com.razvivashka.logic.levels.level75.Level75Normal;
import com.razvivashka.logic.levels.level76.Level76;
import com.razvivashka.logic.levels.level76.Level76Easy;
import com.razvivashka.logic.levels.level76.Level76Hard;
import com.razvivashka.logic.levels.level76.Level76Normal;
import com.razvivashka.logic.levels.level77.Level77;
import com.razvivashka.logic.levels.level77.Level77Easy;
import com.razvivashka.logic.levels.level77.Level77Hard;
import com.razvivashka.logic.levels.level77.Level77Normal;
import com.razvivashka.logic.levels.level78.Level78;
import com.razvivashka.logic.levels.level78.Level78Easy;
import com.razvivashka.logic.levels.level78.Level78Hard;
import com.razvivashka.logic.levels.level78.Level78Normal;
import com.razvivashka.logic.levels.level79.Level79;
import com.razvivashka.logic.levels.level79.Level79Easy;
import com.razvivashka.logic.levels.level79.Level79Hard;
import com.razvivashka.logic.levels.level79.Level79Normal;
import com.razvivashka.logic.levels.level80.Level80;
import com.razvivashka.logic.levels.level80.Level80Easy;
import com.razvivashka.logic.levels.level80.Level80Hard;
import com.razvivashka.logic.levels.level80.Level80Normal;
import com.razvivashka.logic.levels.level81.Level81;
import com.razvivashka.logic.levels.level81.Level81Easy;
import com.razvivashka.logic.levels.level81.Level81Hard;
import com.razvivashka.logic.levels.level81.Level81Normal;
import com.razvivashka.logic.levels.level82.Level82;
import com.razvivashka.logic.levels.level82.Level82Easy;
import com.razvivashka.logic.levels.level82.Level82Hard;
import com.razvivashka.logic.levels.level82.Level82Normal;
import com.razvivashka.logic.levels.level83.Level83;
import com.razvivashka.logic.levels.level83.Level83Easy;
import com.razvivashka.logic.levels.level83.Level83Hard;
import com.razvivashka.logic.levels.level83.Level83Normal;
import com.razvivashka.logic.levels.level84.Level84;
import com.razvivashka.logic.levels.level84.Level84Easy;
import com.razvivashka.logic.levels.level84.Level84Hard;
import com.razvivashka.logic.levels.level84.Level84Normal;
import com.razvivashka.logic.levels.level85.Level85;
import com.razvivashka.logic.levels.level85.Level85Easy;
import com.razvivashka.logic.levels.level85.Level85Hard;
import com.razvivashka.logic.levels.level85.Level85Normal;
import com.razvivashka.logic.levels.level86.Level86;
import com.razvivashka.logic.levels.level86.Level86Easy;
import com.razvivashka.logic.levels.level86.Level86Hard;
import com.razvivashka.logic.levels.level86.Level86Normal;
import com.razvivashka.logic.levels.level87.Level87;
import com.razvivashka.logic.levels.level87.Level87Easy;
import com.razvivashka.logic.levels.level87.Level87Hard;
import com.razvivashka.logic.levels.level87.Level87Normal;
import com.razvivashka.logic.levels.level88.Level88;
import com.razvivashka.logic.levels.level88.Level88Easy;
import com.razvivashka.logic.levels.level88.Level88Hard;
import com.razvivashka.logic.levels.level88.Level88Normal;
import com.razvivashka.logic.levels.level89.Level89;
import com.razvivashka.logic.levels.level89.Level89Easy;
import com.razvivashka.logic.levels.level89.Level89Hard;
import com.razvivashka.logic.levels.level89.Level89Normal;
import com.razvivashka.logic.levels.level90.Level90;
import com.razvivashka.logic.levels.level90.Level90Easy;
import com.razvivashka.logic.levels.level90.Level90Hard;
import com.razvivashka.logic.levels.level90.Level90Normal;
import com.razvivashka.logic.levels.level91.Level91;
import com.razvivashka.logic.levels.level91.Level91Easy;
import com.razvivashka.logic.levels.level91.Level91Hard;
import com.razvivashka.logic.levels.level91.Level91Normal;
import com.razvivashka.logic.levels.level92.Level92;
import com.razvivashka.logic.levels.level92.Level92Easy;
import com.razvivashka.logic.levels.level92.Level92Hard;
import com.razvivashka.logic.levels.level92.Level92Normal;
import com.razvivashka.logic.levels.level93.Level93;
import com.razvivashka.logic.levels.level93.Level93Easy;
import com.razvivashka.logic.levels.level93.Level93Hard;
import com.razvivashka.logic.levels.level93.Level93Normal;
import com.razvivashka.logic.levels.level94.Level94;
import com.razvivashka.logic.levels.level94.Level94Easy;
import com.razvivashka.logic.levels.level94.Level94Hard;
import com.razvivashka.logic.levels.level94.Level94Normal;
import com.razvivashka.logic.levels.level95.Level95;
import com.razvivashka.logic.levels.level95.Level95Easy;
import com.razvivashka.logic.levels.level95.Level95Hard;
import com.razvivashka.logic.levels.level95.Level95Normal;
import com.razvivashka.logic.levels.level96.Level96;
import com.razvivashka.logic.levels.level96.Level96Easy;
import com.razvivashka.logic.levels.level96.Level96Hard;
import com.razvivashka.logic.levels.level96.Level96Normal;
import com.razvivashka.logic.levels.level97.Level97;
import com.razvivashka.logic.levels.level97.Level97Easy;
import com.razvivashka.logic.levels.level97.Level97Hard;
import com.razvivashka.logic.levels.level97.Level97Normal;
import com.razvivashka.logic.levels.level98.Level98;
import com.razvivashka.logic.levels.level98.Level98Easy;
import com.razvivashka.logic.levels.level98.Level98Hard;
import com.razvivashka.logic.levels.level98.Level98Normal;
import com.razvivashka.logic.levels.level99.Level99;
import com.razvivashka.logic.levels.level99.Level99Easy;
import com.razvivashka.logic.levels.level99.Level99Hard;
import com.razvivashka.logic.levels.level99.Level99Normal;

import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class GameLevelFactory {



	public GameLevel getGameLevel (int level, GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevel gameLevel = null;

		switch (level) {
			case 1:
				gameLevel = getLevel1(gameDifficulty, heroCellTypes);
				break;
			case 2:
				gameLevel = getLevel2(gameDifficulty, heroCellTypes);
				break;
			case 3:
				gameLevel = getLevel3(gameDifficulty, heroCellTypes);
				break;
			case 4:
				gameLevel = getLevel4(gameDifficulty, heroCellTypes);
				break;
			case 5:
				gameLevel = getLevel5(gameDifficulty, heroCellTypes);
				break;
			case 6:
				gameLevel = getLevel6(gameDifficulty, heroCellTypes);
				break;
			case 7:
				gameLevel = getLevel7(gameDifficulty, heroCellTypes);
				break;
			case 14:
				gameLevel = getLevel14(gameDifficulty, heroCellTypes);
				break;
			case 15:
				gameLevel = getLevel15(gameDifficulty, heroCellTypes);
				break;
			case 16:
				gameLevel = getLevel16(gameDifficulty, heroCellTypes);
				break;
			case 18:
				gameLevel = getLevel18(gameDifficulty, heroCellTypes);
				break;
			case 19:
				gameLevel = getLevel19(gameDifficulty, heroCellTypes);
				break;
			case 25:
				gameLevel = getLevel25(gameDifficulty, heroCellTypes);
				break;
			case 26:
				gameLevel = getLevel26(gameDifficulty, heroCellTypes);
				break;
			case 27:
				gameLevel = getLevel27(gameDifficulty, heroCellTypes);
				break;
			case 28:
				gameLevel = getLevel28(gameDifficulty, heroCellTypes);
				break;
			case 31:
				gameLevel = getLevel31(gameDifficulty, heroCellTypes);
				break;
			case 32:
				gameLevel = getLevel32(gameDifficulty, heroCellTypes);
				break;
			case 33:
				gameLevel = getLevel33(gameDifficulty, heroCellTypes);
				break;
			case 40:
				gameLevel = getLevel40(gameDifficulty, heroCellTypes);
				break;
			case 43:
				gameLevel = getLevel43(gameDifficulty, heroCellTypes);
				break;
			case 45:
				gameLevel = getLevel45(gameDifficulty, heroCellTypes);
				break;
			case 46:
				gameLevel = getLevel46(gameDifficulty, heroCellTypes);
				break;
			case 48:
				gameLevel = getLevel48(gameDifficulty, heroCellTypes);
				break;
			case 51:
				gameLevel = getLevel51(gameDifficulty, heroCellTypes);
				break;
			case 55:
				gameLevel = getLevel55(gameDifficulty, heroCellTypes);
				break;
			case 61:
				gameLevel = getLevel61(gameDifficulty, heroCellTypes);
				break;
			case 62:
				gameLevel = getLevel62(gameDifficulty, heroCellTypes);
				break;
			case 63:
				gameLevel = getLevel63(gameDifficulty, heroCellTypes);
				break;
			case 64:
				gameLevel = getLevel64(gameDifficulty, heroCellTypes);
				break;
			case 65:
				gameLevel = getLevel65(gameDifficulty, heroCellTypes);
				break;
			case 66:
				gameLevel = getLevel66(gameDifficulty, heroCellTypes);
				break;
			case 67:
				gameLevel = getLevel67(gameDifficulty, heroCellTypes);
				break;
			case 68:
				gameLevel = getLevel68(gameDifficulty, heroCellTypes);
				break;
			case 69:
				gameLevel = getLevel69(gameDifficulty, heroCellTypes);
				break;
			case 70:
				gameLevel = getLevel70(gameDifficulty, heroCellTypes);
				break;
			case 71:
				gameLevel = getLevel71(gameDifficulty, heroCellTypes);
				break;
			case 72:
				gameLevel = getLevel72(gameDifficulty, heroCellTypes);
				break;
			case 73:
				gameLevel = getLevel73(gameDifficulty, heroCellTypes);
				break;
			case 74:
				gameLevel = getLevel74(gameDifficulty, heroCellTypes);
				break;
			case 75:
				gameLevel = getLevel75(gameDifficulty, heroCellTypes);
				break;
			case 76:
				gameLevel = getLevel76(gameDifficulty, heroCellTypes);
				break;
			case 77:
				gameLevel = getLevel77(gameDifficulty, heroCellTypes);
				break;
			case 78:
				gameLevel = getLevel78(gameDifficulty, heroCellTypes);
				break;
			case 79:
				gameLevel = getLevel79(gameDifficulty, heroCellTypes);
				break;
			case 80:
				gameLevel = getLevel80(gameDifficulty, heroCellTypes);
				break;
			case 81:
				gameLevel = getLevel81(gameDifficulty, heroCellTypes);
				break;
			case 82:
				gameLevel = getLevel82(gameDifficulty, heroCellTypes);
				break;
			case 83:
				gameLevel = getLevel83(gameDifficulty, heroCellTypes);
				break;
			case 84:
				gameLevel = getLevel84(gameDifficulty, heroCellTypes);
				break;
			case 85:
				gameLevel = getLevel85(gameDifficulty, heroCellTypes);
				break;
			case 86:
				gameLevel = getLevel86(gameDifficulty, heroCellTypes);
				break;
			case 87:
				gameLevel = getLevel87(gameDifficulty, heroCellTypes);
				break;
			case 88:
				gameLevel = getLevel88(gameDifficulty, heroCellTypes);
				break;
			case 89:
				gameLevel = getLevel89(gameDifficulty, heroCellTypes);
				break;
			case 90:
				gameLevel = getLevel90(gameDifficulty, heroCellTypes);
				break;
			case 91:
				gameLevel = getLevel91(gameDifficulty, heroCellTypes);
				break;
			case 92:
				gameLevel = getLevel92(gameDifficulty, heroCellTypes);
				break;
			case 93:
				gameLevel = getLevel93(gameDifficulty, heroCellTypes);
				break;
			case 94:
				gameLevel = getLevel94(gameDifficulty, heroCellTypes);
				break;
			case 95:
				gameLevel = getLevel95(gameDifficulty, heroCellTypes);
				break;
			case 96:
				gameLevel = getLevel96(gameDifficulty, heroCellTypes);
				break;
			case 97:
				gameLevel = getLevel97(gameDifficulty, heroCellTypes);
				break;
			case 98:
				gameLevel = getLevel98(gameDifficulty, heroCellTypes);
				break;
			case 99:
				gameLevel = getLevel99(gameDifficulty, heroCellTypes);
				break;
			case 100:
				gameLevel = getLevel100(gameDifficulty, heroCellTypes);
				break;
			case 101:
				gameLevel = getLevel101(gameDifficulty, heroCellTypes);
				break;
			case 102:
				gameLevel = getLevel102(gameDifficulty, heroCellTypes);
				break;
			case 103:
				gameLevel = getLevel103(gameDifficulty, heroCellTypes);
				break;
			case 104:
				gameLevel = getLevel104(gameDifficulty, heroCellTypes);
				break;
			case 105:
				gameLevel = getLevel105(gameDifficulty, heroCellTypes);
				break;
			case 106:
				gameLevel = getLevel106(gameDifficulty, heroCellTypes);
				break;
			case 107:
				gameLevel = getLevel107(gameDifficulty, heroCellTypes);
				break;
			case 108:
				gameLevel = getLevel108(gameDifficulty, heroCellTypes);
				break;
			case 109:
				gameLevel = getLevel109(gameDifficulty, heroCellTypes);
				break;
			case 110:
				gameLevel = getLevel110(gameDifficulty, heroCellTypes);
				break;
			case 111:
				gameLevel = getLevel111(gameDifficulty, heroCellTypes);
				break;
			case 112:
				gameLevel = getLevel112(gameDifficulty, heroCellTypes);
				break;
			case 113:
				gameLevel = getLevel113(gameDifficulty, heroCellTypes);
				break;
			case 114:
				gameLevel = getLevel114(gameDifficulty, heroCellTypes);
				break;
			case 115:
				gameLevel = getLevel115(gameDifficulty, heroCellTypes);
				break;
			case 116:
				gameLevel = getLevel116(gameDifficulty, heroCellTypes);
				break;
			case 117:
				gameLevel = getLevel117(gameDifficulty, heroCellTypes);
				break;
			case 118:
				gameLevel = getLevel118(gameDifficulty, heroCellTypes);
				break;
			case 119:
				gameLevel = getLevel119(gameDifficulty, heroCellTypes);
				break;
			case 120:
				gameLevel = getLevel120(gameDifficulty, heroCellTypes);
				break;
			case 121:
				gameLevel = getLevel121(gameDifficulty, heroCellTypes);
				break;
			case 122:
				gameLevel = getLevel122(gameDifficulty, heroCellTypes);
				break;
			case 123:
				gameLevel = getLevel123(gameDifficulty, heroCellTypes);
				break;
			case 124:
				gameLevel = getLevel124(gameDifficulty, heroCellTypes);
				break;
			case 125:
				gameLevel = getLevel125(gameDifficulty, heroCellTypes);
				break;
			case 126:
				gameLevel = getLevel126(gameDifficulty, heroCellTypes);
				break;
			case 127:
				gameLevel = getLevel127(gameDifficulty, heroCellTypes);
				break;
			case 128:
				gameLevel = getLevel128(gameDifficulty, heroCellTypes);
				break;
			case 129:
				gameLevel = getLevel129(gameDifficulty, heroCellTypes);
				break;
			case 130:
				gameLevel = getLevel130(gameDifficulty, heroCellTypes);
				break;
			case 131:
				gameLevel = getLevel131(gameDifficulty, heroCellTypes);
				break;
			case 132:
				gameLevel = getLevel132(gameDifficulty, heroCellTypes);
				break;
			case 133:
				gameLevel = getLevel133(gameDifficulty, heroCellTypes);
				break;
			case 134:
				gameLevel = getLevel134(gameDifficulty, heroCellTypes);
				break;
			case 135:
				gameLevel = getLevel135(gameDifficulty, heroCellTypes);
				break;
			case 136:
				gameLevel = getLevel136(gameDifficulty, heroCellTypes);
				break;
			case 137:
				gameLevel = getLevel137(gameDifficulty, heroCellTypes);
				break;
			case 138:
				gameLevel = getLevel138(gameDifficulty, heroCellTypes);
				break;
			case 139:
				gameLevel = getLevel139(gameDifficulty, heroCellTypes);
				break;
			case 140:
				gameLevel = getLevel140(gameDifficulty, heroCellTypes);
				break;
			case 141:
				gameLevel = getLevel141(gameDifficulty, heroCellTypes);
				break;
			case 142:
				gameLevel = getLevel142(gameDifficulty, heroCellTypes);
				break;
			case 143:
				gameLevel = getLevel143(gameDifficulty, heroCellTypes);
				break;
			case 144:
				gameLevel = getLevel144(gameDifficulty, heroCellTypes);
				break;
			case 145:
				gameLevel = getLevel145(gameDifficulty, heroCellTypes);
				break;
			case 146:
				gameLevel = getLevel146(gameDifficulty, heroCellTypes);
				break;
			case 147:
				gameLevel = getLevel147(gameDifficulty, heroCellTypes);
				break;
			case 148:
				gameLevel = getLevel148(gameDifficulty, heroCellTypes);
				break;
			case 149:
				gameLevel = getLevel149(gameDifficulty, heroCellTypes);
				break;
			case 150:
				gameLevel = getLevel150(gameDifficulty, heroCellTypes);
				break;
			case 151:
				gameLevel = getLevel151(gameDifficulty, heroCellTypes);
				break;
			case 152:
				gameLevel = getLevel152(gameDifficulty, heroCellTypes);
				break;
			case 153:
				gameLevel = getLevel153(gameDifficulty, heroCellTypes);
				break;
			case 154:
				gameLevel = getLevel154(gameDifficulty, heroCellTypes);
				break;
			case 155:
				gameLevel = getLevel155(gameDifficulty, heroCellTypes);
				break;
			case 156:
				gameLevel = getLevel156(gameDifficulty, heroCellTypes);
				break;
			case 157:
				gameLevel = getLevel157(gameDifficulty, heroCellTypes);
				break;
			case 158:
				gameLevel = getLevel158(gameDifficulty, heroCellTypes);
				break;
			case 159:
				gameLevel = getLevel159(gameDifficulty, heroCellTypes);
				break;
			case 160:
				gameLevel = getLevel160(gameDifficulty, heroCellTypes);
				break;
			case 161:
				gameLevel = getLevel161(gameDifficulty, heroCellTypes);
				break;
			case 162:
				gameLevel = getLevel162(gameDifficulty, heroCellTypes);
				break;
			case 163:
				gameLevel = getLevel163(gameDifficulty, heroCellTypes);
				break;
			case 164:
				gameLevel = getLevel164(gameDifficulty, heroCellTypes);
				break;
			case 165:
				gameLevel = getLevel165(gameDifficulty, heroCellTypes);
				break;
			case 166:
				gameLevel = getLevel166(gameDifficulty, heroCellTypes);
				break;
			case 167:
				gameLevel = getLevel167(gameDifficulty, heroCellTypes);
				break;
			case 168:
				gameLevel = getLevel168(gameDifficulty, heroCellTypes);
				break;
			case 169:
				gameLevel = getLevel169(gameDifficulty, heroCellTypes);
				break;
			case 170:
				gameLevel = getLevel170(gameDifficulty, heroCellTypes);
				break;
			case 171:
				gameLevel = getLevel171(gameDifficulty, heroCellTypes);
				break;
			case 172:
				gameLevel = getLevel172(gameDifficulty, heroCellTypes);
				break;
			case 173:
				gameLevel = getLevel173(gameDifficulty, heroCellTypes);
				break;
			case 174:
				gameLevel = getLevel174(gameDifficulty, heroCellTypes);
				break;
			case 175:
				gameLevel = getLevel175(gameDifficulty, heroCellTypes);
				break;
			case 176:
				gameLevel = getLevel176(gameDifficulty, heroCellTypes);
				break;
			case 177:
				gameLevel = getLevel177(gameDifficulty, heroCellTypes);
				break;
			case 178:
				gameLevel = getLevel178(gameDifficulty, heroCellTypes);
				break;
			case 179:
				gameLevel = getLevel179(gameDifficulty, heroCellTypes);
				break;
			case 180:
				gameLevel = getLevel180(gameDifficulty, heroCellTypes);
				break;
			case 181:
				gameLevel = getLevel181(gameDifficulty, heroCellTypes);
				break;
			case 182:
				gameLevel = getLevel182(gameDifficulty, heroCellTypes);
				break;
			case 183:
				gameLevel = getLevel183(gameDifficulty, heroCellTypes);
				break;
			case 184:
				gameLevel = getLevel184(gameDifficulty, heroCellTypes);
				break;
			case 185:
				gameLevel = getLevel185(gameDifficulty, heroCellTypes);
				break;
			case 186:
				gameLevel = getLevel186(gameDifficulty, heroCellTypes);
				break;
			case 187:
				gameLevel = getLevel187(gameDifficulty, heroCellTypes);
				break;
			case 188:
				gameLevel = getLevel188(gameDifficulty, heroCellTypes);
				break;
			case 189:
				gameLevel = getLevel189(gameDifficulty, heroCellTypes);
				break;
			case 190:
				gameLevel = getLevel190(gameDifficulty, heroCellTypes);
				break;
			case 191:
				gameLevel = getLevel191(gameDifficulty, heroCellTypes);
				break;
			case 192:
				gameLevel = getLevel192(gameDifficulty, heroCellTypes);
				break;
			case 193:
				gameLevel = getLevel193(gameDifficulty, heroCellTypes);
				break;
			case 194:
				gameLevel = getLevel194(gameDifficulty, heroCellTypes);
				break;
			case 195:
				gameLevel = getLevel195(gameDifficulty, heroCellTypes);
				break;
			case 196:
				gameLevel = getLevel196(gameDifficulty, heroCellTypes);
				break;
			case 197:
				gameLevel = getLevel197(gameDifficulty, heroCellTypes);
				break;
			case 198:
				gameLevel = getLevel198(gameDifficulty, heroCellTypes);
				break;
			case 199:
				gameLevel = getLevel199(gameDifficulty, heroCellTypes);
				break;
			case 200:
				gameLevel = getLevel200(gameDifficulty, heroCellTypes);
				break;
		}

		return gameLevel;
	}

	public Level1 getLevel1 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level1Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level1Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level1Hard(heroCellTypes);
				break;
		}

		return new Level1(gameLevelData);
	}
	public Level2 getLevel2 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level2Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level2Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level2Hard(heroCellTypes);
				break;
		}

		return new Level2(gameLevelData);
	}
	public Level3 getLevel3 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level3Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level3Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level3Hard(heroCellTypes);
				break;
		}

		return new Level3(gameLevelData);
	}
	public Level4 getLevel4 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level4Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level4Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level4Hard(heroCellTypes);
				break;
		}

		return new Level4(gameLevelData);
	}
	public Level5 getLevel5 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level5Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level5Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level5Hard(heroCellTypes);
				break;
		}

		return new Level5(gameLevelData);
	}
	public Level6 getLevel6 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level6Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level6Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level6Hard(heroCellTypes);
				break;
		}

		return new Level6(gameLevelData);
	}
	public Level7 getLevel7 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level7Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level7Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level7Hard(heroCellTypes);
				break;
		}

		return new Level7(gameLevelData);
	}
	public Level14 getLevel14 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level14Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level14Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level14Hard(heroCellTypes);
				break;
		}

		return new Level14(gameLevelData);
	}
	public Level15 getLevel15 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level15Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level15Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level15Hard(heroCellTypes);
				break;
		}

		return new Level15(gameLevelData);
	}
	public Level16 getLevel16 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level16Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level16Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level16Hard(heroCellTypes);
				break;
		}

		return new Level16(gameLevelData);
	}
	public Level18 getLevel18 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level18Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level18Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level18Hard(heroCellTypes);
				break;
		}

		return new Level18(gameLevelData);
	}
	public Level19 getLevel19 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level19Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level19Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level19Hard(heroCellTypes);
				break;
		}

		return new Level19(gameLevelData);
	}
	public Level25 getLevel25 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level25Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level25Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level25Hard(heroCellTypes);
				break;
		}

		return new Level25(gameLevelData);
	}
	public Level26 getLevel26 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level26Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level26Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level26Hard(heroCellTypes);
				break;
		}

		return new Level26(gameLevelData);
	}
	public Level27 getLevel27 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level27Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level27Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level27Hard(heroCellTypes);
				break;
		}

		return new Level27(gameLevelData);
	}
	public Level28 getLevel28 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level28Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level28Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level28Hard(heroCellTypes);
				break;
		}

		return new Level28(gameLevelData);
	}
	public Level31 getLevel31 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level31Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level31Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level31Hard(heroCellTypes);
				break;
		}

		return new Level31(gameLevelData);
	}
	public Level32 getLevel32 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level32Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level32Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level32Hard(heroCellTypes);
				break;
		}

		return new Level32(gameLevelData);
	}
	public Level33 getLevel33 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level33Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level33Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level33Hard(heroCellTypes);
				break;
		}

		return new Level33(gameLevelData);
	}
	public Level40 getLevel40 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level40Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level40Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level40Hard(heroCellTypes);
				break;
		}

		return new Level40(gameLevelData);
	}
	public Level43 getLevel43 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level43Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level43Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level43Hard(heroCellTypes);
				break;
		}

		return new Level43(gameLevelData);
	}
	public Level45 getLevel45 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level45Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level45Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level45Hard(heroCellTypes);
				break;
		}

		return new Level45(gameLevelData);
	}
	public Level46 getLevel46 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level46Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level46Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level46Hard(heroCellTypes);
				break;
		}

		return new Level46(gameLevelData);
	}
	public Level48 getLevel48 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level48Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level48Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level48Hard(heroCellTypes);
				break;
		}

		return new Level48(gameLevelData);
	}
	public Level51 getLevel51 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level51Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level51Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level51Hard(heroCellTypes);
				break;
		}

		return new Level51(gameLevelData);
	}
	public Level55 getLevel55 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level55Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level55Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level55Hard(heroCellTypes);
				break;
		}

		return new Level55(gameLevelData);
	}
	public Level61 getLevel61 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level61Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level61Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level61Hard(heroCellTypes);
				break;
		}

		return new Level61(gameLevelData);
	}
	public Level62 getLevel62 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level62Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level62Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level62Hard(heroCellTypes);
				break;
		}

		return new Level62(gameLevelData);
	}
	public Level63 getLevel63 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level63Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level63Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level63Hard(heroCellTypes);
				break;
		}

		return new Level63(gameLevelData);
	}
	public Level64 getLevel64 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level64Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level64Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level64Hard(heroCellTypes);
				break;
		}

		return new Level64(gameLevelData);
	}
	public Level65 getLevel65 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level65Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level65Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level65Hard(heroCellTypes);
				break;
		}

		return new Level65(gameLevelData);
	}
	public Level66 getLevel66 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level66Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level66Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level66Hard(heroCellTypes);
				break;
		}

		return new Level66(gameLevelData);
	}
	public Level67 getLevel67 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level67Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level67Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level67Hard(heroCellTypes);
				break;
		}

		return new Level67(gameLevelData);
	}
	public Level68 getLevel68 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level68Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level68Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level68Hard(heroCellTypes);
				break;
		}

		return new Level68(gameLevelData);
	}
	public Level69 getLevel69 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level69Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level69Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level69Hard(heroCellTypes);
				break;
		}

		return new Level69(gameLevelData);
	}
	public Level70 getLevel70 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level70Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level70Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level70Hard(heroCellTypes);
				break;
		}

		return new Level70(gameLevelData);
	}
	public Level71 getLevel71 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level71Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level71Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level71Hard(heroCellTypes);
				break;
		}

		return new Level71(gameLevelData);
	}
	public Level72 getLevel72 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level72Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level72Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level72Hard(heroCellTypes);
				break;
		}

		return new Level72(gameLevelData);
	}
	public Level73 getLevel73 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level73Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level73Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level73Hard(heroCellTypes);
				break;
		}

		return new Level73(gameLevelData);
	}
	public Level74 getLevel74 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level74Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level74Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level74Hard(heroCellTypes);
				break;
		}

		return new Level74(gameLevelData);
	}
	public Level75 getLevel75 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level75Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level75Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level75Hard(heroCellTypes);
				break;
		}

		return new Level75(gameLevelData);
	}
	public Level76 getLevel76 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level76Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level76Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level76Hard(heroCellTypes);
				break;
		}

		return new Level76(gameLevelData);
	}
	public Level77 getLevel77 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level77Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level77Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level77Hard(heroCellTypes);
				break;
		}

		return new Level77(gameLevelData);
	}
	public Level78 getLevel78 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level78Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level78Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level78Hard(heroCellTypes);
				break;
		}

		return new Level78(gameLevelData);
	}
	public Level79 getLevel79 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level79Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level79Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level79Hard(heroCellTypes);
				break;
		}

		return new Level79(gameLevelData);
	}
	public Level80 getLevel80 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level80Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level80Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level80Hard(heroCellTypes);
				break;
		}

		return new Level80(gameLevelData);
	}
	public Level81 getLevel81 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level81Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level81Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level81Hard(heroCellTypes);
				break;
		}

		return new Level81(gameLevelData);
	}
	public Level82 getLevel82 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level82Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level82Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level82Hard(heroCellTypes);
				break;
		}

		return new Level82(gameLevelData);
	}
	public Level83 getLevel83 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level83Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level83Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level83Hard(heroCellTypes);
				break;
		}

		return new Level83(gameLevelData);
	}
	public Level84 getLevel84 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level84Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level84Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level84Hard(heroCellTypes);
				break;
		}

		return new Level84(gameLevelData);
	}
	public Level85 getLevel85 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level85Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level85Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level85Hard(heroCellTypes);
				break;
		}

		return new Level85(gameLevelData);
	}
	public Level86 getLevel86 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level86Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level86Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level86Hard(heroCellTypes);
				break;
		}

		return new Level86(gameLevelData);
	}
	public Level87 getLevel87 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level87Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level87Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level87Hard(heroCellTypes);
				break;
		}

		return new Level87(gameLevelData);
	}
	public Level88 getLevel88 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level88Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level88Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level88Hard(heroCellTypes);
				break;
		}

		return new Level88(gameLevelData);
	}
	public Level89 getLevel89 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level89Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level89Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level89Hard(heroCellTypes);
				break;
		}

		return new Level89(gameLevelData);
	}
	public Level90 getLevel90 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level90Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level90Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level90Hard(heroCellTypes);
				break;
		}

		return new Level90(gameLevelData);
	}
	public Level91 getLevel91 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level91Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level91Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level91Hard(heroCellTypes);
				break;
		}

		return new Level91(gameLevelData);
	}
	public Level92 getLevel92 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level92Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level92Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level92Hard(heroCellTypes);
				break;
		}

		return new Level92(gameLevelData);
	}
	public Level93 getLevel93 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level93Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level93Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level93Hard(heroCellTypes);
				break;
		}

		return new Level93(gameLevelData);
	}
	public Level94 getLevel94 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level94Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level94Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level94Hard(heroCellTypes);
				break;
		}

		return new Level94(gameLevelData);
	}
	public Level95 getLevel95 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level95Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level95Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level95Hard(heroCellTypes);
				break;
		}

		return new Level95(gameLevelData);
	}
	public Level96 getLevel96 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level96Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level96Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level96Hard(heroCellTypes);
				break;
		}

		return new Level96(gameLevelData);
	}
	public Level97 getLevel97 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level97Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level97Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level97Hard(heroCellTypes);
				break;
		}

		return new Level97(gameLevelData);
	}
	public Level98 getLevel98 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level98Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level98Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level98Hard(heroCellTypes);
				break;
		}

		return new Level98(gameLevelData);
	}
	public Level99 getLevel99 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level99Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level99Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level99Hard(heroCellTypes);
				break;
		}

		return new Level99(gameLevelData);
	}
	public Level100 getLevel100 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level100Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level100Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level100Hard(heroCellTypes);
				break;
		}

		return new Level100(gameLevelData);
	}
	public Level101 getLevel101 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level101Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level101Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level101Hard(heroCellTypes);
				break;
		}

		return new Level101(gameLevelData);
	}
	public Level102 getLevel102 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level102Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level102Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level102Hard(heroCellTypes);
				break;
		}

		return new Level102(gameLevelData);
	}
	public Level103 getLevel103 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level103Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level103Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level103Hard(heroCellTypes);
				break;
		}

		return new Level103(gameLevelData);
	}
	public Level104 getLevel104 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level104Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level104Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level104Hard(heroCellTypes);
				break;
		}

		return new Level104(gameLevelData);
	}
	public Level105 getLevel105 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level105Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level105Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level105Hard(heroCellTypes);
				break;
		}

		return new Level105(gameLevelData);
	}
	public Level106 getLevel106 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level106Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level106Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level106Hard(heroCellTypes);
				break;
		}

		return new Level106(gameLevelData);
	}
	public Level107 getLevel107 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level107Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level107Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level107Hard(heroCellTypes);
				break;
		}

		return new Level107(gameLevelData);
	}
	public Level108 getLevel108 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level108Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level108Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level108Hard(heroCellTypes);
				break;
		}

		return new Level108(gameLevelData);
	}
	public Level109 getLevel109 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level109Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level109Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level109Hard(heroCellTypes);
				break;
		}

		return new Level109(gameLevelData);
	}
	public Level110 getLevel110 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level110Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level110Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level110Hard(heroCellTypes);
				break;
		}

		return new Level110(gameLevelData);
	}
	public Level111 getLevel111 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level111Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level111Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level111Hard(heroCellTypes);
				break;
		}

		return new Level111(gameLevelData);
	}
	public Level112 getLevel112 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level112Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level112Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level112Hard(heroCellTypes);
				break;
		}

		return new Level112(gameLevelData);
	}
	public Level113 getLevel113 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level113Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level113Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level113Hard(heroCellTypes);
				break;
		}

		return new Level113(gameLevelData);
	}
	public Level114 getLevel114 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level114Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level114Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level114Hard(heroCellTypes);
				break;
		}

		return new Level114(gameLevelData);
	}
	public Level115 getLevel115 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level115Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level115Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level115Hard(heroCellTypes);
				break;
		}

		return new Level115(gameLevelData);
	}
	public Level116 getLevel116 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level116Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level116Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level116Hard(heroCellTypes);
				break;
		}

		return new Level116(gameLevelData);
	}
	public Level117 getLevel117 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level117Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level117Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level117Hard(heroCellTypes);
				break;
		}

		return new Level117(gameLevelData);
	}
	public Level118 getLevel118 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level118Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level118Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level118Hard(heroCellTypes);
				break;
		}

		return new Level118(gameLevelData);
	}
	public Level119 getLevel119 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level119Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level119Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level119Hard(heroCellTypes);
				break;
		}

		return new Level119(gameLevelData);
	}
	public Level120 getLevel120 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level120Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level120Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level120Hard(heroCellTypes);
				break;
		}

		return new Level120(gameLevelData);
	}
	public Level121 getLevel121 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level121Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level121Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level121Hard(heroCellTypes);
				break;
		}

		return new Level121(gameLevelData);
	}
	public Level122 getLevel122 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level122Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level122Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level122Hard(heroCellTypes);
				break;
		}

		return new Level122(gameLevelData);
	}
	public Level123 getLevel123 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level123Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level123Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level123Hard(heroCellTypes);
				break;
		}

		return new Level123(gameLevelData);
	}
	public Level124 getLevel124 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level124Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level124Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level124Hard(heroCellTypes);
				break;
		}

		return new Level124(gameLevelData);
	}
	public Level125 getLevel125 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level125Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level125Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level125Hard(heroCellTypes);
				break;
		}

		return new Level125(gameLevelData);
	}
	public Level126 getLevel126 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level126Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level126Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level126Hard(heroCellTypes);
				break;
		}

		return new Level126(gameLevelData);
	}
	public Level127 getLevel127 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level127Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level127Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level127Hard(heroCellTypes);
				break;
		}

		return new Level127(gameLevelData);
	}
	public Level128 getLevel128 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level128Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level128Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level128Hard(heroCellTypes);
				break;
		}

		return new Level128(gameLevelData);
	}
	public Level129 getLevel129 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level129Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level129Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level129Hard(heroCellTypes);
				break;
		}

		return new Level129(gameLevelData);
	}
	public Level130 getLevel130 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level130Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level130Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level130Hard(heroCellTypes);
				break;
		}

		return new Level130(gameLevelData);
	}
	public Level131 getLevel131 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level131Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level131Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level131Hard(heroCellTypes);
				break;
		}

		return new Level131(gameLevelData);
	}
	public Level132 getLevel132 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level132Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level132Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level132Hard(heroCellTypes);
				break;
		}

		return new Level132(gameLevelData);
	}
	public Level133 getLevel133 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level133Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level133Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level133Hard(heroCellTypes);
				break;
		}

		return new Level133(gameLevelData);
	}
	public Level134 getLevel134 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level134Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level134Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level134Hard(heroCellTypes);
				break;
		}

		return new Level134(gameLevelData);
	}
	public Level135 getLevel135 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level135Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level135Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level135Hard(heroCellTypes);
				break;
		}

		return new Level135(gameLevelData);
	}
	public Level136 getLevel136 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level136Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level136Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level136Hard(heroCellTypes);
				break;
		}

		return new Level136(gameLevelData);
	}
	public Level137 getLevel137 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level137Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level137Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level137Hard(heroCellTypes);
				break;
		}

		return new Level137(gameLevelData);
	}
	public Level138 getLevel138 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level138Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level138Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level138Hard(heroCellTypes);
				break;
		}

		return new Level138(gameLevelData);
	}
	public Level139 getLevel139 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level139Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level139Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level139Hard(heroCellTypes);
				break;
		}

		return new Level139(gameLevelData);
	}
	public Level140 getLevel140 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level140Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level140Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level140Hard(heroCellTypes);
				break;
		}

		return new Level140(gameLevelData);
	}
	public Level141 getLevel141 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level141Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level141Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level141Hard(heroCellTypes);
				break;
		}

		return new Level141(gameLevelData);
	}
	public Level142 getLevel142 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level142Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level142Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level142Hard(heroCellTypes);
				break;
		}

		return new Level142(gameLevelData);
	}
	public Level143 getLevel143 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level143Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level143Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level143Hard(heroCellTypes);
				break;
		}

		return new Level143(gameLevelData);
	}
	public Level144 getLevel144 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level144Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level144Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level144Hard(heroCellTypes);
				break;
		}

		return new Level144(gameLevelData);
	}
	public Level145 getLevel145 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level145Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level145Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level145Hard(heroCellTypes);
				break;
		}

		return new Level145(gameLevelData);
	}
	public Level146 getLevel146 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level146Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level146Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level146Hard(heroCellTypes);
				break;
		}

		return new Level146(gameLevelData);
	}
	public Level147 getLevel147 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level147Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level147Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level147Hard(heroCellTypes);
				break;
		}

		return new Level147(gameLevelData);
	}
	public Level148 getLevel148 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level148Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level148Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level148Hard(heroCellTypes);
				break;
		}

		return new Level148(gameLevelData);
	}
	public Level149 getLevel149 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level149Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level149Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level149Hard(heroCellTypes);
				break;
		}

		return new Level149(gameLevelData);
	}
	public Level150 getLevel150 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level150Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level150Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level150Hard(heroCellTypes);
				break;
		}

		return new Level150(gameLevelData);
	}
	public Level151 getLevel151 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level151Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level151Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level151Hard(heroCellTypes);
				break;
		}

		return new Level151(gameLevelData);
	}
	public Level152 getLevel152 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level152Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level152Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level152Hard(heroCellTypes);
				break;
		}

		return new Level152(gameLevelData);
	}
	public Level153 getLevel153 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level153Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level153Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level153Hard(heroCellTypes);
				break;
		}

		return new Level153(gameLevelData);
	}
	public Level154 getLevel154 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level154Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level154Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level154Hard(heroCellTypes);
				break;
		}

		return new Level154(gameLevelData);
	}
	public Level155 getLevel155 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level155Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level155Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level155Hard(heroCellTypes);
				break;
		}

		return new Level155(gameLevelData);
	}
	public Level156 getLevel156 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level156Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level156Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level156Hard(heroCellTypes);
				break;
		}

		return new Level156(gameLevelData);
	}
	public Level157 getLevel157 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level157Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level157Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level157Hard(heroCellTypes);
				break;
		}

		return new Level157(gameLevelData);
	}
	public Level158 getLevel158 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level158Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level158Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level158Hard(heroCellTypes);
				break;
		}

		return new Level158(gameLevelData);
	}
	public Level159 getLevel159 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level159Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level159Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level159Hard(heroCellTypes);
				break;
		}

		return new Level159(gameLevelData);
	}
	public Level160 getLevel160 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level160Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level160Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level160Hard(heroCellTypes);
				break;
		}

		return new Level160(gameLevelData);
	}
	public Level161 getLevel161 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level161Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level161Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level161Hard(heroCellTypes);
				break;
		}

		return new Level161(gameLevelData);
	}
	public Level162 getLevel162 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level162Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level162Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level162Hard(heroCellTypes);
				break;
		}

		return new Level162(gameLevelData);
	}
	public Level163 getLevel163 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level163Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level163Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level163Hard(heroCellTypes);
				break;
		}

		return new Level163(gameLevelData);
	}
	public Level164 getLevel164 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level164Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level164Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level164Hard(heroCellTypes);
				break;
		}

		return new Level164(gameLevelData);
	}
	public Level165 getLevel165 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level165Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level165Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level165Hard(heroCellTypes);
				break;
		}

		return new Level165(gameLevelData);
	}
	public Level166 getLevel166 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level166Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level166Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level166Hard(heroCellTypes);
				break;
		}

		return new Level166(gameLevelData);
	}
	public Level167 getLevel167 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level167Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level167Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level167Hard(heroCellTypes);
				break;
		}

		return new Level167(gameLevelData);
	}
	public Level168 getLevel168 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level168Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level168Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level168Hard(heroCellTypes);
				break;
		}

		return new Level168(gameLevelData);
	}
	public Level169 getLevel169 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level169Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level169Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level169Hard(heroCellTypes);
				break;
		}

		return new Level169(gameLevelData);
	}
	public Level170 getLevel170 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level170Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level170Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level170Hard(heroCellTypes);
				break;
		}

		return new Level170(gameLevelData);
	}
	public Level171 getLevel171 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level171Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level171Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level171Hard(heroCellTypes);
				break;
		}

		return new Level171(gameLevelData);
	}
	public Level172 getLevel172 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level172Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level172Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level172Hard(heroCellTypes);
				break;
		}

		return new Level172(gameLevelData);
	}
	public Level173 getLevel173 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level173Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level173Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level173Hard(heroCellTypes);
				break;
		}

		return new Level173(gameLevelData);
	}
	public Level174 getLevel174 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level174Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level174Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level174Hard(heroCellTypes);
				break;
		}

		return new Level174(gameLevelData);
	}
	public Level175 getLevel175 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level175Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level175Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level175Hard(heroCellTypes);
				break;
		}

		return new Level175(gameLevelData);
	}
	public Level176 getLevel176 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level176Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level176Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level176Hard(heroCellTypes);
				break;
		}

		return new Level176(gameLevelData);
	}
	public Level177 getLevel177 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level177Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level177Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level177Hard(heroCellTypes);
				break;
		}

		return new Level177(gameLevelData);
	}
	public Level178 getLevel178 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level178Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level178Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level178Hard(heroCellTypes);
				break;
		}

		return new Level178(gameLevelData);
	}
	public Level179 getLevel179 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level179Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level179Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level179Hard(heroCellTypes);
				break;
		}

		return new Level179(gameLevelData);
	}
	public Level180 getLevel180 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level180Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level180Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level180Hard(heroCellTypes);
				break;
		}

		return new Level180(gameLevelData);
	}
	public Level181 getLevel181 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level181Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level181Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level181Hard(heroCellTypes);
				break;
		}

		return new Level181(gameLevelData);
	}
	public Level182 getLevel182 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level182Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level182Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level182Hard(heroCellTypes);
				break;
		}

		return new Level182(gameLevelData);
	}
	public Level183 getLevel183 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level183Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level183Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level183Hard(heroCellTypes);
				break;
		}

		return new Level183(gameLevelData);
	}
	public Level184 getLevel184 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level184Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level184Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level184Hard(heroCellTypes);
				break;
		}

		return new Level184(gameLevelData);
	}
	public Level185 getLevel185 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level185Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level185Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level185Hard(heroCellTypes);
				break;
		}

		return new Level185(gameLevelData);
	}
	public Level186 getLevel186 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level186Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level186Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level186Hard(heroCellTypes);
				break;
		}

		return new Level186(gameLevelData);
	}
	public Level187 getLevel187 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level187Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level187Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level187Hard(heroCellTypes);
				break;
		}

		return new Level187(gameLevelData);
	}
	public Level188 getLevel188 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level188Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level188Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level188Hard(heroCellTypes);
				break;
		}

		return new Level188(gameLevelData);
	}
	public Level189 getLevel189 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level189Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level189Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level189Hard(heroCellTypes);
				break;
		}

		return new Level189(gameLevelData);
	}
	public Level190 getLevel190 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level190Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level190Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level190Hard(heroCellTypes);
				break;
		}

		return new Level190(gameLevelData);
	}
	public Level191 getLevel191 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level191Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level191Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level191Hard(heroCellTypes);
				break;
		}

		return new Level191(gameLevelData);
	}
	public Level192 getLevel192 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level192Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level192Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level192Hard(heroCellTypes);
				break;
		}

		return new Level192(gameLevelData);
	}
	public Level193 getLevel193 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level193Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level193Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level193Hard(heroCellTypes);
				break;
		}

		return new Level193(gameLevelData);
	}
	public Level194 getLevel194 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level194Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level194Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level194Hard(heroCellTypes);
				break;
		}

		return new Level194(gameLevelData);
	}
	public Level195 getLevel195 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level195Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level195Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level195Hard(heroCellTypes);
				break;
		}

		return new Level195(gameLevelData);
	}
	public Level196 getLevel196 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level196Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level196Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level196Hard(heroCellTypes);
				break;
		}

		return new Level196(gameLevelData);
	}
	public Level197 getLevel197 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level197Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level197Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level197Hard(heroCellTypes);
				break;
		}

		return new Level197(gameLevelData);
	}
	public Level198 getLevel198 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level198Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level198Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level198Hard(heroCellTypes);
				break;
		}

		return new Level198(gameLevelData);
	}
	public Level199 getLevel199 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level199Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level199Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level199Hard(heroCellTypes);
				break;
		}

		return new Level199(gameLevelData);
	}
	public Level200 getLevel200 (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {
		GameLevelData gameLevelData;

		switch (gameDifficulty) {
			case EASY:
				gameLevelData = new Level200Easy(heroCellTypes);
				break;
			case NORMAL:
			default:
				gameLevelData = new Level200Normal(heroCellTypes);
				break;
			case HARD:
				gameLevelData = new Level200Hard(heroCellTypes);
				break;
		}

		return new Level200(gameLevelData);
	}

}
