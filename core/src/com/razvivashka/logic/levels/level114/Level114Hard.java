package com.razvivashka.logic.levels.level114;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level114Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level114Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 11, 23, 22, 11, 32, 21, 0},
			{32, 31, 31, 40, 0, 33, 22, 40},
			{30, 10, 40, 21, 40, 0, 20, 23},
			{33, 40, 21, 10, 21, 0, 30, 10},
			{30, 11, 10, 31, 30, 33, 10, 21},
			{22, 0, 10, 20, 0, 31, 23, 23},
			{33, 10, 32, 33, 30, 31, 21, 60},
			{22, 22, 21, 23, 21, 30, 32, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.GOLD));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.GOLD));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,7, StaticCellType.GOLD));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
