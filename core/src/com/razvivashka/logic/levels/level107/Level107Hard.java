package com.razvivashka.logic.levels.level107;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level107Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level107Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 33, 33, 10, 10, 23, 0},
			{10, 21, 33, 33, 23, 31, 33, 20},
			{32, 40, 40, 22, 20, 20, 23, 10},
			{30, 20, 23, 21, 32, 31, 21, 33},
			{20, 30, 33, 33, 32, 20, 23, 33},
			{33, 11, 40, 20, 32, 33, 31, 10},
			{23, 23, 30, 40, 40, 23, 20, 60},
			{32, 20, 23, 11, 30, 20, 10, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,4, StaticCellType.GOLD));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,2, StaticCellType.BONUS));
		list.add(new StaticCell(7,1, StaticCellType.GOLD));
		list.add(new StaticCell(0,5, StaticCellType.BONUS));
		list.add(new StaticCell(7,0, StaticCellType.GOLD));
		list.add(new StaticCell(3,6, StaticCellType.GOLD));
		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,6, StaticCellType.BONUS));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,0, StaticCellType.BONUS));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
