package com.razvivashka.logic.levels.level79;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level79Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level79Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 11, 31, 20, 21, 10, 10},
			{33, 0, 31, 30, 40, 10, 21, 10},
			{11, 23, 33, 10, 40, 33, 21, 22},
			{32, 0, 33, 22, 10, 0, 33, 22},
			{10, 10, 10, 0, 21, 20, 23, 30},
			{11, 10, 33, 30, 30, 30, 30, 10},
			{21, 10, 23, 0, 33, 21, 30, 60},
			{20, 33, 21, 10, 11, 33, 11, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,4, StaticCellType.BONUS));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));
		list.add(new StaticCell(0,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
