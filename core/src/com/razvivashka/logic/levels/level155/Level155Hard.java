package com.razvivashka.logic.levels.level155;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level155Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level155Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 21, 11, 32, 40, 33, 21, 22},
			{33, 23, 22, 23, 10, 31, 0, 32},
			{10, 32, 20, 31, 11, 32, 0, 11},
			{31, 40, 20, 21, 20, 11, 40, 10},
			{30, 0, 20, 33, 32, 21, 23, 32},
			{10, 20, 32, 31, 0, 31, 32, 11},
			{0, 23, 23, 30, 20, 23, 31, 60},
			{23, 22, 21, 10, 32, 23, 30, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(0,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.BONUS));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.BONUS));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
