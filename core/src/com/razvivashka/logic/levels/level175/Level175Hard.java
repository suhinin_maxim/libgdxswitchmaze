package com.razvivashka.logic.levels.level175;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level175Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level175Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 11, 22, 20, 0, 21, 21},
			{30, 31, 23, 40, 23, 21, 10, 21},
			{40, 20, 10, 32, 30, 31, 32, 0},
			{11, 11, 10, 0, 11, 0, 40, 22},
			{11, 21, 10, 23, 23, 32, 11, 32},
			{21, 23, 21, 22, 23, 11, 33, 21},
			{31, 30, 30, 31, 30, 22, 20, 60},
			{40, 0, 0, 33, 30, 40, 32, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,7, StaticCellType.GOLD));
		list.add(new StaticCell(4,5, StaticCellType.GOLD));
		list.add(new StaticCell(5,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.BONUS));
		list.add(new StaticCell(0,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
