package com.razvivashka.logic.levels.level135;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level135Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level135Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 32, 33, 23, 33, 11, 20},
			{33, 21, 20, 10, 33, 33, 11, 23},
			{11, 40, 32, 30, 23, 33, 11, 40},
			{31, 30, 20, 0, 11, 32, 30, 23},
			{30, 20, 40, 30, 10, 0, 30, 21},
			{11, 11, 10, 23, 30, 21, 21, 32},
			{11, 0, 23, 23, 0, 22, 21, 60},
			{21, 11, 32, 32, 11, 30, 11, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,1, StaticCellType.GOLD));
		list.add(new StaticCell(2,3, StaticCellType.GOLD));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,2, StaticCellType.BONUS));
		list.add(new StaticCell(1,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,2, StaticCellType.BONUS));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
