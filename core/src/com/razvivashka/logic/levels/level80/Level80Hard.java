package com.razvivashka.logic.levels.level80;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level80Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level80Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 20, 21, 11, 31, 21, 10},
			{10, 32, 11, 32, 20, 33, 23, 0},
			{30, 31, 40, 10, 33, 32, 32, 40},
			{31, 0, 22, 21, 0, 40, 33, 33},
			{10, 10, 21, 40, 0, 23, 40, 40},
			{20, 23, 30, 0, 0, 30, 30, 33},
			{40, 32, 40, 23, 11, 31, 40, 60},
			{33, 20, 30, 10, 20, 31, 21, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,4, StaticCellType.GOLD));
		list.add(new StaticCell(5,7, StaticCellType.BONUS));
		list.add(new StaticCell(3,2, StaticCellType.GOLD));
		list.add(new StaticCell(3,0, StaticCellType.GOLD));
		list.add(new StaticCell(1,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
