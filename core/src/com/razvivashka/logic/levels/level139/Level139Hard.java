package com.razvivashka.logic.levels.level139;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level139Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level139Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 23, 10, 11, 23, 31, 32},
			{40, 20, 22, 11, 22, 22, 23, 11},
			{31, 22, 0, 22, 31, 22, 10, 21},
			{40, 20, 31, 31, 31, 30, 23, 40},
			{11, 32, 11, 23, 30, 20, 10, 31},
			{11, 30, 31, 0, 40, 10, 32, 0},
			{30, 23, 0, 31, 31, 31, 33, 60},
			{21, 33, 10, 40, 40, 10, 40, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.GOLD));
		list.add(new StaticCell(7,7, StaticCellType.GOLD));
		list.add(new StaticCell(6,3, StaticCellType.GOLD));
		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,2, StaticCellType.BONUS));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,5, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
