package com.razvivashka.logic.levels.level19;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level19Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level19Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 20, 32, 32, 30, 23, 20, 30},
			{33, 10, 0, 10, 10, 33, 0, 31},
			{40, 21, 20, 20, 23, 10, 10, 11},
			{33, 11, 0, 11, 20, 40, 33, 40},
			{10, 23, 22, 30, 40, 20, 21, 33},
			{11, 10, 22, 30, 33, 40, 32, 20},
			{30, 11, 31, 32, 31, 32, 22, 60},
			{10, 11, 31, 11, 32, 32, 33, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,1, StaticCellType.BONUS));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,7, StaticCellType.BONUS));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.BONUS));
		list.add(new StaticCell(1,1, StaticCellType.BONUS));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
