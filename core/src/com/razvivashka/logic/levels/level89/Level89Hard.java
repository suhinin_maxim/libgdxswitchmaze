package com.razvivashka.logic.levels.level89;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level89Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level89Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 21, 21, 20, 22, 23, 32},
			{31, 40, 20, 33, 32, 10, 30, 22},
			{33, 20, 21, 32, 11, 23, 33, 22},
			{10, 40, 22, 33, 0, 10, 0, 20},
			{32, 20, 31, 10, 22, 22, 10, 11},
			{11, 23, 40, 20, 0, 11, 22, 11},
			{21, 33, 40, 30, 11, 23, 30, 60},
			{23, 32, 20, 21, 11, 20, 30, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.GOLD));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,7, StaticCellType.GOLD));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,1, StaticCellType.BONUS));
		list.add(new StaticCell(6,3, StaticCellType.BONUS));
		list.add(new StaticCell(7,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.BONUS));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.BONUS));
		list.add(new StaticCell(4,7, StaticCellType.GOLD));
		list.add(new StaticCell(1,5, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
