package com.razvivashka.logic.levels.level108;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level108Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level108Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 40, 33, 11, 23, 11, 23},
			{11, 32, 10, 40, 30, 31, 31, 32},
			{40, 11, 32, 32, 11, 20, 30, 22},
			{30, 22, 31, 40, 32, 23, 33, 0},
			{11, 21, 23, 21, 33, 10, 10, 11},
			{30, 30, 32, 22, 11, 11, 21, 30},
			{22, 40, 0, 30, 23, 40, 30, 60},
			{33, 21, 30, 33, 30, 40, 10, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,5, StaticCellType.BONUS));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,0, StaticCellType.BONUS));
		list.add(new StaticCell(5,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.BONUS));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,0, StaticCellType.GOLD));
		list.add(new StaticCell(4,4, StaticCellType.GOLD));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,2, StaticCellType.BONUS));
		list.add(new StaticCell(0,3, StaticCellType.BONUS));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,1, StaticCellType.GOLD));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
