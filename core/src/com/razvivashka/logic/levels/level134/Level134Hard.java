package com.razvivashka.logic.levels.level134;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level134Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level134Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 11, 20, 11, 20, 31, 11, 0},
			{33, 33, 33, 22, 0, 40, 32, 22},
			{23, 40, 40, 0, 33, 11, 23, 10},
			{20, 11, 40, 31, 33, 21, 32, 23},
			{0, 10, 20, 23, 33, 33, 32, 20},
			{30, 11, 31, 33, 23, 21, 30, 30},
			{0, 30, 20, 32, 21, 20, 22, 60},
			{23, 33, 40, 21, 10, 11, 11, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.GOLD));
		list.add(new StaticCell(1,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.BONUS));
		list.add(new StaticCell(3,0, StaticCellType.BONUS));
		list.add(new StaticCell(0,1, StaticCellType.GOLD));
		list.add(new StaticCell(1,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,5, StaticCellType.BONUS));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
