package com.razvivashka.logic.levels.level45;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level45Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level45Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 30, 20, 31, 11, 33, 0},
			{33, 40, 30, 31, 23, 30, 23, 21},
			{40, 31, 20, 33, 11, 23, 10, 40},
			{11, 10, 31, 23, 33, 21, 31, 20},
			{31, 23, 0, 22, 22, 40, 11, 40},
			{30, 22, 40, 32, 0, 33, 20, 40},
			{32, 32, 30, 11, 0, 31, 10, 60},
			{21, 33, 31, 40, 30, 31, 32, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,2, StaticCellType.GOLD));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,4, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
