package com.razvivashka.logic.levels.level179;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level179Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level179Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 21, 32, 33, 33, 10, 20, 32},
			{33, 40, 21, 30, 31, 40, 10, 20},
			{40, 32, 40, 11, 11, 32, 21, 22},
			{33, 30, 21, 0, 30, 30, 32, 30},
			{20, 11, 33, 11, 30, 31, 33, 30},
			{22, 30, 11, 31, 40, 0, 22, 40},
			{11, 33, 32, 30, 31, 31, 11, 60},
			{22, 21, 0, 21, 32, 40, 40, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,5, StaticCellType.BONUS));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,6, StaticCellType.BONUS));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.GOLD));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,4, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 24;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
