package com.razvivashka.logic.levels;


import com.razvivashka.logic.domaindata.GameBoardCell;
import com.razvivashka.logic.domaindata.GameBoardCellType;
import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class GameLevelBase implements GameLevel {

    //region Fields

    private GameBoardCell[][] mBoard;
    private List<HeroCell> mHeroesCells;
    private List<StaticCell> mStaticCells;

    private GameLevelData mGameLevelData;
    private int mSteps;
    private int mCruising;
    private Map<String, Integer> mCollectedBonuses;

    //endregion


    //region constructor

    public GameLevelBase(GameLevelData gameLevelData) {
        mSteps = 0;
        mCollectedBonuses = new HashMap<>();
        mGameLevelData = gameLevelData;

        mHeroesCells = gameLevelData.getHeroCells();
        mStaticCells = gameLevelData.getStaticCells();
        mBoard = new GameBoardCell[mGameLevelData.getFieldSize()][mGameLevelData.getFieldSize()];
        mCruising = gameLevelData.getStartCruising();

        reloadField();
    }

    //endregion


    //region implements GameLevel

    @Override
    public GameBoardCell[][] getField() {
        return mBoard;
    }

    @Override
    public int getFieldSize() {
        return mGameLevelData.getFieldSize();
    }

    @Override
    public synchronized GameBoardCell move(int i, int j) {
        for (HeroCell hero : mHeroesCells) {
            if (hero.getPosI() == i && hero.getPosJ() == j) {
                hero.rotateWithBoard();
            }
        }

        GameBoardCell cell = mBoard[i][j];
        cell.rotate();

        return cell;
    }

    @Override
    public List<HeroCell> getHeroes() {
        return mHeroesCells;
    }

    @Override
    public List<StaticCell> getStaticCells(){
        return mStaticCells;
    }

    @Override
    public synchronized boolean moveHeroes() {
        boolean moved = false;
        for (HeroCell hero : mHeroesCells) {
            if (isHeroMove(hero)) {
                moved = true;
                continue;
            }

            hero.moveBack();
            hero.rotateCCW();
            if (isHeroMove(hero)) {
                moved = true;
                continue;
            }

            hero.moveBack();
            hero.rotateCCW();
            hero.rotateCCW();
            if (isHeroMove(hero)) {
                moved = true;
                continue;
            }

            hero.moveBack();
            hero.rotateCW();
            if (isHeroMove(hero)) {
                moved = true;
                continue;
            }
        }

        return moved;
    }

    @Override
    public void reloadField() {
        for (int i=0; i<mGameLevelData.getFieldSize(); i++) {
            for (int j=0; j<mGameLevelData.getFieldSize(); j++) {
                mBoard[i][j] = new GameBoardCell(mGameLevelData.getBoardInt()[i][j]);
            }
        }
    }

    @Override
    public int calcScores() {
        int scores = 0;

        for (Map.Entry<String, Integer> entry: mCollectedBonuses.entrySet()) {
            if (entry.getKey().equals(StaticCellType.BONUS.name())) {
                scores += entry.getValue() * 100;
            } else if (entry.getKey().equals(StaticCellType.GOLD.name())) {
                scores += entry.getValue() * 10;
            }
        }

        return scores;
    }

    @Override
    public int calcStars() {
        // TODO use mCollectedBonuses, mSteps, mCruising to calculate stars by level

        return 0;
    }

    @Override
    public boolean isFinish() {
        for (HeroCell hero : mHeroesCells) {
            if (mBoard[hero.getPosI()][hero.getPosJ()].getType() == GameBoardCellType.FINISH) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isLose () {
        return mSteps >= mCruising;
    }

    @Override
    public void checkBonus() {
        for (HeroCell hero : mHeroesCells) {
            StaticCell staticCell = getStaticCell(hero.getPosI(), hero.getPosJ());
            if (staticCell != null) {
                switch (staticCell.getType()) {
                    case ADD_TIME_5:
                        addCruising(5);

                        mStaticCells.remove(staticCell);
                        staticCell.startAction();
                        break;
                    case ADD_TIME_10:
                        addCruising(10);

                        mStaticCells.remove(staticCell);
                        staticCell.startAction();
                        break;
                    case BONUS:
                    case GOLD:
                        String key = staticCell.getType().name();
                        if (mCollectedBonuses.containsKey(key) == true) {
                            mCollectedBonuses.put(key, mCollectedBonuses.get(key) + 1);
                        } else {
                            mCollectedBonuses.put(key, 1);
                        }

                        mStaticCells.remove(staticCell);
                        staticCell.startAction();
                        break;
                }
            }
        }
    }

    @Override
    public void incSteps () {
        mSteps++;
    }

    @Override
    public int getSteps () {
        return mSteps;
    }

    @Override
    public Map<String, Integer> getCollectedBonuses() {
        return mCollectedBonuses;
    }

    @Override
    public int getCruising() {
        return mCruising;
    }

    @Override
    public int getMaxCruising () {
        return mGameLevelData.getMaxCruising();
    }

    @Override
    public int getMaxScores() {
        return mGameLevelData.getMaxScores();
    }

    //endregion


    //region service method

    private void addCruising(int cruising) {
        mCruising += cruising;
        if (mCruising - mSteps > mGameLevelData.getMaxCruising()) {
            mCruising = mSteps + mGameLevelData.getMaxCruising();
        }
    }

    private StaticCell getStaticCell (int posI, int posJ) {
        StaticCell staticCell = null;

        for (StaticCell cell : mStaticCells) {
            if (cell.getPosI() == posI && cell.getPosJ() == posJ) {
                staticCell = cell;
                break;
            }
        }

        return staticCell;
    }

    private boolean isHeroMove(HeroCell hero) {
        hero.tryToMoveNext();
        GameBoardCell cell = mBoard[hero.getPosI()][hero.getPosJ()];
        if (cell.isWall(hero.getCellI(), hero.getCellJ()) == false) {
            hero.acceptMove();
            return true;
        }

        hero.moveBack();

        return false;
    }

    //endregion
}
