package com.razvivashka.logic.levels.level65;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level65Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level65Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 31, 31, 31, 22, 22, 11},
			{11, 23, 40, 23, 40, 40, 30, 11},
			{10, 31, 33, 22, 32, 31, 32, 33},
			{30, 23, 11, 31, 32, 21, 21, 33},
			{40, 21, 40, 31, 11, 31, 23, 21},
			{32, 0, 22, 21, 32, 22, 40, 32},
			{23, 31, 10, 21, 30, 40, 33, 60},
			{10, 40, 33, 23, 32, 22, 33, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,2, StaticCellType.GOLD));
		list.add(new StaticCell(6,0, StaticCellType.GOLD));
		list.add(new StaticCell(0,1, StaticCellType.BONUS));
		list.add(new StaticCell(3,5, StaticCellType.GOLD));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.GOLD));
		list.add(new StaticCell(7,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.BONUS));
		list.add(new StaticCell(1,7, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
