package com.razvivashka.logic.levels.level43;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level43Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level43Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 20, 23, 31, 23, 31, 11, 23},
			{21, 40, 22, 10, 31, 23, 10, 32},
			{30, 32, 10, 40, 22, 20, 0, 23},
			{40, 21, 30, 22, 33, 11, 0, 31},
			{11, 11, 20, 21, 10, 32, 22, 22},
			{23, 23, 21, 20, 32, 22, 40, 30},
			{20, 32, 21, 31, 20, 30, 31, 60},
			{0, 40, 30, 10, 10, 32, 30, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,5, StaticCellType.GOLD));
		list.add(new StaticCell(4,6, StaticCellType.BONUS));
		list.add(new StaticCell(0,2, StaticCellType.GOLD));
		list.add(new StaticCell(0,1, StaticCellType.BONUS));
		list.add(new StaticCell(2,7, StaticCellType.GOLD));
		list.add(new StaticCell(7,2, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
