package com.razvivashka.logic.levels.level167;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level167Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level167Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 20, 32, 31, 22, 30, 40, 11},
			{33, 10, 20, 20, 22, 20, 22, 20},
			{31, 33, 11, 31, 20, 23, 32, 11},
			{32, 32, 23, 23, 21, 33, 40, 23},
			{21, 23, 22, 23, 30, 23, 30, 30},
			{33, 20, 30, 32, 40, 22, 23, 31},
			{23, 32, 31, 30, 21, 40, 32, 60},
			{31, 40, 0, 33, 0, 21, 30, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.GOLD));
		list.add(new StaticCell(5,5, StaticCellType.BONUS));
		list.add(new StaticCell(3,6, StaticCellType.GOLD));
		list.add(new StaticCell(6,1, StaticCellType.GOLD));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,7, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
