package com.razvivashka.logic.levels.level180;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level180Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level180Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 11, 40, 20, 11, 10},
			{32, 30, 31, 30, 33, 33},
			{40, 40, 23, 20, 33, 40},
			{20, 23, 30, 23, 0, 23},
			{31, 20, 31, 32, 23, 60},
			{33, 11, 11, 32, 40, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,4, StaticCellType.BONUS));
		list.add(new StaticCell(0,2, StaticCellType.BONUS));
		list.add(new StaticCell(2,3, StaticCellType.BONUS));
		list.add(new StaticCell(3,2, StaticCellType.BONUS));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 14;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
