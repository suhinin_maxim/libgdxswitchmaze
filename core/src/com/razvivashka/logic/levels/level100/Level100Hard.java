package com.razvivashka.logic.levels.level100;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level100Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level100Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 33, 11, 31, 10, 11, 33, 11},
			{40, 30, 20, 20, 20, 22, 20, 33},
			{31, 20, 40, 20, 31, 11, 20, 0},
			{31, 30, 11, 32, 22, 0, 0, 30},
			{10, 30, 40, 32, 23, 0, 20, 20},
			{10, 31, 40, 32, 30, 23, 32, 32},
			{30, 11, 10, 22, 33, 22, 23, 60},
			{30, 31, 32, 21, 40, 40, 10, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.GOLD));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.GOLD));
		list.add(new StaticCell(0,1, StaticCellType.GOLD));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,7, StaticCellType.BONUS));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.BONUS));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
