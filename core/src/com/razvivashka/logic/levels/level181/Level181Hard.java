package com.razvivashka.logic.levels.level181;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level181Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level181Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 11, 22, 23, 23, 31, 21, 10},
			{33, 22, 31, 32, 23, 11, 40, 32},
			{31, 11, 32, 33, 22, 23, 21, 32},
			{23, 11, 11, 20, 32, 22, 32, 20},
			{0, 23, 30, 22, 23, 21, 32, 33},
			{32, 40, 32, 31, 10, 22, 30, 20},
			{20, 22, 0, 32, 22, 32, 30, 60},
			{22, 23, 31, 21, 33, 11, 11, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,5, StaticCellType.BONUS));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,3, StaticCellType.BONUS));
		list.add(new StaticCell(1,2, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
