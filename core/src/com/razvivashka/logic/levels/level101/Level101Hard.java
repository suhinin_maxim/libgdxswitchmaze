package com.razvivashka.logic.levels.level101;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level101Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level101Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 11, 32, 20, 11, 32, 40},
			{10, 32, 0, 11, 32, 21, 23, 10},
			{33, 0, 32, 30, 0, 40, 0, 40},
			{33, 11, 33, 30, 32, 33, 10, 21},
			{23, 33, 20, 0, 11, 32, 40, 11},
			{30, 21, 10, 30, 33, 30, 31, 31},
			{33, 22, 22, 21, 20, 22, 0, 60},
			{21, 20, 10, 30, 31, 11, 30, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,2, StaticCellType.GOLD));
		list.add(new StaticCell(1,6, StaticCellType.GOLD));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,4, StaticCellType.BONUS));
		list.add(new StaticCell(4,0, StaticCellType.BONUS));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,5, StaticCellType.GOLD));
		list.add(new StaticCell(6,3, StaticCellType.GOLD));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.BONUS));
		list.add(new StaticCell(0,6, StaticCellType.GOLD));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.GOLD));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
