package com.razvivashka.logic.levels.level190;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level190Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level190Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 31, 20, 22, 0, 30, 11},
			{30, 0, 40, 31, 31, 20, 0, 11},
			{11, 40, 33, 11, 31, 40, 21, 20},
			{32, 0, 32, 23, 23, 23, 22, 0},
			{40, 0, 21, 11, 31, 40, 0, 22},
			{21, 30, 30, 32, 22, 20, 40, 32},
			{10, 20, 0, 40, 40, 40, 20, 60},
			{10, 21, 40, 31, 21, 40, 31, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,3, StaticCellType.BONUS));
		list.add(new StaticCell(0,4, StaticCellType.GOLD));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
