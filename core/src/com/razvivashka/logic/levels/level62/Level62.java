package com.razvivashka.logic.levels.level62;

import com.razvivashka.logic.levels.GameLevelBase;
import com.razvivashka.logic.levels.GameLevelData;

/**
* Created by maxim on 06.12.2017
*/

public class Level62 extends GameLevelBase {

	public Level62 (GameLevelData gameLevelData) {
		super(gameLevelData);
	}

	@Override
	public int getLevel() {
		return 62;
}

}
