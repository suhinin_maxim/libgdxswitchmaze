package com.razvivashka.logic.levels.level161;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level161Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level161Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 0, 33, 11, 31, 31, 23},
			{33, 31, 23, 40, 21, 21, 33, 0},
			{31, 10, 20, 10, 40, 23, 31, 10},
			{23, 40, 11, 40, 22, 40, 40, 30},
			{21, 33, 10, 23, 10, 31, 32, 11},
			{10, 21, 33, 20, 10, 32, 22, 40},
			{23, 40, 33, 0, 23, 10, 31, 60},
			{32, 33, 40, 10, 0, 33, 40, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,0, StaticCellType.BONUS));
		list.add(new StaticCell(1,3, StaticCellType.BONUS));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,6, StaticCellType.BONUS));
		list.add(new StaticCell(4,1, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
