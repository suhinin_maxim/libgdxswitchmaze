package com.razvivashka.logic.levels.level186;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level186Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level186Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 33, 10, 30, 11, 11, 32},
			{20, 40, 30, 22, 10, 21, 31, 31},
			{20, 23, 11, 40, 31, 33, 23, 21},
			{22, 33, 23, 40, 32, 22, 10, 23},
			{23, 21, 40, 33, 30, 11, 23, 33},
			{23, 0, 10, 20, 40, 32, 10, 33},
			{33, 10, 11, 10, 11, 22, 10, 60},
			{23, 23, 0, 10, 30, 21, 30, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,1, StaticCellType.GOLD));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,2, StaticCellType.GOLD));
		list.add(new StaticCell(4,7, StaticCellType.GOLD));
		list.add(new StaticCell(4,2, StaticCellType.GOLD));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));
		list.add(new StaticCell(7,3, StaticCellType.BONUS));
		list.add(new StaticCell(7,1, StaticCellType.GOLD));
		list.add(new StaticCell(0,1, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
