package com.razvivashka.logic.levels.level106;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level106Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level106Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 21, 21, 30, 21, 40, 11, 0},
			{31, 21, 23, 10, 11, 30, 40, 40},
			{33, 30, 11, 31, 40, 23, 33, 10},
			{21, 30, 33, 11, 21, 31, 32, 32},
			{21, 21, 40, 33, 40, 31, 0, 10},
			{10, 23, 21, 31, 0, 40, 30, 30},
			{33, 20, 33, 33, 31, 10, 10, 60},
			{30, 20, 33, 20, 33, 21, 11, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,6, StaticCellType.BONUS));
		list.add(new StaticCell(4,4, StaticCellType.GOLD));
		list.add(new StaticCell(6,3, StaticCellType.BONUS));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));
		list.add(new StaticCell(0,1, StaticCellType.BONUS));
		list.add(new StaticCell(2,6, StaticCellType.BONUS));
		list.add(new StaticCell(7,6, StaticCellType.BONUS));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.BONUS));
		list.add(new StaticCell(7,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,4, StaticCellType.BONUS));
		list.add(new StaticCell(6,5, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
