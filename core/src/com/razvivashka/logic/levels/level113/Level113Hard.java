package com.razvivashka.logic.levels.level113;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level113Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level113Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 20, 32, 11, 21, 20, 30},
			{33, 40, 10, 21, 11, 21, 31, 20},
			{20, 31, 30, 21, 31, 40, 21, 30},
			{22, 30, 10, 33, 20, 10, 40, 10},
			{32, 11, 0, 40, 20, 32, 31, 32},
			{11, 20, 0, 33, 11, 30, 21, 23},
			{21, 31, 11, 40, 33, 22, 40, 60},
			{23, 22, 32, 33, 32, 10, 30, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,5, StaticCellType.BONUS));
		list.add(new StaticCell(5,1, StaticCellType.BONUS));
		list.add(new StaticCell(7,3, StaticCellType.GOLD));
		list.add(new StaticCell(5,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,0, StaticCellType.BONUS));
		list.add(new StaticCell(6,2, StaticCellType.GOLD));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 20;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
