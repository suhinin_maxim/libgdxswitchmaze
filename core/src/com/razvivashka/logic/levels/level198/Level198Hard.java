package com.razvivashka.logic.levels.level198;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level198Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level198Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 32, 31, 23, 31, 32, 32},
			{31, 23, 30, 23, 40, 33, 0, 32},
			{33, 30, 10, 30, 31, 23, 21, 30},
			{23, 20, 40, 40, 30, 33, 11, 30},
			{23, 10, 21, 10, 11, 40, 10, 21},
			{20, 23, 10, 0, 30, 30, 32, 33},
			{22, 10, 0, 10, 23, 0, 33, 60},
			{31, 21, 31, 31, 31, 32, 40, 40}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.GOLD));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
