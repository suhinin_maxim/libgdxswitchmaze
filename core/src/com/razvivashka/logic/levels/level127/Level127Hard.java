package com.razvivashka.logic.levels.level127;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level127Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level127Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 23, 10, 32, 40, 21, 23},
			{30, 10, 0, 30, 33, 40, 11, 21},
			{11, 0, 23, 31, 11, 33, 0, 0},
			{11, 31, 33, 21, 21, 30, 32, 40},
			{21, 23, 33, 32, 22, 10, 31, 10},
			{40, 33, 30, 40, 22, 33, 11, 30},
			{23, 20, 11, 31, 10, 30, 0, 60},
			{33, 10, 32, 31, 21, 40, 31, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
