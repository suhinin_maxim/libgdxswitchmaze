package com.razvivashka.logic.levels.level172;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level172Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level172Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 21, 21, 23, 20, 30, 40},
			{30, 33, 30, 22, 32, 32, 0, 0},
			{30, 32, 22, 31, 40, 40, 23, 40},
			{11, 11, 21, 30, 22, 22, 0, 0},
			{0, 21, 21, 23, 21, 32, 40, 33},
			{33, 11, 31, 22, 40, 20, 0, 21},
			{33, 11, 30, 23, 10, 0, 0, 60},
			{10, 10, 23, 20, 31, 32, 32, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,5, StaticCellType.GOLD));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.BONUS));
		list.add(new StaticCell(7,4, StaticCellType.BONUS));
		list.add(new StaticCell(4,6, StaticCellType.GOLD));
		list.add(new StaticCell(3,2, StaticCellType.GOLD));
		list.add(new StaticCell(1,2, StaticCellType.GOLD));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,1, StaticCellType.GOLD));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.GOLD));
		list.add(new StaticCell(4,4, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
