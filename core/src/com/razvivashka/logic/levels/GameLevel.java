package com.razvivashka.logic.levels;


import com.razvivashka.logic.domaindata.GameBoardCell;
import com.razvivashka.logic.domaindata.GameDifficulty;
import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellBase;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;

import java.util.List;
import java.util.Map;

public interface GameLevel {

    GameBoardCell[][] getField();               // get mixed field
    int getFieldSize();
    GameBoardCell move(int i, int j);           // may be diff moves for diff field calls
    void reloadField();                         // loads init field and rebuilt

    boolean moveHeroes();
    List<HeroCell> getHeroes();

    List<StaticCell> getStaticCells();

    int getLevel();

    Map<String, Integer> getCollectedBonuses();
    int calcScores();
    int getMaxScores();

    int calcStars();                            // use amount of moves and ....

    boolean isFinish();
    boolean isLose();
    void checkBonus();

    void incSteps();
    int getSteps();

    int getCruising();
    int getMaxCruising();

}
