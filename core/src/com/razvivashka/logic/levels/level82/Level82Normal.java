package com.razvivashka.logic.levels.level82;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level82Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level82Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 30, 0, 30, 30},
			{40, 33, 0, 40, 30, 10},
			{30, 11, 11, 23, 21, 10},
			{0, 31, 22, 10, 11, 40},
			{30, 31, 23, 33, 40, 60},
			{33, 23, 22, 21, 31, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,2, StaticCellType.BONUS));
		list.add(new StaticCell(5,3, StaticCellType.BONUS));
		list.add(new StaticCell(3,5, StaticCellType.GOLD));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.BONUS));
		list.add(new StaticCell(5,0, StaticCellType.GOLD));
		list.add(new StaticCell(0,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(2,5, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 12;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
