package com.razvivashka.logic.levels.level189;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level189Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level189Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 30, 22, 21, 23, 23, 31},
			{11, 20, 32, 11, 10, 31, 30, 32},
			{20, 40, 32, 23, 40, 20, 32, 31},
			{11, 40, 20, 20, 22, 23, 11, 32},
			{40, 33, 11, 40, 21, 32, 23, 30},
			{30, 23, 33, 40, 21, 22, 32, 11},
			{23, 11, 40, 40, 30, 11, 23, 60},
			{22, 40, 22, 40, 21, 40, 31, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,6, StaticCellType.GOLD));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,3, StaticCellType.GOLD));
		list.add(new StaticCell(7,0, StaticCellType.BONUS));
		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.GOLD));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 20;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
