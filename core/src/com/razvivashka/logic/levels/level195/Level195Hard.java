package com.razvivashka.logic.levels.level195;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level195Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level195Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 21, 30, 20, 30, 33, 32},
			{21, 32, 22, 23, 33, 22, 30, 30},
			{40, 31, 32, 40, 10, 0, 21, 11},
			{20, 23, 31, 31, 31, 10, 40, 22},
			{30, 0, 40, 10, 30, 0, 22, 40},
			{32, 23, 33, 30, 20, 0, 11, 22},
			{31, 30, 21, 11, 22, 31, 10, 60},
			{22, 0, 0, 11, 20, 32, 10, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.GOLD));
		list.add(new StaticCell(3,7, StaticCellType.GOLD));
		list.add(new StaticCell(1,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,7, StaticCellType.BONUS));
		list.add(new StaticCell(3,0, StaticCellType.GOLD));
		list.add(new StaticCell(7,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.BONUS));
		list.add(new StaticCell(5,6, StaticCellType.GOLD));
		list.add(new StaticCell(5,1, StaticCellType.BONUS));
		list.add(new StaticCell(2,0, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
