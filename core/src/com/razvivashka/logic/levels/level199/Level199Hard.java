package com.razvivashka.logic.levels.level199;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level199Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level199Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 30, 40, 22, 0, 22, 30},
			{20, 22, 33, 22, 33, 23, 21, 11},
			{22, 20, 32, 40, 20, 23, 10, 32},
			{10, 11, 32, 31, 40, 22, 0, 31},
			{32, 0, 21, 23, 40, 32, 23, 0},
			{11, 11, 23, 40, 11, 22, 0, 11},
			{11, 21, 21, 10, 11, 11, 11, 60},
			{33, 31, 22, 30, 40, 30, 40, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,4, StaticCellType.GOLD));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,6, StaticCellType.GOLD));
		list.add(new StaticCell(7,1, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
