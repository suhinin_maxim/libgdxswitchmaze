package com.razvivashka.logic.levels.level118;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level118Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level118Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 11, 11, 31, 31, 30, 23, 0},
			{33, 0, 20, 20, 21, 40, 23, 40},
			{40, 21, 30, 23, 32, 21, 22, 10},
			{30, 32, 31, 33, 23, 23, 20, 21},
			{10, 20, 32, 21, 40, 30, 20, 21},
			{33, 23, 40, 11, 33, 22, 0, 33},
			{21, 23, 23, 33, 33, 21, 22, 60},
			{21, 30, 31, 11, 40, 10, 11, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,4, StaticCellType.GOLD));
		list.add(new StaticCell(4,1, StaticCellType.GOLD));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,0, StaticCellType.BONUS));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,0, StaticCellType.GOLD));
		list.add(new StaticCell(6,5, StaticCellType.BONUS));
		list.add(new StaticCell(1,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
