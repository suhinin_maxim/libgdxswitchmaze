package com.razvivashka.logic.levels.level164;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level164Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level164Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 31, 32, 10, 33, 23, 11},
			{33, 0, 20, 31, 0, 40, 21, 31},
			{33, 10, 10, 10, 32, 11, 32, 10},
			{11, 20, 21, 21, 31, 40, 20, 23},
			{21, 33, 11, 40, 20, 22, 10, 30},
			{30, 40, 23, 10, 21, 33, 23, 31},
			{22, 40, 30, 22, 33, 22, 32, 60},
			{0, 23, 10, 22, 23, 10, 30, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,6, StaticCellType.GOLD));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,1, StaticCellType.GOLD));
		list.add(new StaticCell(1,0, StaticCellType.GOLD));
		list.add(new StaticCell(2,6, StaticCellType.GOLD));
		list.add(new StaticCell(7,5, StaticCellType.GOLD));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,7, StaticCellType.GOLD));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
