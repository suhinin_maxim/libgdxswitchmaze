package com.razvivashka.logic.levels.level95;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level95Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level95Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 20, 10, 22, 11, 21, 11, 20},
			{10, 32, 33, 20, 0, 23, 11, 31},
			{40, 20, 23, 30, 22, 32, 10, 32},
			{40, 32, 33, 10, 11, 32, 0, 20},
			{21, 10, 22, 11, 30, 32, 21, 21},
			{40, 0, 11, 31, 33, 11, 23, 22},
			{11, 22, 21, 30, 33, 31, 31, 60},
			{33, 31, 31, 20, 20, 40, 33, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,2, StaticCellType.GOLD));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.BONUS));
		list.add(new StaticCell(3,4, StaticCellType.BONUS));
		list.add(new StaticCell(5,2, StaticCellType.BONUS));
		list.add(new StaticCell(3,5, StaticCellType.BONUS));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.GOLD));
		list.add(new StaticCell(2,4, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
