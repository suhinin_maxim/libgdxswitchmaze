package com.razvivashka.logic.levels.level150;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level150Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level150Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 22, 23, 0, 11, 30, 30, 40},
			{31, 32, 11, 10, 20, 23, 10, 30},
			{23, 11, 40, 22, 11, 30, 23, 20},
			{32, 23, 33, 10, 32, 30, 33, 33},
			{10, 33, 0, 30, 33, 33, 32, 10},
			{40, 10, 33, 21, 10, 40, 33, 23},
			{23, 0, 31, 0, 31, 32, 40, 60},
			{23, 40, 40, 0, 33, 0, 40, 40}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,0, StaticCellType.GOLD));
		list.add(new StaticCell(3,3, StaticCellType.BONUS));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(5,3, StaticCellType.GOLD));
		list.add(new StaticCell(3,7, StaticCellType.BONUS));
		list.add(new StaticCell(2,0, StaticCellType.GOLD));
		list.add(new StaticCell(7,6, StaticCellType.GOLD));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
