package com.razvivashka.logic.levels.level2;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level2Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level2Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 20, 11, 30, 33, 22, 23},
			{31, 11, 31, 22, 40, 30, 10, 31},
			{11, 33, 20, 40, 21, 40, 32, 32},
			{21, 33, 23, 0, 30, 33, 33, 20},
			{30, 10, 31, 0, 11, 0, 30, 10},
			{21, 30, 33, 11, 11, 32, 11, 33},
			{10, 23, 21, 21, 11, 30, 33, 60},
			{22, 20, 31, 21, 20, 11, 21, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.BONUS));
		list.add(new StaticCell(4,7, StaticCellType.GOLD));
		list.add(new StaticCell(1,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,7, StaticCellType.GOLD));
		list.add(new StaticCell(0,5, StaticCellType.BONUS));
		list.add(new StaticCell(4,2, StaticCellType.BONUS));
		list.add(new StaticCell(2,6, StaticCellType.BONUS));
		list.add(new StaticCell(1,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,0, StaticCellType.BONUS));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 100;
	}

	@Override
	public int getMaxCruising () {
		return 100;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
