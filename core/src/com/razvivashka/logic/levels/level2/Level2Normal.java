package com.razvivashka.logic.levels.level2;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level2Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level2Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 31, 32, 23, 20},
			{21, 32, 11, 22, 0, 22},
			{0, 31, 21, 40, 0, 0},
			{31, 10, 11, 11, 21, 20},
			{10, 10, 10, 11, 22, 60},
			{22, 11, 31, 21, 33, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(2,1, StaticCellType.GOLD));
		list.add(new StaticCell(3,0, StaticCellType.GOLD));
		list.add(new StaticCell(1,0, StaticCellType.BONUS));
		list.add(new StaticCell(5,3, StaticCellType.BONUS));
		list.add(new StaticCell(3,1, StaticCellType.GOLD));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 100;
	}

	@Override
	public int getMaxCruising () {
		return 100;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
