package com.razvivashka.logic.levels.level2;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level2Easy implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level2Easy (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 4;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 10, 11, 30},
			{40, 22, 10, 22},
			{31, 21, 20, 60},
			{33, 32, 40, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,1, StaticCellType.BONUS));
		list.add(new StaticCell(1,1, StaticCellType.BONUS));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 100;
	}

	@Override
	public int getMaxCruising () {
		return 100;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
