package com.razvivashka.logic.levels.level1;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level1Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level1Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 20, 20, 20, 0, 23, 10},
			{33, 30, 33, 30, 20, 20, 33, 22},
			{40, 20, 40, 20, 10, 40, 40, 32},
			{40, 33, 32, 31, 31, 31, 11, 11},
			{10, 22, 11, 31, 40, 33, 11, 40},
			{23, 31, 32, 32, 21, 33, 20, 11},
			{33, 0, 21, 40, 31, 30, 32, 60},
			{23, 22, 11, 30, 21, 32, 21, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,0, StaticCellType.BONUS));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,6, StaticCellType.BONUS));
		list.add(new StaticCell(1,0, StaticCellType.GOLD));
		list.add(new StaticCell(0,2, StaticCellType.BONUS));
		list.add(new StaticCell(4,5, StaticCellType.BONUS));
		list.add(new StaticCell(5,5, StaticCellType.BONUS));
		list.add(new StaticCell(4,4, StaticCellType.GOLD));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,6, StaticCellType.BONUS));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,2, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 100;
	}

	@Override
	public int getMaxCruising () {
		return 100;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
