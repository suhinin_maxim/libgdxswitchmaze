package com.razvivashka.logic.levels.level125;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level125Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level125Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 23, 11, 22, 20, 0, 11},
			{21, 23, 23, 22, 40, 31, 0, 10},
			{11, 40, 10, 40, 11, 32, 10, 0},
			{10, 31, 40, 11, 11, 22, 10, 21},
			{10, 40, 33, 40, 40, 33, 31, 22},
			{31, 11, 32, 40, 23, 32, 21, 30},
			{30, 31, 10, 22, 40, 22, 22, 60},
			{31, 20, 33, 40, 30, 11, 31, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,4, StaticCellType.BONUS));
		list.add(new StaticCell(1,5, StaticCellType.GOLD));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.BONUS));
		list.add(new StaticCell(5,5, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.BONUS));
		list.add(new StaticCell(2,4, StaticCellType.BONUS));
		list.add(new StaticCell(6,2, StaticCellType.GOLD));
		list.add(new StaticCell(2,3, StaticCellType.BONUS));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,2, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
