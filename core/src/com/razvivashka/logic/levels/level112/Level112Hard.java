package com.razvivashka.logic.levels.level112;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level112Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level112Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 23, 32, 33, 22, 31, 33},
			{32, 33, 23, 33, 11, 32, 23, 33},
			{33, 21, 23, 0, 31, 11, 21, 0},
			{10, 22, 11, 0, 33, 32, 33, 20},
			{40, 32, 32, 11, 33, 23, 0, 11},
			{33, 21, 33, 23, 30, 23, 22, 11},
			{33, 33, 22, 0, 23, 33, 40, 60},
			{20, 40, 11, 11, 10, 10, 32, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,6, StaticCellType.GOLD));
		list.add(new StaticCell(3,4, StaticCellType.GOLD));
		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
