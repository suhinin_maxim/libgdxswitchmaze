package com.razvivashka.logic.levels.level149;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level149Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level149Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 20, 40, 0, 0, 0, 30},
			{40, 22, 31, 22, 21, 33, 0, 21},
			{33, 20, 21, 22, 0, 33, 0, 10},
			{40, 20, 23, 22, 30, 30, 0, 22},
			{10, 31, 10, 23, 22, 21, 30, 10},
			{31, 10, 20, 11, 21, 23, 0, 22},
			{10, 32, 30, 10, 0, 10, 21, 60},
			{21, 32, 40, 40, 33, 40, 32, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.BONUS));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
