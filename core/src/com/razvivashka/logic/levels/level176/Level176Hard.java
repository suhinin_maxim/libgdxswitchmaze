package com.razvivashka.logic.levels.level176;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level176Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level176Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 40, 31, 11, 40, 23, 23, 32},
			{30, 40, 30, 21, 10, 40, 33, 20},
			{33, 30, 40, 10, 20, 40, 11, 31},
			{31, 40, 23, 22, 10, 21, 21, 22},
			{11, 21, 21, 30, 33, 30, 40, 30},
			{10, 30, 11, 11, 32, 40, 23, 31},
			{21, 0, 20, 20, 30, 0, 40, 60},
			{33, 21, 32, 11, 33, 40, 40, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,0, StaticCellType.GOLD));
		list.add(new StaticCell(1,4, StaticCellType.GOLD));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(5,4, StaticCellType.BONUS));
		list.add(new StaticCell(3,7, StaticCellType.BONUS));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));
		list.add(new StaticCell(6,2, StaticCellType.BONUS));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.BONUS));
		list.add(new StaticCell(4,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 26;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
