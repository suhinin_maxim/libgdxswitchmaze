package com.razvivashka.logic.levels.level105;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level105Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level105Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 33, 20, 21, 20, 23, 11, 33},
			{40, 0, 21, 31, 22, 10, 30, 20},
			{30, 20, 20, 40, 0, 0, 32, 11},
			{40, 31, 40, 33, 11, 30, 32, 0},
			{40, 40, 22, 31, 40, 32, 10, 32},
			{23, 40, 30, 33, 30, 32, 30, 33},
			{10, 20, 10, 21, 23, 0, 40, 60},
			{40, 0, 30, 31, 33, 32, 30, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,3, StaticCellType.GOLD));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,6, StaticCellType.GOLD));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.BONUS));
		list.add(new StaticCell(6,3, StaticCellType.GOLD));
		list.add(new StaticCell(6,2, StaticCellType.BONUS));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.BONUS));
		list.add(new StaticCell(6,1, StaticCellType.BONUS));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 20;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
