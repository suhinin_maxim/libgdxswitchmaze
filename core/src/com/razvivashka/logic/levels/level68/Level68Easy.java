package com.razvivashka.logic.levels.level68;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level68Easy implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level68Easy (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 4;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 33, 20},
			{32, 40, 33, 10},
			{10, 21, 22, 60},
			{20, 10, 10, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,1, StaticCellType.GOLD));
		list.add(new StaticCell(1,0, StaticCellType.GOLD));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 8;
	}

	@Override
	public int getMaxCruising () {
		return 8;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
