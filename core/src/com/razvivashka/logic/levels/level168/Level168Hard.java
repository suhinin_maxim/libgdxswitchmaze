package com.razvivashka.logic.levels.level168;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level168Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level168Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 21, 30, 22, 32, 22, 23, 23},
			{31, 0, 32, 30, 11, 22, 20, 32},
			{31, 21, 31, 22, 32, 22, 10, 21},
			{31, 10, 40, 0, 10, 21, 10, 23},
			{31, 22, 0, 23, 40, 0, 10, 22},
			{31, 0, 40, 23, 31, 21, 33, 32},
			{20, 32, 21, 11, 32, 20, 11, 60},
			{22, 30, 10, 11, 40, 33, 40, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,5, StaticCellType.GOLD));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,7, StaticCellType.GOLD));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,6, StaticCellType.GOLD));
		list.add(new StaticCell(2,0, StaticCellType.BONUS));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
