package com.razvivashka.logic.levels.level137;

import com.razvivashka.logic.levels.GameLevelBase;
import com.razvivashka.logic.levels.GameLevelData;

/**
* Created by maxim on 06.12.2017
*/

public class Level137 extends GameLevelBase {

	public Level137 (GameLevelData gameLevelData) {
		super(gameLevelData);
	}

	@Override
	public int getLevel() {
		return 137;
}

}
