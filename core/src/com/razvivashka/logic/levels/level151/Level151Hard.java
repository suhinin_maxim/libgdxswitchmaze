package com.razvivashka.logic.levels.level151;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level151Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level151Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 10, 0, 0, 21, 23, 11, 21},
			{40, 33, 33, 33, 21, 10, 10, 32},
			{40, 22, 32, 20, 31, 0, 40, 31},
			{33, 0, 0, 32, 21, 31, 23, 0},
			{40, 10, 40, 23, 21, 11, 0, 32},
			{11, 33, 32, 32, 11, 10, 20, 20},
			{30, 33, 32, 11, 33, 0, 20, 60},
			{11, 20, 40, 30, 30, 40, 30, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,6, StaticCellType.BONUS));
		list.add(new StaticCell(4,7, StaticCellType.GOLD));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,7, StaticCellType.BONUS));
		list.add(new StaticCell(1,7, StaticCellType.BONUS));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,1, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
