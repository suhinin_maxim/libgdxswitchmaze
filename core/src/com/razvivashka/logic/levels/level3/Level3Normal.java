package com.razvivashka.logic.levels.level3;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level3Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level3Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 31, 23, 10, 22, 40},
			{30, 30, 23, 22, 23, 32},
			{10, 22, 20, 22, 22, 40},
			{32, 22, 10, 30, 10, 10},
			{0, 23, 30, 10, 10, 60},
			{10, 20, 21, 20, 40, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(2,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,2, StaticCellType.BONUS));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,5, StaticCellType.BONUS));
		list.add(new StaticCell(5,1, StaticCellType.GOLD));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.GOLD));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,4, StaticCellType.GOLD));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 100;
	}

	@Override
	public int getMaxCruising () {
		return 100;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
