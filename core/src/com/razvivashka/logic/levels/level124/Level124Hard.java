package com.razvivashka.logic.levels.level124;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level124Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level124Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 11, 21, 40, 40, 21, 32},
			{32, 0, 22, 31, 40, 23, 10, 32},
			{30, 23, 11, 21, 31, 40, 20, 21},
			{10, 10, 22, 31, 23, 21, 40, 10},
			{11, 40, 32, 10, 31, 31, 32, 21},
			{20, 30, 22, 23, 32, 20, 22, 10},
			{30, 21, 30, 23, 10, 11, 11, 60},
			{22, 23, 33, 30, 31, 31, 10, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,7, StaticCellType.BONUS));
		list.add(new StaticCell(5,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,4, StaticCellType.BONUS));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.GOLD));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,4, StaticCellType.GOLD));
		list.add(new StaticCell(3,2, StaticCellType.BONUS));
		list.add(new StaticCell(6,6, StaticCellType.BONUS));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
