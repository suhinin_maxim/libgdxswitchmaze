package com.razvivashka.logic.levels.level25;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level25Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level25Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 11, 23, 33, 10, 21, 10},
			{33, 11, 22, 23, 10, 31, 20, 23},
			{11, 0, 40, 30, 33, 20, 11, 33},
			{33, 22, 31, 30, 20, 30, 32, 20},
			{10, 20, 10, 32, 31, 23, 21, 40},
			{11, 21, 20, 30, 20, 30, 23, 20},
			{21, 10, 40, 31, 32, 32, 21, 60},
			{0, 32, 11, 22, 21, 31, 31, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
