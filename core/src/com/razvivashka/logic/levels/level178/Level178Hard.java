package com.razvivashka.logic.levels.level178;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level178Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level178Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 22, 40, 10, 40, 22, 23},
			{40, 40, 21, 0, 33, 22, 21, 23},
			{20, 23, 23, 33, 0, 0, 0, 31},
			{11, 32, 0, 31, 40, 0, 33, 10},
			{10, 40, 32, 11, 40, 40, 23, 0},
			{22, 21, 22, 32, 21, 0, 22, 21},
			{32, 33, 30, 22, 11, 11, 20, 60},
			{31, 40, 32, 30, 30, 10, 32, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.BONUS));
		list.add(new StaticCell(4,3, StaticCellType.GOLD));
		list.add(new StaticCell(1,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,7, StaticCellType.GOLD));
		list.add(new StaticCell(3,6, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
