package com.razvivashka.logic.levels.level72;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level72Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level72Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 10, 30, 20, 30, 30, 32},
			{30, 23, 31, 20, 22, 23, 22, 32},
			{32, 11, 40, 30, 0, 0, 30, 31},
			{10, 33, 31, 33, 30, 33, 31, 33},
			{10, 32, 40, 20, 10, 33, 31, 23},
			{31, 11, 30, 31, 30, 0, 20, 40},
			{0, 32, 32, 40, 23, 23, 22, 60},
			{11, 33, 30, 10, 31, 40, 32, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,6, StaticCellType.GOLD));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.BONUS));
		list.add(new StaticCell(5,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.BONUS));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
