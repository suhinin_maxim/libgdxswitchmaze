package com.razvivashka.logic.levels.level103;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level103Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level103Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 11, 0, 32, 30, 32, 32, 40},
			{11, 32, 30, 11, 10, 0, 0, 32},
			{31, 31, 21, 11, 0, 0, 30, 10},
			{33, 32, 32, 33, 21, 10, 31, 40},
			{21, 33, 31, 33, 40, 23, 20, 20},
			{31, 11, 0, 32, 11, 32, 11, 30},
			{31, 40, 10, 10, 40, 20, 22, 60},
			{22, 40, 33, 21, 10, 22, 30, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,0, StaticCellType.BONUS));
		list.add(new StaticCell(6,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,1, StaticCellType.BONUS));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.BONUS));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
