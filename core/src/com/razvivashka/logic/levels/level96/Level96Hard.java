package com.razvivashka.logic.levels.level96;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level96Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level96Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 32, 23, 10, 33, 40, 22},
			{10, 20, 30, 33, 23, 22, 33, 20},
			{10, 10, 33, 33, 20, 22, 20, 10},
			{33, 32, 30, 33, 40, 10, 22, 10},
			{32, 30, 21, 32, 23, 32, 30, 0},
			{31, 31, 33, 11, 22, 21, 33, 32},
			{30, 30, 33, 31, 33, 33, 33, 60},
			{30, 10, 32, 33, 33, 33, 10, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,5, StaticCellType.BONUS));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,5, StaticCellType.BONUS));
		list.add(new StaticCell(2,2, StaticCellType.GOLD));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,1, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
