package com.razvivashka.logic.levels.level85;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level85Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level85Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 0, 23, 20, 11, 22, 40, 0},
			{33, 30, 40, 0, 40, 32, 40, 32},
			{30, 11, 30, 22, 0, 20, 20, 10},
			{0, 22, 31, 30, 23, 22, 32, 32},
			{20, 40, 11, 32, 20, 32, 32, 20},
			{31, 0, 22, 23, 31, 23, 20, 30},
			{20, 33, 31, 22, 23, 11, 31, 60},
			{23, 21, 21, 32, 40, 40, 33, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.BONUS));
		list.add(new StaticCell(5,2, StaticCellType.BONUS));
		list.add(new StaticCell(2,7, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,7, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
