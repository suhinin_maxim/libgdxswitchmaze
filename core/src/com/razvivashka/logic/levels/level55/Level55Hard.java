package com.razvivashka.logic.levels.level55;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level55Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level55Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 30, 20, 22, 30, 32, 20, 0},
			{10, 10, 30, 33, 33, 40, 30, 0},
			{40, 40, 10, 32, 10, 11, 10, 10},
			{40, 33, 23, 20, 10, 21, 11, 20},
			{20, 33, 23, 23, 10, 33, 20, 20},
			{10, 11, 10, 33, 21, 23, 10, 33},
			{40, 40, 33, 40, 33, 32, 10, 60},
			{20, 33, 40, 40, 32, 10, 31, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,0, StaticCellType.BONUS));
		list.add(new StaticCell(4,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,5, StaticCellType.GOLD));
		list.add(new StaticCell(6,5, StaticCellType.BONUS));
		list.add(new StaticCell(5,7, StaticCellType.GOLD));
		list.add(new StaticCell(2,5, StaticCellType.GOLD));
		list.add(new StaticCell(6,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.GOLD));
		list.add(new StaticCell(5,1, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
