package com.razvivashka.logic.levels.level187;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level187Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level187Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 32, 40, 22, 21, 20, 30, 40},
			{33, 10, 40, 32, 40, 32, 22, 33},
			{11, 23, 40, 0, 22, 22, 21, 20},
			{30, 40, 11, 10, 10, 32, 21, 23},
			{20, 30, 33, 30, 21, 11, 40, 31},
			{10, 10, 20, 21, 10, 30, 20, 40},
			{22, 30, 11, 33, 22, 30, 23, 60},
			{23, 32, 32, 22, 10, 21, 32, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,1, StaticCellType.GOLD));
		list.add(new StaticCell(5,2, StaticCellType.GOLD));
		list.add(new StaticCell(2,1, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
