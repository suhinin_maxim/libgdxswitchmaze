package com.razvivashka.logic.levels;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellBase;
import com.razvivashka.logic.domaindata.StaticCell;

import java.util.List;

/**
 * Created by admin on 08.09.17.
 */

public interface GameLevelData {

    int getFieldSize();
    int[][] getBoardInt();
    List<HeroCell> getHeroCells();
    List<StaticCell> getStaticCells();
    int getStartCruising();
    int getMaxCruising();
    int getMaxScores();

}
