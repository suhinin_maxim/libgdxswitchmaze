package com.razvivashka.logic.levels.level111;

import com.razvivashka.logic.levels.GameLevelBase;
import com.razvivashka.logic.levels.GameLevelData;

/**
* Created by maxim on 06.12.2017
*/

public class Level111 extends GameLevelBase {

	public Level111 (GameLevelData gameLevelData) {
		super(gameLevelData);
	}

	@Override
	public int getLevel() {
		return 111;
}

}
