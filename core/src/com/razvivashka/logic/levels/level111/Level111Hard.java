package com.razvivashka.logic.levels.level111;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level111Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level111Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 10, 33, 30, 32, 0, 10, 20},
			{32, 30, 40, 11, 10, 40, 21, 10},
			{30, 11, 23, 33, 32, 11, 22, 0},
			{31, 21, 30, 30, 0, 40, 30, 21},
			{11, 23, 0, 32, 11, 20, 40, 30},
			{21, 11, 23, 22, 20, 31, 20, 22},
			{30, 40, 33, 22, 22, 0, 33, 60},
			{23, 32, 30, 33, 40, 30, 32, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,5, StaticCellType.GOLD));
		list.add(new StaticCell(4,7, StaticCellType.BONUS));
		list.add(new StaticCell(6,6, StaticCellType.BONUS));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,2, StaticCellType.GOLD));
		list.add(new StaticCell(6,3, StaticCellType.GOLD));
		list.add(new StaticCell(2,6, StaticCellType.BONUS));
		list.add(new StaticCell(3,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,1, StaticCellType.BONUS));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,6, StaticCellType.GOLD));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,7, StaticCellType.BONUS));
		list.add(new StaticCell(0,7, StaticCellType.BONUS));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
