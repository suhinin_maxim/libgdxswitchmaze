package com.razvivashka.logic.levels.level99;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level99Normal implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level99Normal (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 6;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 21, 10, 30, 10, 0},
			{33, 30, 0, 20, 31, 22},
			{11, 10, 40, 23, 33, 11},
			{33, 30, 11, 33, 23, 0},
			{20, 40, 31, 22, 10, 60},
			{33, 30, 31, 10, 33, 23}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(3,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,4, StaticCellType.BONUS));
		list.add(new StaticCell(0,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,2, StaticCellType.GOLD));
		list.add(new StaticCell(2,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,4, StaticCellType.BONUS));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,2, StaticCellType.BONUS));
		list.add(new StaticCell(1,4, StaticCellType.BONUS));
		list.add(new StaticCell(2,1, StaticCellType.GOLD));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 12;
	}

	@Override
	public int getMaxCruising () {
		return 12;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
