package com.razvivashka.logic.levels.level116;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level116Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level116Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 21, 32, 22, 31, 20, 33, 32},
			{40, 22, 22, 32, 23, 33, 22, 23},
			{11, 23, 0, 21, 22, 32, 23, 32},
			{10, 22, 31, 21, 20, 32, 33, 30},
			{31, 31, 32, 22, 30, 21, 21, 40},
			{21, 40, 40, 10, 33, 22, 10, 23},
			{11, 10, 30, 40, 21, 11, 33, 60},
			{22, 30, 30, 33, 23, 21, 10, 20}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,7, StaticCellType.GOLD));
		list.add(new StaticCell(7,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(4,0, StaticCellType.GOLD));
		list.add(new StaticCell(4,5, StaticCellType.GOLD));
		list.add(new StaticCell(5,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,1, StaticCellType.GOLD));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
