package com.razvivashka.logic.levels.level64;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level64Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level64Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 20, 31, 21, 11, 32, 21, 33},
			{33, 0, 22, 30, 21, 22, 21, 10},
			{21, 23, 21, 22, 20, 30, 21, 23},
			{20, 33, 30, 40, 40, 30, 22, 11},
			{40, 30, 11, 11, 32, 20, 40, 32},
			{40, 30, 31, 40, 31, 33, 11, 23},
			{22, 30, 10, 30, 30, 31, 33, 60},
			{40, 0, 23, 21, 40, 20, 30, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,1, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,5, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(4,5, StaticCellType.GOLD));
		list.add(new StaticCell(6,5, StaticCellType.BONUS));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,2, StaticCellType.GOLD));
		list.add(new StaticCell(4,6, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
