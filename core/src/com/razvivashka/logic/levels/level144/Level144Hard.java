package com.razvivashka.logic.levels.level144;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level144Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level144Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 11, 0, 31, 23, 22, 31, 30},
			{32, 32, 21, 40, 32, 22, 22, 31},
			{32, 32, 33, 40, 40, 31, 33, 11},
			{30, 31, 22, 0, 32, 30, 10, 33},
			{22, 0, 32, 30, 21, 31, 33, 23},
			{33, 20, 30, 11, 23, 23, 40, 21},
			{33, 21, 21, 23, 21, 30, 23, 60},
			{23, 40, 21, 21, 33, 33, 33, 33}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(4,2, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(6,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,5, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,2, StaticCellType.BONUS));
		list.add(new StaticCell(1,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,1, StaticCellType.BONUS));
		list.add(new StaticCell(6,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,3, StaticCellType.GOLD));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(5,4, StaticCellType.BONUS));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,0, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(3,4, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(2,3, StaticCellType.BONUS));
		list.add(new StaticCell(4,6, StaticCellType.BONUS));
		list.add(new StaticCell(2,6, StaticCellType.ADD_TIME_5));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 18;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
