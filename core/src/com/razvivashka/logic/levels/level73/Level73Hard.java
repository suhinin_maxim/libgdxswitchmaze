package com.razvivashka.logic.levels.level73;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.ArrayList;
import java.util.List;

/**
* Created by admin on 06.12.2017
*/

public class Level73Hard implements GameLevelData {

	private List<HeroCellType> mHeroCellTypes;

	public Level73Hard (List<HeroCellType> heroCellTypes) {
		mHeroCellTypes = heroCellTypes;
	}

	@Override
	public int getFieldSize () {
		return 8;
	}

	@Override
	public int[][] getBoardInt () {
		return new int[][]{
			{50, 23, 0, 30, 23, 33, 31, 32},
			{31, 23, 0, 10, 23, 23, 20, 22},
			{40, 23, 0, 31, 33, 32, 32, 20},
			{40, 30, 21, 33, 21, 21, 40, 21},
			{32, 20, 32, 23, 11, 32, 11, 32},
			{20, 40, 23, 10, 30, 20, 33, 20},
			{40, 33, 21, 31, 32, 22, 21, 60},
			{21, 33, 10, 11, 20, 31, 10, 32}
		};
	}

	@Override
	public List<HeroCell> getHeroCells () {
		HeroCellFactory heroCellFactory = new HeroCellFactory();

		List<HeroCell> list = new ArrayList<>();
		for (HeroCellType heroCellType : mHeroCellTypes) {
			HeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);
			list.add(heroCell);
		}

		return list;
	}

	@Override
	public List<StaticCell> getStaticCells () {
		List<StaticCell> list = new ArrayList<>();

		list.add(new StaticCell(5,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(5,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,2, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(6,4, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(7,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,6, StaticCellType.GOLD));
		list.add(new StaticCell(4,4, StaticCellType.BONUS));
		list.add(new StaticCell(5,3, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(1,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(1,0, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(7,7, StaticCellType.BONUS));
		list.add(new StaticCell(7,1, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(2,7, StaticCellType.BONUS));
		list.add(new StaticCell(3,4, StaticCellType.BONUS));
		list.add(new StaticCell(0,7, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(3,6, StaticCellType.ADD_TIME_10));
		list.add(new StaticCell(0,3, StaticCellType.ADD_TIME_5));
		list.add(new StaticCell(0,4, StaticCellType.GOLD));
		list.add(new StaticCell(2,5, StaticCellType.ADD_TIME_10));

		return list;
	}

	@Override
	public int getStartCruising() {
		return 16;
	}

	@Override
	public int getMaxCruising () {
		return 16;
	}

	@Override
	public int getMaxScores() {
		return 100;
	}

}
