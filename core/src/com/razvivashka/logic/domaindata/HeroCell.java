package com.razvivashka.logic.domaindata;

/**
 * Created by maxim on 13.09.2017.
 */

public interface HeroCell {

    int getPosI();
    int getPosJ();

    int getCellI();
    int getCellJ();

    void tryToMoveNext();
    void acceptMove();

    void moveBack();
    void rotateCW();
    void rotateCCW();
    void rotateWithBoard();

    HeroCell setMoveListener(OnMoveListener listener);
    OnMoveListener getMoveListener();
    HeroCellType getType();

    interface OnMoveListener {

        void OnMove(HeroCell hero);

    }

}
