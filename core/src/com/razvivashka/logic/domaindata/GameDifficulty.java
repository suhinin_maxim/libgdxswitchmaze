package com.razvivashka.logic.domaindata;

/**
 * Created by admin on 04.09.17.
 */

public enum GameDifficulty {
    EASY,
    NORMAL,
    HARD
}
