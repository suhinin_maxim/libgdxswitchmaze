package com.razvivashka.logic.domaindata;


/**
 * Created by maxim on 13.09.2017.
 */

public class HeroCellFactory {

    public HeroCell getHeroCell (int i, int j, int ci, int cj, int fieldSize, HeroCellType type) {
        return new HeroCellBase(i, j, ci, cj, fieldSize, type);
    }

}
