package com.razvivashka.logic.domaindata;

public enum GameBoardCellType {
    CORNER,
    LINE,
    CROSSROAD,
    TEE,
    GROUND,
    START,
    FINISH
}
