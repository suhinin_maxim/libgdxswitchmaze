package com.razvivashka.logic.domaindata;


public class GameBoardCell {

    private int turns;                      //amount of turn needed to init state, 0 - for symmetric
    private int degree;                     // 0-0, 1-90, 2-180, 3-270
    private GameBoardCellType type;         // type of cell

    public GameBoardCell(int typeInt) {
        switch (typeInt) {
            case 0:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.GROUND;
                break;

            case 1:
                turns = 1;
                degree = 0;
                type = GameBoardCellType.LINE;
                break;
            case 10:
                turns = 1;
                degree = 0;
                type = GameBoardCellType.LINE;
                break;
            case 11:
                turns = 1;
                degree = 1;
                type = GameBoardCellType.LINE;
                break;

            case 2:
                turns = 3;
                degree = 0;
                type = GameBoardCellType.CORNER;
                break;
            case 20:
                turns = 3;
                degree = 0;
                type = GameBoardCellType.CORNER;
                break;
            case 21:
                turns = 3;
                degree = 1;
                type = GameBoardCellType.CORNER;
                break;
            case 22:
                turns = 3;
                degree = 2;
                type = GameBoardCellType.CORNER;
                break;
            case 23:
                turns = 3;
                degree = 3;
                type = GameBoardCellType.CORNER;
                break;

            case 3:
                turns = 3;
                degree = 0;
                type = GameBoardCellType.TEE;
                break;
            case 30:
                turns = 3;
                degree = 0;
                type = GameBoardCellType.TEE;
                break;
            case 31:
                turns = 3;
                degree = 1;
                type = GameBoardCellType.TEE;
                break;
            case 32:
                turns = 3;
                degree = 2;
                type = GameBoardCellType.TEE;
                break;
            case 33:
                turns = 3;
                degree = 3;
                type = GameBoardCellType.TEE;
                break;
            case 34: //freeze point
                turns = 0;
                degree = 0;
                type = GameBoardCellType.TEE;
                break;

            case 4:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.CROSSROAD;
                break;
            case 40:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.CROSSROAD;
                break;

            case 5:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.START;
                break;
            case 50:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.START;
                break;

            case 6:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.FINISH;
                break;
            case 60:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.FINISH;
                break;

            default:
                turns = 0;
                degree = 0;
                type = GameBoardCellType.GROUND;
        }
    }

    public int getTurns() {
        return turns;
    }

    protected GameBoardCell setTurns(int turns) {
        this.turns = turns;
        return this;
    }

    public int getDegree() {
        return 90 * degree;
    }

    protected GameBoardCell setDegree(int degree) {
        this.degree = degree;
        return this;
    }

    public GameBoardCellType getType() {
        return type;
    }

    protected GameBoardCell setType(GameBoardCellType type) {
        this.type = type;
        return this;
    }

    public void rotate() {
        degree++;
        degree = degree % (turns + 1);
    }

    public boolean isWall(int i, int j) {
        if (type == GameBoardCellType.GROUND) {
            return true;
        }

        if (type == GameBoardCellType.CROSSROAD) {
            if (((i == 0) && (j == 0)) || ((i == 2) && (j == 2)) || ((i == 2) && (j == 0)) || ((i == 0) && (j == 2))) {
                return true;
            }

            return false;
        }

        if (type == GameBoardCellType.START) {
            if ((i == 2) && (j == 1)) {
                return false;
            }

            return true;
        }

        if (type == GameBoardCellType.FINISH) {
            if (((i == 2) && (j == 1)) || ((i == 1) && (j == 1))) {
                return false;
            }

            return true;
        }

        if (type == GameBoardCellType.LINE) {
            if (degree == 0 && j == 1) {
                return false;
            } else if (degree == 1 && i == 1) {
                return false;
            }

            return true;
        }

        if (type == GameBoardCellType.CORNER) {
            if (degree == 0 && (((i == 0) && (j == 1)) || ((i == 1) && (j == 1)) || ((i == 1) && (j == 2))))
                return false;

            if (degree == 1 && (((i == 1) && (j == 1)) || ((i == 1) && (j == 2)) || ((i == 2) && (j == 1))))
                return false;

            if (degree == 2 && (((i == 1) && (j == 1)) || ((i == 1) && (j == 0)) || ((i == 2) && (j == 1))))
                return false;

            if (degree == 3 && (((i == 1) && (j == 1)) || ((i == 1) && (j == 0)) || ((i == 0) && (j == 1))))
                return false;

            return true;
        }

        if (type == GameBoardCellType.TEE) {
            if (degree == 0 && j == 1)
                return false;
            if (degree == 1 && i == 1)
                return false;
            if (degree == 2 && j == 1)
                return false;
            if (degree == 3 && i == 1)
                return false;

            if (degree == 0 && i == 1 && j == 2)
                return false;
            if (degree == 1 && i == 2 && j == 1)
                return false;
            if (degree == 2 && i == 1 && j == 0)
                return false;
            if (degree == 3 && i == 0 && j == 1)
                return false;

            return true;
        }

        return true;
    }

    public int getTypeInt() {
        int typeInt = -1;

        switch (type) {
            case GROUND:
                typeInt = 0;
                break;

            case LINE:
                if (turns == 1 && degree == 0) {
                    typeInt = 10;
                } else if (turns == 1 && degree == 1) {
                    typeInt = 11;
                }
                break;

            case CORNER:
                if (turns == 3 && degree == 0) {
                    typeInt = 20;
                } else if (turns == 3 && degree == 1) {
                    typeInt = 21;
                } else if (turns == 3 && degree == 2) {
                    typeInt = 22;
                } else if (turns == 3 && degree == 3) {
                    typeInt = 23;
                }
                break;

            case TEE:
                if (turns == 3 && degree == 0) {
                    typeInt = 30;
                } else if (turns == 3 && degree == 1) {
                    typeInt = 31;
                } else if (turns == 3 && degree == 2) {
                    typeInt = 32;
                } else if (turns == 3 && degree == 3) {
                    typeInt = 33;
                }
                break;

            case CROSSROAD:
                typeInt = 40;
                break;

            case START:
                typeInt = 50;
                break;

            case FINISH:
                typeInt = 60;
                break;
        }

        return typeInt;
    }

    @Override
    public String toString() {
        return String.valueOf(getTypeInt());
    }
}
