package com.razvivashka.logic.domaindata;


public class HeroCellBase implements HeroCell {

    protected HeroCellType mType;

    protected int posI, posJ;
    protected int cellI, cellJ;

    protected int posIold, posJold;
    protected int cellIold, cellJold;

    protected int mFieldSize;

    protected int dirI, dirJ;

    protected OnMoveListener moveListener;

    public HeroCellBase(int i, int j, int ci, int cj, int fieldSize, HeroCellType type) {
        posI = i;
        posJ = j;
        cellI = ci;
        cellJ = cj;
        dirJ = 0;
        dirI = 1;
        mFieldSize = fieldSize;
        mType = type;
    }

    @Override
    public HeroCellType getType () {
        return mType;
    }

    @Override
    public HeroCell.OnMoveListener getMoveListener() {
        return moveListener;
    }

    @Override
    public HeroCell setMoveListener(OnMoveListener moveListener) {
        this.moveListener = moveListener;
        return this;
    }

    @Override
    public int getPosI() {
        return posI;
    }

    @Override
    public int getPosJ() {
        return posJ;
    }

    @Override
    public int getCellI() {
        return cellI;
    }

    @Override
    public int getCellJ() {
        return cellJ;
    }

    @Override
    public void tryToMoveNext() {
        posIold = posI;
        posJold = posJ;
        cellIold = cellI;
        cellJold = cellJ;

        posI += ((3 + cellI + dirI) / 3 - 1 + mFieldSize) % mFieldSize;
        posI = posI % mFieldSize;
        cellI = (3 + cellI + dirI) % 3;

        posJ += ((3 + cellJ + dirJ) / 3 - 1 + mFieldSize) % mFieldSize;
        posJ = posJ % mFieldSize;
        cellJ = (3 + cellJ + dirJ) % 3;
    }

    @Override
    public void acceptMove() {
        if (moveListener != null) {
            moveListener.OnMove(this);
        }
    }

    @Override
    public void moveBack() {
        posI = posIold;
        posJ = posJold;
        cellI = cellIold;
        cellJ = cellJold;
    }

    @Override
    public void rotateCW() {
        if (dirI == 1 && dirJ == 0) {
            dirI = 0;
            dirJ = -1;
            return;
        }
        if (dirI == 0 && dirJ == -1) {
            dirI = -1;
            dirJ = 0;
            return;
        }
        if (dirI == -1 && dirJ == 0) {
            dirI = 0;
            dirJ = 1;
            return;
        }
        if (dirI == 0 && dirJ == 1) {
            dirI = 1;
            dirJ = 0;
            return;
        }
    }

    @Override
    public void rotateCCW() {
        if (dirI == 1 && dirJ == 0) {
            dirI = 0;
            dirJ = 1;
            return;
        }
        if (dirI == 0 && dirJ == 1) {
            dirI = -1;
            dirJ = 0;
            return;
        }
        if (dirI == -1 && dirJ == 0) {
            dirI = 0;
            dirJ = -1;
            return;
        }
        if (dirI == 0 && dirJ == -1) {
            dirI = 1;
            dirJ = 0;
            return;
        }
    }

    @Override
    public void rotateWithBoard() {
        cellI = 1;
        cellJ = 1;

        cellIold = cellI;
        cellJold = cellJ;
        posIold = posI;
        posJold = posJ;

        if (moveListener != null) {
            moveListener.OnMove(this);
        }
    }

}
