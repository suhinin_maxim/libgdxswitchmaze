package com.razvivashka.logic.domaindata;


public class StaticCell {

    private int posI, posJ;
    private StaticCellType type;
    private OnActionListener onActionListener;

    public StaticCell(int posI, int posJ, StaticCellType type) {
        this.posI = posI;
        this.posJ = posJ;
        this.type = type;
    }

    public int getPosI() {
        return posI;
    }

    public int getPosJ() {
        return posJ;
    }

    public StaticCellType getType() {
        return type;
    }

    public void setOnActionListener(OnActionListener onActionListener) {
        this.onActionListener = onActionListener;
    }

    public void startAction() {
        if(onActionListener != null){
            onActionListener.onAction(this);
        }
    }

    public interface OnActionListener {

        void onAction(StaticCell cell);

    }

}
