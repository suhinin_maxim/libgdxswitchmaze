package com.razvivashka.logic.domaindata;

public enum GameState {
    PLAY,
    FINISH,
    LOOSE,
    WIN,
    BONUSE_MOVIE,
    PAUSE
}
