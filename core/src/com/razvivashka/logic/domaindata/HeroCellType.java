package com.razvivashka.logic.domaindata;

/**
 * Created by admin on 05.09.17.
 */

public enum HeroCellType {
    HERO_ONE,
    HERO_TWO,
    HERO_THREE,
    HERO_FOUR,
    HERO_FIVE,
    HERO_SIX
}
