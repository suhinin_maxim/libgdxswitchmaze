package com.razvivashka.logic;


import com.razvivashka.logic.domaindata.GameBoardCell;
import com.razvivashka.logic.domaindata.GameDifficulty;
import com.razvivashka.logic.domaindata.GameState;
import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.levels.GameLevel;
import com.razvivashka.logic.levels.GameLevelFactory;

import java.util.ArrayList;
import java.util.List;

public class GameLogicStandard implements GameLogic {

    //region Fields

    private GameLevel mLevelLogic;
    private GameState mGameState;
    private GameDifficulty mGameDifficulty;
    private List<HeroCellType> mHeroTypes;
    private long mTimeLeft;

    private GameLevelFactory mGameLevelFactory;

    //endregion


    // region constructors

    public GameLogicStandard(){
        mHeroTypes = new ArrayList<>();
        mGameDifficulty = GameDifficulty.NORMAL;
        mGameState = GameState.PLAY;

        mGameLevelFactory = new GameLevelFactory();
        mTimeLeft = 0;
        mLevelLogic = mGameLevelFactory.getGameLevel(1, mGameDifficulty, mHeroTypes);
    }

    public GameLogicStandard(GameLevel levelLogic) {
        mHeroTypes = new ArrayList<>();
        mGameDifficulty = GameDifficulty.NORMAL;
        mGameState = GameState.PLAY;

        mGameLevelFactory = new GameLevelFactory();
        mTimeLeft = 0;
        mLevelLogic = levelLogic;
    }

    // endregion


    //region implements GameLogic

    @Override
    public GameBoardCell[][] getField() {
        return mLevelLogic.getField();
    }

    @Override
    public int getFieldSize() {
        return mLevelLogic.getFieldSize();
    }

    @Override
    public GameBoardCell move(int i, int j) {
        return mLevelLogic.move(i,j);
    }

    @Override
    public List<HeroCell> getHeroes() {
        return mLevelLogic.getHeroes();
    }

    @Override
    public List<StaticCell> getStaticCells() {
        return mLevelLogic.getStaticCells();
    }

    @Override
    public void gameMove(){
        mLevelLogic.checkBonus();

        if(mLevelLogic.isFinish() == false) {
            if (mLevelLogic.moveHeroes()) {
                mLevelLogic.incSteps();
            }
        }

        if(mLevelLogic.isFinish() == true){
            mGameState = GameState.WIN;
        } else if (mLevelLogic.isLose() == true) {
            mGameState = GameState.LOOSE;
        }
    }

    @Override
    public int getLevelCount() {
        return 166;
    }

    @Override
    public int getMaxLevelNumber() {
        return 200;
    }

    @Override
    public int getCurrentLevel() {
        return mLevelLogic.getLevel();
    }

    @Override
    public boolean loadNextLevel() {
        GameLevel nextLevel = findLevel(mLevelLogic.getLevel() + 1);
        if (nextLevel != null) {
            loadLevel(nextLevel.getLevel());
            return true;
        }

        return false;
    }

    @Override
    public void replayLevel() {
        loadLevel(mLevelLogic.getLevel());
    }

    @Override
    public void loadLevel(int level) {
        mLevelLogic = mGameLevelFactory.getGameLevel(level, mGameDifficulty, mHeroTypes);

        mTimeLeft = 0;
        mGameState = GameState.PLAY;
    }

    @Override
    public GameLevel findLevel(int level) {
        for (int i=level; i<=getMaxLevelNumber(); i++) {
            GameLevel gameLevel = mGameLevelFactory.getGameLevel(i, mGameDifficulty, mHeroTypes);
            if (gameLevel != null) {
                return gameLevel;
            }
        }

        return null;
    }

    @Override
    public int calcScores() {
        return mLevelLogic.calcScores();
    }

    @Override
    public int calcStars() {
        return mLevelLogic.calcStars();
    }

    @Override
    public void setGameState(GameState gameState) {
        this.mGameState = gameState;
    }

    @Override
    public GameState getGameState() {
        return mGameState;
    }

    @Override
    public void setDifficulty (GameDifficulty difficulty) {
        mGameDifficulty = difficulty;
    }

    @Override
    public GameDifficulty getDifficulty () {
        return mGameDifficulty;
    }

    @Override
    public void clearHeroCellTypes () {
        mHeroTypes.clear();
    }

    @Override
    public void addHeroCellType (HeroCellType heroCellType) {
        mHeroTypes.add(heroCellType);
    }

    @Override
    public List<HeroCellType> getHeroCellTypes() {
        return mHeroTypes;
    }

    @Override
    public int getCollectedBonusCount(String key) {
        int count = 0;

        if (mLevelLogic.getCollectedBonuses().containsKey(key)) {
            count = mLevelLogic.getCollectedBonuses().get(key);
        }

        return count;
    }

    @Override
    public int getCruising() {
        return mLevelLogic.getCruising();
    }

    @Override
    public int getMaxCruising() {
        return mLevelLogic.getMaxCruising();
    }

    @Override
    public int getSteps() {
        return mLevelLogic.getSteps();
    }

    @Override
    public void incTimeLeft(long millis) {
        mTimeLeft += millis;
    }

    @Override
    public long getTimeLeft() {
        return mTimeLeft;
    }

    //endregion

}
