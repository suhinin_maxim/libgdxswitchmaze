package com.razvivashka.logic;

import com.razvivashka.logic.domaindata.GameBoardCell;
import com.razvivashka.logic.domaindata.GameDifficulty;
import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.GameState;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.levels.GameLevel;

import java.util.List;

public interface GameLogic {

    GameBoardCell[][] getField();
    int getFieldSize();

    GameBoardCell move(int i, int j);
    List<HeroCell> getHeroes();
    List<StaticCell> getStaticCells();
    void gameMove();

    int getLevelCount();
    int getMaxLevelNumber();
    int getCurrentLevel();

    boolean loadNextLevel();
    void replayLevel();
    void loadLevel(int level);
    GameLevel findLevel(int level);

    int calcScores();
    int calcStars();

    void setGameState(GameState gameState);
    GameState getGameState();

    void setDifficulty(GameDifficulty difficulty);
    GameDifficulty getDifficulty();

    void addHeroCellType(HeroCellType heroCellType);
    void clearHeroCellTypes();
    List<HeroCellType> getHeroCellTypes();

    int getCruising();
    int getMaxCruising();
    int getSteps();

    void incTimeLeft(long millis);
    long getTimeLeft();
    int getCollectedBonusCount(String key);

}
