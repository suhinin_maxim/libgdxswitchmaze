package com.razvivashka.services;

/**
 * Created by maxim on 08.12.2017.
 */

public interface Logger {

    void startLevel(int number);
    void winLevel(int number, int scores);
    void loseLevel(int number);

}
