package com.razvivashka.services;

/**
 * Created by maxim on 03.01.2018.
 */

public interface AdvertisingOnClose {

    void onClose();

}
