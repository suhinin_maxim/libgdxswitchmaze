package com.razvivashka.generator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by maxim on 03.10.2017.
 */

public class WaveAlgorithm {

    private boolean[][] table;
    private int rows;
    private int cols;
    private BoardPoint startPoint;
    private BoardPoint finishPoint;
    private LinkedHashMap<String,BoardPoint> previousPoints;

    public WaveAlgorithm(boolean[][] table, BoardPoint startPoint, BoardPoint finishPoint) {
        this.table = table;
        this.rows = table.length;
        this.cols = table[0].length;
        this.startPoint = startPoint;
        this.finishPoint = finishPoint;
    }

    public int computeDist() {
        int[][] dist = new int[rows][cols];
        int[][] directions = { {1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        List<BoardPoint> oldFront = new ArrayList<>();
        List<BoardPoint> newFront = new ArrayList<>();
        int currentDist = 0;

        for(int i=0; i<rows; i++) {
            for (int j=0; j<cols; j++) {
                dist[i][j] = table[i][j] == false ? -1 : Integer.MAX_VALUE;
            }
        }

        oldFront.add(startPoint);
        dist[startPoint.row][startPoint.col] = 0;
        previousPoints = new LinkedHashMap<>();

        while(true) {
            currentDist++;

            for(BoardPoint p : oldFront) {
                for(int[] dir : directions) {
                    BoardPoint newPoint = new BoardPoint(p.row + dir[0], p.col + dir[1]);

                    if (isOutOfField(newPoint)) {
                        continue;
                    }
                    if (dist[newPoint.row][newPoint.col] != -1) {
                        continue;
                    }

                    dist[newPoint.row][newPoint.col] = currentDist;
                    previousPoints.put(newPoint.toString(), p);

                    if (newPoint.equals(finishPoint) == true) {
                        return currentDist;
                    }

                    newFront.add(newPoint);
                }
            }

            if (newFront.isEmpty()) {
                return -1;
            }

            oldFront = new ArrayList<>(newFront);
            newFront.clear();
        }
    }

    public boolean isOutOfField(BoardPoint p) {
        return (p.row < 0 || p.row >= rows || p.col < 0 || p.col >= cols);
    }

    public List<BoardPoint> getPath() {
        if(previousPoints.containsKey(finishPoint.toString()) == false) {
            return null;
        }

        BoardPoint lastPoint = finishPoint;
        List<BoardPoint> path = new ArrayList<>();
        while (lastPoint.equals(startPoint) == false) {
            path.add(lastPoint);
            lastPoint = previousPoints.get(lastPoint.toString());
        }
        path.add(startPoint);

        return path;
    }

}