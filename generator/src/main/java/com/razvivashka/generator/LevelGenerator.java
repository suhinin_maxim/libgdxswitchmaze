package com.razvivashka.generator;

import com.beust.jcommander.JCommander;
import com.razvivashka.generator.args.MainArgs;
import com.razvivashka.logic.GameLogicStandard;
import com.razvivashka.logic.domaindata.GameBoardCell;
import com.razvivashka.logic.domaindata.GameDifficulty;
import com.razvivashka.logic.domaindata.GameState;
import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.HeroCellFactory;
import com.razvivashka.logic.domaindata.HeroCellType;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.domaindata.StaticCellType;
import com.razvivashka.logic.levels.GameLevel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


public class LevelGenerator {

    private static final String LEVELS_PATH = "C:\\Users\\maxim\\Documents\\libgdx\\Switchmaze\\core\\src\\com\\razvivashka\\logic\\levels\\";

    private Random mRandom = new Random();

    private int[][] mBoardInt;
    private List<BoardPoint> mAvailablePoints;                               // available points for place heroes, static cells, etc.
    private List<HeroCell> mHeroCells;
    private List<StaticCell> mStaticCells;

    private int mStartCruising;
    private int mMaxCruising;
    private int mFieldSize;
    private int mMaxScores;

    private String mGameLevelName;
    private String mGameLevelDifficultyName;
    private int mGameLevelNumber;

    private int[] mBoardCellsInt = {
            0,
            10, 11,
            20, 21, 22, 23,
            30, 31, 32, 33,
            40
    };

    private StaticCellType[] mBenzineCells = {
            StaticCellType.ADD_TIME_5,
            StaticCellType.ADD_TIME_10
    };

    public GameLevel buildGameLevel() {
        GameLevelAttemptData gameLevelData = new GameLevelAttemptData();
        gameLevelData.setBoardInt(mBoardInt);
        gameLevelData.setHeroCells(mHeroCells);
        gameLevelData.setStaticCells(mStaticCells);
        gameLevelData.setFieldSize(mFieldSize);                                             // TODO game don't support non square fields
        gameLevelData.setStartCruising(mStartCruising);
        gameLevelData.setMaxCruising(mMaxCruising);
        gameLevelData.setMaxScores(mMaxScores);

        return new GameLevelAttempt(gameLevelData);
    }

    private void generateDifficultyLevel(MainArgs mainArgs) {
        BoardPoint startPoint = new BoardPoint(mainArgs.start.get(0), mainArgs.start.get(1));
        BoardPoint finishPoint = new BoardPoint(mainArgs.finish.get(0), mainArgs.finish.get(1));
        BoardSize boardSize = new BoardSize(mainArgs.size.get(0), mainArgs.size.get(1));

        mGameLevelName = mainArgs.levelName;
        mGameLevelDifficultyName = mainArgs.difficultyName;
        mGameLevelNumber = mainArgs.level;
        mMaxCruising = mainArgs.size.get(0) + mainArgs.size.get(1);
        mFieldSize = mainArgs.size.get(0);

        boolean boardConvenient = false;
        do {
            int[][] boardInt = randomBoard(boardSize, startPoint, finishPoint);

            GameBoardCell[][] boardCells = toGameBoard(boardInt);
            PathFinder pathFinder = new PathFinder(boardCells, startPoint, finishPoint);
            List<BoardPoint> path = pathFinder.findPath();

            if (path != null) {
                mStaticCells = new ArrayList<>();
                mStartCruising = Math.max(path.size(), mMaxCruising);
                placeBenzineOnPath(path);

                mHeroCells = randomHeroCells(startPoint, boardInt);
                mStaticCells.addAll(randomStaticCells(boardInt));

                List<BoardPoint> rotatingPoints = getRotatingPoints(boardCells, path);
                if (rotatingPoints.size() >= mainArgs.switches) {
                    boardConvenient = tryPassLevel(boardInt);
                    makeSwitches(boardCells, path, mainArgs.switches);
                    mBoardInt = toBoardInt(boardCells);
                    mMaxScores = 100;                                                                 //TODO run wave algorithm to calculate max score
                }
            }
        } while (boardConvenient == false);

        createLevelDifficultyClass();
    }

    private boolean tryPassLevel(int[][] boardInt) {
        GameLevelAttemptData gameLevelData = new GameLevelAttemptData();
        gameLevelData.setBoardInt(boardInt);
        gameLevelData.setHeroCells(new ArrayList<>(mHeroCells));
        gameLevelData.setStaticCells(new ArrayList<>(mStaticCells));
        gameLevelData.setFieldSize(mFieldSize);                                             // TODO game don't support non square fields
        gameLevelData.setStartCruising(mStartCruising);
        gameLevelData.setMaxCruising(mMaxCruising);
        gameLevelData.setMaxScores(mMaxScores);

        GameLevel gameLevel = new GameLevelAttempt(gameLevelData);
        GameLogicStandard gameLogicStandard = new GameLogicStandard(gameLevel);

        while(gameLogicStandard.getGameState() == GameState.PLAY) {
            gameLogicStandard.gameMove();
//        gameLogic.incTimeLeft(SPEED_TIMER_TIME);
        }

        return gameLogicStandard.getGameState() == GameState.WIN;
    }

    private void placeBenzineOnPath(List<BoardPoint> path) {
        int benzinePointIndex = mStartCruising / 3;
        int cruising = mStartCruising % 3;
        while(benzinePointIndex < path.size() - 1) {
            BoardPoint benzinePoint = path.get(benzinePointIndex);
            StaticCellType benzineCellType = randomBenzineCell();
            mStaticCells.add(new StaticCell(benzinePoint.row, benzinePoint.col, benzineCellType));
            mAvailablePoints.remove(benzinePoint);

            int addedTime = 0;
            switch (benzineCellType) {
                case ADD_TIME_5:
                    addedTime = 5;
                    break;
                case ADD_TIME_10:
                    addedTime = 10;
                    break;
            }

            benzinePointIndex += (cruising + addedTime) / 3;
            cruising = (cruising + addedTime) % 3;
        }
    }

    private int[][] randomBoard(BoardSize size, BoardPoint start, BoardPoint finish) {
        mAvailablePoints = new ArrayList<>();

        int[][] boardInt = new int[size.rows][size.cols];
        for (int i=0; i<size.rows; i++) {
            for (int j=0; j<size.cols; j++) {
                if (i == start.row && j == start.col) {
                    boardInt[i][j] = 50;
                } else if (i == finish.row && j == finish.col) {
                    boardInt[i][j] = 60;
                } else {
                    int cellInt = randomBoardCellInt();
                    boardInt[i][j] = cellInt;

                    if (cellInt != 0) {
                        mAvailablePoints.add(new BoardPoint(i,j));
                    }
                }
            }
        }

        return boardInt;
    }

    private StaticCellType randomBenzineCell() {
        int randomIndex = mRandom.nextInt(mBenzineCells.length);

        return mBenzineCells[randomIndex];
    }

    private int randomBoardCellInt () {
        int randomIndex = mRandom.nextInt(mBoardCellsInt.length);

        return mBoardCellsInt[randomIndex];
    }

    private List<HeroCellType> randomHeroCellTypes (int count) {
        List<HeroCellType> types = new ArrayList<>();

        for (int i=0; i<count; i++) {
            types.add(randomHeroCellType());
        }

        return types;
    }

    private HeroCellType randomHeroCellType () {
        int randomTypeIndex = randomInt(0, HeroCellType.values().length-1);
        return HeroCellType.values()[randomTypeIndex];
    }

    private List<HeroCell> randomHeroCells (BoardPoint startPoint, int[][] boardInt) {
        HeroCellFactory heroCellFactory = new HeroCellFactory();

        HeroCellType heroCellType = randomHeroCellType();
        HeroCell heroCell = heroCellFactory.getHeroCell(startPoint.row, startPoint.col, 1, 1, boardInt.length, heroCellType);

        List<HeroCell> list = new ArrayList<>();
        list.add(heroCell);

        // TODO for support more than one hero
//        int heroesCount = randomInt(1, mFieldSize);
//        List<HeroCellType> heroCellTypes = randomHeroCellTypes(heroesCount);
//
//        for (HeroCellType heroCellType : heroCellTypes) {
//            BoardPoint randomPoint = randomAvailablePoint();
//
//            HeroCell heroCell = heroCellFactory.getHeroCell(randomPoint.row, randomPoint.col, 1, 1, boardInt.length, heroCellType);        // TODO game don't support non square fields
//            list.add(heroCell);
//        }
//
        return list;
    }

    private List<StaticCell> randomStaticCells (int[][] boardInt) {
        int staticCellsCount = randomInt(boardInt.length/2, 2*Math.max(boardInt.length, boardInt[0].length));

        List<StaticCell> cells = new ArrayList<>();
        for (int k=0; k<staticCellsCount; k++) {
            BoardPoint randomPoint = randomAvailablePoint();

            int typeIndex = randomInt(0, StaticCellType.values().length-1);
            StaticCellType type = StaticCellType.values()[typeIndex];

            cells.add(new StaticCell(randomPoint.row, randomPoint.col, type));
        }

        return cells;
    }

    private List<BoardPoint> getRotatingPoints(GameBoardCell[][] boardCells, List<BoardPoint> path) {
        List<BoardPoint> rotatingPoints  = new ArrayList<>(path);

        // remove point from path witch can't rotates
        Iterator<BoardPoint> iterator = rotatingPoints.iterator();
        while (iterator.hasNext() == true) {
            BoardPoint pathPoint = iterator.next();
            GameBoardCell boardCell = boardCells[pathPoint.row][pathPoint.col];
            if (boardCell.getTurns() == 0) {
                iterator.remove();
            }
        }

        return rotatingPoints;
    }

    private void makeSwitches(GameBoardCell[][] boardCells, List<BoardPoint> path, int switches) {
        List<BoardPoint> pathCopy  = new ArrayList<>(path);

        for (int k=0; k<switches; k++) {
            int randomCellOnPathIndex = randomInt(0, pathCopy.size() - 1);
            BoardPoint randomPoint = pathCopy.get(randomCellOnPathIndex);
            boardCells[randomPoint.row][randomPoint.col].rotate();

            pathCopy.remove(randomCellOnPathIndex);
        }
    }

    private BoardPoint randomAvailablePoint() {
        int index = randomInt(0, mAvailablePoints.size()-1);
        BoardPoint point = mAvailablePoints.get(index);
        mAvailablePoints.remove(index);

        return point;
    }

    private GameBoardCell[][] toGameBoard(int[][] boardInt) {
        GameBoardCell[][] boardCells = new GameBoardCell[boardInt.length][boardInt[0].length];

        for (int i=0; i<boardInt.length; i++) {
            for (int j=0; j<boardInt[0].length; j++) {
                boardCells[i][j] = new GameBoardCell(boardInt[i][j]);
            }
        }

        return boardCells;
    }

    private int[][] toBoardInt(GameBoardCell[][] boardCells) {
        int[][] boardInt = new int[boardCells.length][boardCells[0].length];
        for (int i=0; i<boardCells.length; i++) {
            for (int j=0; j<boardCells[0].length; j++) {
                boardInt[i][j] = boardCells[i][j].getTypeInt();
            }
        }

        return boardInt;
    }

    private int randomInt (int min, int max) {
        return min + mRandom.nextInt((max - min) + 1);
    }

    private void createLevelFolder() {
        Path path = Paths.get(LEVELS_PATH + mGameLevelName.toLowerCase());
        if (Files.exists(path) == false) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createLevelClass() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(LEVELS_PATH + mGameLevelName.toLowerCase() + "\\" + mGameLevelName + ".java", "UTF-8");

            writer.println("package com.razvivashka.logic.levels." + mGameLevelName.toLowerCase() + ";");
            writer.println();
            writer.println("import com.razvivashka.logic.levels.GameLevelBase;");
            writer.println("import com.razvivashka.logic.levels.GameLevelData;");
            writer.println();
            writer.println("/**");
            writer.println("* Created by maxim on " + new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
            writer.println("*/");
            writer.println();
            writer.println("public class " + mGameLevelName + " extends GameLevelBase {");
            writer.println();

            // constructor
            writer.println("\tpublic " + mGameLevelName + " (GameLevelData gameLevelData) {");
            writer.println("\t\tsuper(gameLevelData);");
            writer.println("\t}");
            writer.println();

            // getLevel()
            writer.println("\t@Override");
            writer.println("\tpublic int getLevel() {");
            writer.println("\t\treturn " + mGameLevelNumber + ";");
            writer.println("}");
            writer.println();
            writer.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }

    }

    private void createLevelDifficultyClass() {
        // print results
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(LEVELS_PATH + mGameLevelName.toLowerCase() + "\\" + mGameLevelName + mGameLevelDifficultyName + ".java", "UTF-8");
            GameLevel gameLevel = buildGameLevel();

            // includes
            writer.println("package com.razvivashka.logic.levels." + mGameLevelName.toLowerCase() + ";");
            writer.println();
            writer.println("import com.razvivashka.logic.domaindata.HeroCell;");
            writer.println("import com.razvivashka.logic.domaindata.HeroCellFactory;");
            writer.println("import com.razvivashka.logic.domaindata.HeroCellType;");
            writer.println("import com.razvivashka.logic.domaindata.StaticCell;");
            writer.println("import com.razvivashka.logic.domaindata.StaticCellType;");
            writer.println("import com.razvivashka.logic.levels.GameLevelData;");
            writer.println();
            writer.println("import java.util.ArrayList;");
            writer.println("import java.util.List;");
            writer.println();
            writer.println("/**");
            writer.println("* Created by admin on " + new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
            writer.println("*/");
            writer.println();

            // class name
            writer.println("public class " + mGameLevelName + mGameLevelDifficultyName + " implements GameLevelData {");
            writer.println();
            writer.println("\tprivate List<HeroCellType> mHeroCellTypes;");
            writer.println();

            // constructor
            writer.println("\tpublic " + mGameLevelName + mGameLevelDifficultyName + " (List<HeroCellType> heroCellTypes) {");
            writer.println("\t\tmHeroCellTypes = heroCellTypes;");
            writer.println("\t}");
            writer.println();

            // field size
            writer.println("\t@Override");
            writer.println("\tpublic int getFieldSize () {");
            writer.println("\t\treturn " + gameLevel.getFieldSize() + ";");
            writer.println("\t}");
            writer.println();

            // board
            writer.println("\t@Override");
            writer.println("\tpublic int[][] getBoardInt () {");
            writer.println("\t\treturn new int[][]{");

            for (int i=0; i<gameLevel.getFieldSize(); i++) {
                writer.print("\t\t\t{");
                for (int j=0; j<gameLevel.getFieldSize() - 1; j++) {
                    writer.print(gameLevel.getField()[i][j] + ", ");
                }
                writer.println(gameLevel.getField()[i][gameLevel.getFieldSize() - 1] + "}" + ((i < gameLevel.getFieldSize() - 1) ? "," : ""));
            }

            writer.println("\t\t};");
            writer.println("\t}");
            writer.println();

            // hero cells
            writer.println("\t@Override");
            writer.println("\tpublic List<HeroCell> getHeroCells () {");
            writer.println("\t\tHeroCellFactory heroCellFactory = new HeroCellFactory();");
            writer.println();
            writer.println("\t\tList<HeroCell> list = new ArrayList<>();");
            writer.println("\t\tfor (HeroCellType heroCellType : mHeroCellTypes) {");
            writer.println("\t\t\tHeroCell heroCell = heroCellFactory.getHeroCell(0, 0, 1, 1, getFieldSize(), heroCellType);");
            writer.println("\t\t\tlist.add(heroCell);");
            writer.println("\t\t}");
            writer.println();
            writer.println("\t\treturn list;");
            writer.println("\t}");
            writer.println();

//            writer.println("heroCells list:");
//            for (HeroCell heroCell : gameLevel.getHeroes()) {
//                writer.println("HeroCell heroCell = heroCellFactory.getHeroCell(" + heroCell.getPosI() + ", " + heroCell.getCellJ() + ", 1, 1, getFieldSize(), heroCellType);");
//            }

            // static cells
            writer.println("\t@Override");
            writer.println("\tpublic List<StaticCell> getStaticCells () {");
            writer.println("\t\tList<StaticCell> list = new ArrayList<>();");
            writer.println();
            for (StaticCell staticCell : gameLevel.getStaticCells()) {
                writer.println("\t\tlist.add(new StaticCell(" + staticCell.getPosI() + "," + staticCell.getPosJ() + ", StaticCellType." + staticCell.getType().name() + "));");
            }
            writer.println();
            writer.println("\t\treturn list;");
            writer.println("\t}");
            writer.println();

            // start cruising
            writer.println("\t@Override");
            writer.println("\tpublic int getStartCruising() {");
            writer.println("\t\treturn " + mStartCruising + ";");
            writer.println("\t}");
            writer.println();

            // max cruising
            writer.println("\t@Override");
            writer.println("\tpublic int getMaxCruising () {");
            writer.println("\t\treturn " + mMaxCruising + ";");
            writer.println("\t}");
            writer.println();

            // max scores
            writer.println("\t@Override");
            writer.println("\tpublic int getMaxScores() {");
            writer.println("\t\treturn " + mMaxScores + ";");
            writer.println("\t}");
            writer.println();

            writer.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    private String createLevelShortName(int level) {
        return "Level" + String.valueOf(level);
    }

    private void generateLevelFactoryClass(MainArgs mainArgs) {
        PrintWriter writer = null;
        try {
            String fileName = LEVELS_PATH + "GameLevelFactory.java";
            Path path = Paths.get(fileName);
            if (Files.exists(path) == true) {
                Files.delete(path);
            }
            writer = new PrintWriter(new FileOutputStream(new File(fileName), true));

            // includes
            writer.println("package com.razvivashka.logic.levels;");
            writer.println();
            writer.println("import com.razvivashka.logic.domaindata.GameDifficulty;");
            writer.println("import com.razvivashka.logic.domaindata.HeroCellType;");
            writer.println();

            // include levels
            for(int i=1; i<=mainArgs.levelCount; i++) {
                String levelName = createLevelShortName(i);

                writer.println("import com.razvivashka.logic.levels." + levelName.toLowerCase() + "." + levelName + ";");
                writer.println("import com.razvivashka.logic.levels." + levelName.toLowerCase() + "." + levelName + "Easy;");
                writer.println("import com.razvivashka.logic.levels." + levelName.toLowerCase() + "." + levelName + "Hard;");
                writer.println("import com.razvivashka.logic.levels." + levelName.toLowerCase() + "." + levelName + "Normal;");
                writer.println();
            }

            writer.println();
            writer.println("import java.util.List;");
            writer.println();
            writer.println("/**");
            writer.println("* Created by admin on " + new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
            writer.println("*/");
            writer.println();

            // class name
            writer.println("public class GameLevelFactory {");
            writer.println();

            // getGameLevel
            writer.println("\tpublic GameLevel getGameLevel (int level, GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {");
            writer.println("\t\tGameLevel gameLevel = null;");
            writer.println();
            writer.println("\t\tswitch (level) {");

            for (int i=1; i<=mainArgs.levelCount; i++) {
                String levelName = createLevelShortName(i);

                writer.println("\t\t\tcase " + i + ":");
                writer.println("\t\t\t\tgameLevel = get" + levelName + "(gameDifficulty, heroCellTypes);");
                writer.println("\t\t\t\tbreak;");
            }

            writer.println("\t\t}");
            writer.println();
            writer.println("\t\treturn gameLevel;");
            writer.println("\t}");
            writer.println();

            // levels
            for (int i=1; i<=mainArgs.levelCount; i++) {
                String levelName = createLevelShortName(i);

                writer.println("\tpublic " + levelName + " get" + levelName + " (GameDifficulty gameDifficulty, List<HeroCellType> heroCellTypes) {");
                writer.println("\t\tGameLevelData gameLevelData;");
                writer.println();
                writer.println("\t\tswitch (gameDifficulty) {");
                writer.println("\t\t\tcase EASY:");
                writer.println("\t\t\t\tgameLevelData = new " + levelName + "Easy(heroCellTypes);");
                writer.println("\t\t\t\tbreak;");
                writer.println("\t\t\tcase NORMAL:");
                writer.println("\t\t\tdefault:");
                writer.println("\t\t\t\tgameLevelData = new " + levelName + "Normal(heroCellTypes);");
                writer.println("\t\t\t\tbreak;");
                writer.println("\t\t\tcase HARD:");
                writer.println("\t\t\t\tgameLevelData = new " + levelName + "Hard(heroCellTypes);");
                writer.println("\t\t\t\tbreak;");
                writer.println("\t\t}");
                writer.println();
                writer.println("\t\treturn new " + levelName + "(gameLevelData);");
                writer.println("\t}");
            }
            writer.println();
            writer.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    private void generateFullLevel(MainArgs mainArgs) {
        mGameLevelName = mainArgs.levelName;
        mGameLevelNumber = mainArgs.level;

        createLevelFolder();
        createLevelClass();

        for (GameDifficulty difficulty : GameDifficulty.values()) {
            switch (difficulty) {
                case EASY:
                    mainArgs.size = new ArrayList<>(Arrays.asList(4, 4));
                    mainArgs.difficultyName = "Easy";
                    mainArgs.start = new ArrayList<>(Arrays.asList(0, 0));
                    mainArgs.finish = new ArrayList<>(Arrays.asList(2, 3));
                    mainArgs.switches = 3;
                    break;
                case NORMAL:
                    mainArgs.size = new ArrayList<>(Arrays.asList(6, 6));
                    mainArgs.difficultyName = "Normal";
                    mainArgs.start = new ArrayList<>(Arrays.asList(0, 0));
                    mainArgs.finish = new ArrayList<>(Arrays.asList(4, 5));
                    mainArgs.switches = 6;
                    break;
                case HARD:
                    mainArgs.size = new ArrayList<>(Arrays.asList(8, 8));
                    mainArgs.difficultyName = "Hard";
                    mainArgs.start = new ArrayList<>(Arrays.asList(0, 0));
                    mainArgs.finish = new ArrayList<>(Arrays.asList(6, 7));
                    mainArgs.switches = 9;
                    break;
            }

            generateDifficultyLevel(mainArgs);
        }
    }

    private void generateAllLevels(MainArgs mainArgs) {
        for (int i=1; i<=mainArgs.levelCount; i++) {
            mainArgs.levelName = createLevelShortName(i);
            mainArgs.level = i;

            generateFullLevel(mainArgs);
        }

        generateLevelFactoryClass(mainArgs);
    }

    public static void main (String[] argv) {
        MainArgs mainArgs = new MainArgs();

        JCommander.newBuilder()
                .addObject(mainArgs)
                .build()
                .parse(argv);

        LevelGenerator generator = new LevelGenerator();

        switch (mainArgs.mode) {
            case SINGLE_MAP:
                generator.generateDifficultyLevel(mainArgs);
                break;
            case FULL_LEVEL:
                generator.generateFullLevel(mainArgs);
                break;
            case ALL_LEVELS:
                generator.generateAllLevels(mainArgs);
                break;
        }
    }

}
