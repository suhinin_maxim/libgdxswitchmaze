package com.razvivashka.generator.args;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.ParameterException;

/**
 * Created by maxim on 09.11.2017.
 */

public class ModeConverter implements IStringConverter<Mode> {

    @Override
    public Mode convert(String value) {
        Mode convertedValue = Mode.fromString(value);

        if(convertedValue == null) {
            throw new ParameterException("Value " + value + "can not be converted to Mode");
        }

        return convertedValue;
    }
}
