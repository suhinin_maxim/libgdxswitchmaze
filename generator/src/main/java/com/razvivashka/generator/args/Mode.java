package com.razvivashka.generator.args;

/**
 * Created by maxim on 09.11.2017.
 */

public enum Mode {

    SINGLE_MAP,
    FULL_LEVEL,
    ALL_LEVELS;

    public static Mode fromString(String code) {

        for(Mode mode : Mode.values()) {
            if(mode.toString().equalsIgnoreCase(code)) {
                return mode;
            }
        }

        return null;
    }

}
