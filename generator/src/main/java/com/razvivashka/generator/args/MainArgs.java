package com.razvivashka.generator.args;

import com.beust.jcommander.Parameter;

import java.util.List;

/**
 * Created by admin on 03.10.17.
 */

public class MainArgs {

    @Parameter(names = {"-mode", "-m"}, description = "Mode", required = true, converter = ModeConverter.class)
    public Mode mode;

    @Parameter(names = {"-count", "-c"}, description = "Level count")
    public Integer levelCount;

    @Parameter(names = {"-size", "-s"}, arity = 2, description = "Game board size", validateWith = PositiveInteger.class)
    public List<Integer> size;

    @Parameter(names = {"-switches", "-sw"}, description = "Switches count")
    public Integer switches;

    @Parameter(names = {"-start", "-st"}, arity = 2, description = "Start cell position", validateWith = PositiveInteger.class)
    public List<Integer> start;

    @Parameter(names = {"-finish", "-f"}, arity = 2, description = "Finish cell position", validateWith = PositiveInteger.class)
    public List<Integer> finish;

    @Parameter(names = {"-level", "-l"}, description = "Level number")
    public int level;

    @Parameter(names = {"-name"}, description = "Level name")
    public String levelName;

    @Parameter(names = {"-difficulty", "-df"}, description = "Level difficulty name")
    public String difficultyName;

}
