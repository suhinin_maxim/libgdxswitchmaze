package com.razvivashka.generator;

/**
 * Created by admin on 04.10.17.
 */

public class BoardPoint {

    int row;
    int col;

    public BoardPoint (int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEquals;

        if (this == obj) {
            isEquals = true;
        } else if (obj == null) {
            isEquals = false;
        } else if (getClass() != obj.getClass()) {
            isEquals = false;
        } else {
            BoardPoint boardPoint = (BoardPoint) obj;

            if (this.row != boardPoint.row) {
                isEquals = false;
            } else if (this.col != boardPoint.col) {
                isEquals = false;
            } else {
                isEquals = true;
            }
        }

        return isEquals;
    }

    @Override
    public String toString() {
        return "[" + row + ", " + col + "]";
    }
}