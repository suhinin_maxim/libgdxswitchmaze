package com.razvivashka.generator;

import com.razvivashka.logic.domaindata.GameBoardCell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by maxim on 03.10.2017.
 */

public class PathFinder {

    private GameBoardCell[][] boardCells;
    private BoardPoint start;
    private BoardPoint finish;

    private WaveAlgorithm algorithm;

    public PathFinder(GameBoardCell[][] boardCells, BoardPoint start, BoardPoint finish) {
        this.boardCells = boardCells;
        this.start = start;
        this.finish = finish;
    }

    public List<BoardPoint> findPath() {
        boolean[][] boolBoard = toBoolBoard(boardCells);

        BoardPoint boolBoardStart = new BoardPoint(start.row*3 + 1, start.col*3 + 1);
        BoardPoint boolBoardFinish = new BoardPoint(finish.row*3 + 1, finish.col*3 + 1);
        algorithm = new WaveAlgorithm(boolBoard, boolBoardStart, boolBoardFinish);
        int distance = algorithm.computeDist();

        List<BoardPoint> boolPath = null;
        if (distance > 0) {
            boolPath = algorithm.getPath();
        }
//        else {
//        TODO maybe do some rotation and retry find path
//        }

        List<BoardPoint> boardPath = null;
        if (boolPath != null) {
            boardPath = new ArrayList<>();

            BoardPoint prevBoardPoint = null;
            for (BoardPoint boolPoint : boolPath) {
                BoardPoint boardPoint = new BoardPoint(boolPoint.row/3, boolPoint.col/3);

                if (boardPoint.equals(prevBoardPoint) == false) {
                    boardPath.add(boardPoint);
                    prevBoardPoint = boardPoint;
                }
            }

            Collections.reverse(boardPath);
        }

        return boardPath;
    }

    private boolean[][] toBoolBoard(GameBoardCell[][] boardCells) {
        boolean[][] boardBool = new boolean[boardCells.length*3][boardCells[0].length*3];
        for (int i=0; i<boardCells.length; i++) {
            for (int j=0; j<boardCells[0].length; j++) {
                for (int ci=0; ci<3; ci++) {
                    for (int cj=0; cj<3; cj++) {
                        boardBool[i*3 + ci][j*3 + cj] = boardCells[i][j].isWall(ci, cj);
                    }
                }
            }
        }

        return boardBool;
    }

//    private GameBoardCell[][] toGameBoard(int[][] boardInt) {
//        GameBoardCell[][] boardCells = new GameBoardCell[boardInt.length][boardInt[0].length];
//
//        for (int i=0; i<boardInt.length; i++) {
//            for (int j=0; j<boardInt[0].length; j++) {
//                boardCells[i][j] = new GameBoardCell(boardInt[i][j]);
//            }
//        }
//
//        return boardCells;
//    }

//    private int[][]  toBoardInt(GameBoardCell[][] boardCells) {
//        int[][] boardInt = new int[boardCells.length][boardCells[0].length];
//        for (int i=0; i<boardCells.length; i++) {
//            for (int j=0; j<boardCells[0].length; j++) {
//                boardInt[i][j] = boardCells[i][j].getType().ordinal();
//            }
//        }
//
//        return boardInt;
//    }

}
