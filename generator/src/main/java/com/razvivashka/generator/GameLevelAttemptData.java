package com.razvivashka.generator;

import com.razvivashka.logic.domaindata.HeroCell;
import com.razvivashka.logic.domaindata.StaticCell;
import com.razvivashka.logic.levels.GameLevelData;

import java.util.Arrays;
import java.util.List;

/**
 * Created by admin on 03.10.17.
 */

public class GameLevelAttemptData implements GameLevelData {

    private int mFieldSize;
    private int[][] mBoardInt;
    private List<HeroCell> mHeroCells;
    private List<StaticCell> mStaticCells;
    private int mStartCruising;
    private int mMaxCruising;
    private int mMaxScores;

    public GameLevelAttemptData () {
        // empty constructor
    }

    @Override
    public int getFieldSize () {
        return mFieldSize;
    }

    @Override
    public int[][] getBoardInt () {
        return mBoardInt;
    }

    @Override
    public List<HeroCell> getHeroCells () {
        return mHeroCells;
    }

    @Override
    public List<StaticCell> getStaticCells () {
        return mStaticCells;
    }

    @Override
    public int getStartCruising () {
        return mStartCruising;
    }

    @Override
    public int getMaxCruising () {
        return mMaxCruising;
    }

    @Override
    public int getMaxScores () {
        return mMaxScores;
    }

    public void setFieldSize (int fieldSize) {
        mFieldSize = fieldSize;
    }

    public void setBoardInt (int[][] boardInt) {
        mBoardInt = boardInt;
    }

    public void setHeroCells (List<HeroCell> heroCells) {
        mHeroCells = heroCells;
    }

    public void setStaticCells (List<StaticCell> staticCells) {
        mStaticCells = staticCells;
    }

    public void setStartCruising (int startCruising) {
        mStartCruising = startCruising;
    }

    public void setMaxCruising (int maxCruising) {
        mMaxCruising = maxCruising;
    }

    public void setMaxScores (int maxScores) {
        mMaxScores = maxScores;
    }

}
