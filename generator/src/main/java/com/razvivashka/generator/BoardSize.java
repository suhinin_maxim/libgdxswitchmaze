package com.razvivashka.generator;

/**
 * Created by admin on 04.10.17.
 */

public class BoardSize {

    int rows;
    int cols;

    public BoardSize (int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
    }

}
