package com.razvivashka.generator;


import com.razvivashka.logic.levels.GameLevelBase;
import com.razvivashka.logic.levels.GameLevelData;

/**
 * Created by admin on 03.10.17.
 */

public class GameLevelAttempt extends GameLevelBase {

    public GameLevelAttempt (GameLevelData gameLevelData) {
        super(gameLevelData);
    }

    @Override
    public int getLevel () {
        return 0;
    }

}
