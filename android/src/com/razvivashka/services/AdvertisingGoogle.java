package com.razvivashka.services;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by maxim on 03.01.2018.
 */

public class AdvertisingGoogle extends AdListener implements AdvertisingService {

    // region Fields

    private Context mContext;
    private InterstitialAd mInterstitialAd;

    private Set<AdvertisingOnClose> mListeners;

    // endregion


    // region Constructors

    public AdvertisingGoogle(Context context) {
        mContext = context;

        MobileAds.initialize(context, "ca-app-pub-2782764292160236~3091980707");

        mListeners = new HashSet<>();

        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId("ca-app-pub-2782764292160236/1902064258");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(this);
    }

    // endregion


    // region Implements AdvertisingService

    @Override
    public void show(final AdvertisingOnClose listener) {
        Handler mainHandler = new Handler(mContext.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if (mInterstitialAd.isLoaded()) {
                    mListeners.add(listener);
                    mInterstitialAd.show();
                } else {
                    Log.e("AdvertisingGoogle", "The interstitial wasn't loaded yet.");
                    listener.onClose();
                }

                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        };
        mainHandler.post(myRunnable);
    }

    // endregion


    // region Extends AdListener

    @Override
    public void onAdClosed() {
        super.onAdClosed();

        Iterator<AdvertisingOnClose> iterator = mListeners.iterator();
        while (iterator.hasNext()) {
            AdvertisingOnClose callback = iterator.next();
            callback.onClose();

            iterator.remove();
        }
    }

    // endregion

}
