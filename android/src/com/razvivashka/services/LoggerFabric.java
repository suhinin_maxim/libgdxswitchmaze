package com.razvivashka.services;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LevelEndEvent;
import com.crashlytics.android.answers.LevelStartEvent;

/**
 * Created by maxim on 08.12.2017.
 */

public class LoggerFabric implements com.razvivashka.services.Logger {

    @Override
    public void startLevel(int number) {
        Answers.getInstance().logLevelStart(new LevelStartEvent()
                .putLevelName(String.valueOf(number)));
    }

    @Override
    public void winLevel(int number, int scores) {
        Answers.getInstance().logLevelEnd(new LevelEndEvent()
                .putLevelName(String.valueOf(number))
                .putScore(scores)
                .putSuccess(true));
    }

    @Override
    public void loseLevel(int number) {
        Answers.getInstance().logLevelEnd(new LevelEndEvent()
                .putLevelName(String.valueOf(number))
                .putSuccess(false));
    }

}
