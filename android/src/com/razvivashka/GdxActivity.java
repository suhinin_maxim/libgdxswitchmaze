package com.razvivashka;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.razvivashka.services.AdvertisingGoogle;
import com.razvivashka.services.AdvertisingService;
import com.razvivashka.services.LoggerFabric;

import io.fabric.sdk.android.Fabric;

public class GdxActivity extends AndroidApplication {

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useAccelerometer = false;
		config.useCompass = false;

		final Fabric fabric = new Fabric.Builder(this)
				.kits(new Crashlytics(), new Answers())
				.debuggable(true)
				.build();
		Fabric.with(fabric);

		com.razvivashka.services.Logger logger = new LoggerFabric();

		AdvertisingService advertisingService = new AdvertisingGoogle(this);

		initialize(new GdxGame(logger, advertisingService), config);
	}

}
